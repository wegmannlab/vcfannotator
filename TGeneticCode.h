/*
 * TGeneticCode.h
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */
#include <string>
#include <map>

class TAminoacid{
public:
	std::string name;
	std::string code;
	std::string letter;

	TAminoacid(){};
	TAminoacid(std::string Name, std::string Code, std::string Letter){
		name=Name;
		code=Code;
		letter=Letter;
	};
};

class TStop:public TAminoacid{
public:
	TStop(){
		name="Stop";
		code="Stop";
		letter="!";
	};
};

class TGeneticCode{
public:
	TAminoacid aminoacids[21];
	std::map<std::string, TAminoacid*> code;
	std::map<std::string, TAminoacid*>::iterator c1;
	std::map<std::string, TAminoacid*>::iterator c2;
	std::vector<char> possibleBases;

	TGeneticCode(){
		//generate amino acids
		aminoacids[0]=TStop();
		aminoacids[1]=TAminoacid("Alanine", "Ala", "A");
		aminoacids[2]=TAminoacid("Arginine", "Arg", "R");
		aminoacids[3]=TAminoacid("Asparagine", "Asn", "N");
		aminoacids[4]=TAminoacid("Aspartic acid", "Asp", "D");
		aminoacids[5]=TAminoacid("Cysteine", "Cys", "C");
		aminoacids[6]=TAminoacid("Glutamic acid", "Glu", "E");
		aminoacids[7]=TAminoacid("Glutamine", "Gln", "Q");
		aminoacids[8]=TAminoacid("Glycine", "Gly", "G");
		aminoacids[9]=TAminoacid("Histidine", "His", "H");
		aminoacids[10]=TAminoacid("Isoleucine", "Ile", "I");
		aminoacids[11]=TAminoacid("Leucine", "Leu", "L");
		aminoacids[12]=TAminoacid("Lysine", "Lys", "K");
		aminoacids[13]=TAminoacid("Methionine", "Met", "M");
		aminoacids[14]=TAminoacid("Phenylalanine", "Phe", "F");
		aminoacids[15]=TAminoacid("Proline", "Pro", "P");
		aminoacids[16]=TAminoacid("Serine", "Ser", "S");
		aminoacids[17]=TAminoacid("Threonine", "Thr", "T");
		aminoacids[18]=TAminoacid("Tryptophan", "Trp", "W");
		aminoacids[19]=TAminoacid("Tyrosine", "Tyr", "Y");
		aminoacids[20]=TAminoacid("Valine", "Val", "V");
		//generate genetic code
		code.insert(std::pair<std::string, TAminoacid*>("TTT", &aminoacids[14]));
		code.insert(std::pair<std::string, TAminoacid*>("TCT", &aminoacids[16]));
		code.insert(std::pair<std::string, TAminoacid*>("TAT", &aminoacids[19]));
		code.insert(std::pair<std::string, TAminoacid*>("TGT", &aminoacids[5]));
		code.insert(std::pair<std::string, TAminoacid*>("TTC", &aminoacids[14]));
		code.insert(std::pair<std::string, TAminoacid*>("TCC", &aminoacids[16]));
		code.insert(std::pair<std::string, TAminoacid*>("TAC", &aminoacids[19]));
		code.insert(std::pair<std::string, TAminoacid*>("TGC", &aminoacids[5]));
		code.insert(std::pair<std::string, TAminoacid*>("TTA", &aminoacids[11]));
		code.insert(std::pair<std::string, TAminoacid*>("TCA", &aminoacids[16]));
		code.insert(std::pair<std::string, TAminoacid*>("TAA", &aminoacids[0]));
		code.insert(std::pair<std::string, TAminoacid*>("TGA", &aminoacids[0]));
		code.insert(std::pair<std::string, TAminoacid*>("TTG", &aminoacids[11]));
		code.insert(std::pair<std::string, TAminoacid*>("TCG", &aminoacids[16]));
		code.insert(std::pair<std::string, TAminoacid*>("TAG", &aminoacids[0]));
		code.insert(std::pair<std::string, TAminoacid*>("TGG", &aminoacids[18]));
		code.insert(std::pair<std::string, TAminoacid*>("CTT", &aminoacids[11]));
		code.insert(std::pair<std::string, TAminoacid*>("CCT", &aminoacids[15]));
		code.insert(std::pair<std::string, TAminoacid*>("CAT", &aminoacids[9]));
		code.insert(std::pair<std::string, TAminoacid*>("CGT", &aminoacids[2]));
		code.insert(std::pair<std::string, TAminoacid*>("CTC", &aminoacids[11]));
		code.insert(std::pair<std::string, TAminoacid*>("CCC", &aminoacids[15]));
		code.insert(std::pair<std::string, TAminoacid*>("CAC", &aminoacids[9]));
		code.insert(std::pair<std::string, TAminoacid*>("CGC", &aminoacids[2]));
		code.insert(std::pair<std::string, TAminoacid*>("CTA", &aminoacids[11]));
		code.insert(std::pair<std::string, TAminoacid*>("CCA", &aminoacids[15]));
		code.insert(std::pair<std::string, TAminoacid*>("CAA", &aminoacids[7]));
		code.insert(std::pair<std::string, TAminoacid*>("CGA", &aminoacids[2]));
		code.insert(std::pair<std::string, TAminoacid*>("CTG", &aminoacids[11]));
		code.insert(std::pair<std::string, TAminoacid*>("CCG", &aminoacids[15]));
		code.insert(std::pair<std::string, TAminoacid*>("CAG", &aminoacids[7]));
		code.insert(std::pair<std::string, TAminoacid*>("CGG", &aminoacids[2]));
		code.insert(std::pair<std::string, TAminoacid*>("ATT", &aminoacids[10]));
		code.insert(std::pair<std::string, TAminoacid*>("ACT", &aminoacids[17]));
		code.insert(std::pair<std::string, TAminoacid*>("AAT", &aminoacids[3]));
		code.insert(std::pair<std::string, TAminoacid*>("AGT", &aminoacids[16]));
		code.insert(std::pair<std::string, TAminoacid*>("ATC", &aminoacids[10]));
		code.insert(std::pair<std::string, TAminoacid*>("ACC", &aminoacids[17]));
		code.insert(std::pair<std::string, TAminoacid*>("AAC", &aminoacids[3]));
		code.insert(std::pair<std::string, TAminoacid*>("AGC", &aminoacids[16]));
		code.insert(std::pair<std::string, TAminoacid*>("ATA", &aminoacids[10]));
		code.insert(std::pair<std::string, TAminoacid*>("ACA", &aminoacids[17]));
		code.insert(std::pair<std::string, TAminoacid*>("AAA", &aminoacids[12]));
		code.insert(std::pair<std::string, TAminoacid*>("AGA", &aminoacids[2]));
		code.insert(std::pair<std::string, TAminoacid*>("ATG", &aminoacids[13]));
		code.insert(std::pair<std::string, TAminoacid*>("ACG", &aminoacids[17]));
		code.insert(std::pair<std::string, TAminoacid*>("AAG", &aminoacids[12]));
		code.insert(std::pair<std::string, TAminoacid*>("AGG", &aminoacids[2]));
		code.insert(std::pair<std::string, TAminoacid*>("GTT", &aminoacids[20]));
		code.insert(std::pair<std::string, TAminoacid*>("GCT", &aminoacids[1]));
		code.insert(std::pair<std::string, TAminoacid*>("GAT", &aminoacids[4]));
		code.insert(std::pair<std::string, TAminoacid*>("GGT", &aminoacids[8]));
		code.insert(std::pair<std::string, TAminoacid*>("GTC", &aminoacids[20]));
		code.insert(std::pair<std::string, TAminoacid*>("GCC", &aminoacids[1]));
		code.insert(std::pair<std::string, TAminoacid*>("GAC", &aminoacids[4]));
		code.insert(std::pair<std::string, TAminoacid*>("GGC", &aminoacids[8]));
		code.insert(std::pair<std::string, TAminoacid*>("GTA", &aminoacids[20]));
		code.insert(std::pair<std::string, TAminoacid*>("GCA", &aminoacids[1]));
		code.insert(std::pair<std::string, TAminoacid*>("GAA", &aminoacids[6]));
		code.insert(std::pair<std::string, TAminoacid*>("GGA", &aminoacids[8]));
		code.insert(std::pair<std::string, TAminoacid*>("GTG", &aminoacids[20]));
		code.insert(std::pair<std::string, TAminoacid*>("GCG", &aminoacids[1]));
		code.insert(std::pair<std::string, TAminoacid*>("GAG", &aminoacids[6]));
		code.insert(std::pair<std::string, TAminoacid*>("GGG", &aminoacids[8]));
		//possible bases
		possibleBases.push_back('A');
		possibleBases.push_back('G');
		possibleBases.push_back('C');
		possibleBases.push_back('T');
	};
	bool isSynonymous(std::string codon1, std::string codon2){
		c1=code.find(codon1);
		c2=code.find(codon2);
		if(c1->second == c2->second) return true;
		else return false;
	};

};
