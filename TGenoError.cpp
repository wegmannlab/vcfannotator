/*
 * TGenoError.cpp
 *
 *  Created on: May 11, 2017
 *      Author: wegmannd
 */

#include "TGenoError.h"

TGenoError::TGenoError(bool Verbose){
	verbose = Verbose;

	vcfRead = false;
	numSnps = 0;
	groupOfSample = NULL;
	maxDepthPlusOne = 0;
	numdepthsWithData = 0;
	depthsWithData = NULL;
	depthIndex = NULL;
	numDataPoints = NULL;

	EMstorageInitialized = false;
	freq = NULL;
	newFreq = NULL;
	epsilon = NULL;
	newEpsilon = NULL;
	emissionProbs = NULL;
	weights = NULL;
}

////////////////////////////////////////
//SIMULATIONS
////////////////////////////////////////

void TGenoError::fillEmissionLikelihoodMatrixSimulations(TSquareMatrixStorage & likelihood, const double & error){
	likelihood.resize(3); //row=true, col=observed
	double oneMinusError = 1.0 - error;
	likelihood(0,0) = oneMinusError * oneMinusError;
	likelihood(0,1) = 2.0 * oneMinusError * error;
	likelihood(0,2) = error * error;
	likelihood(1,0) = oneMinusError * error;
	likelihood(1,1) = oneMinusError * oneMinusError + error * error;
	likelihood(1,2) = oneMinusError * error;
	likelihood(2,0) = error * error;
	likelihood(2,1) = 2.0 * oneMinusError * error;
	likelihood(2,2) = oneMinusError * oneMinusError;
}


void TGenoError::simulateGenoErrorData(TParameters* myParameters, TRandomGenerator* randomGenerator){
	//Reading basic parameters
	if(verbose) std::cerr << "   - Reading parameters:" << std::endl;
	int numSamples = myParameters->getParameterIntWithDefault("numSamples", 5);
	if(verbose) std::cerr << "     - Will simulate " << numSamples << " samples." << std::endl;
	int numReplicates = myParameters->getParameterIntWithDefault("numReplicates", 3);
	if(verbose) std::cerr << "     - Will simulate " << numReplicates << " replicates per samples." << std::endl;
	int numSites = myParameters->getParameterIntWithDefault("numSites", 10000);
	if(verbose) std::cerr << "     - Will simulate " << numSites << " sites." << std::endl;
	double het = myParameters->getParameterDoubleWithDefault("het", 0.25);
	if(verbose) std::cerr << "     - Will simulate a fraction " << het << " of heterozygous sites." << std::endl;

	//read likelihoods
	std::vector<double> errorRates;
	if(myParameters->parameterExists("error")){
		myParameters->fillParameterIntoVector("error", errorRates, ',');
	} else {
		errorRates.push_back(0.05);
	}
	int numErrorRates = errorRates.size();

	int* depths = new int[numErrorRates];
	for(int d=0; d<numErrorRates; ++d){
		depths[d] = d*5 + 5;
	}
	if(verbose){
		std::string outString = "     - Will assume ";
		if(numErrorRates == 1){
			outString += "a per allele genotyping error rate of " + toString(errorRates[0]) + ".";
		} else {
			std::vector<double>::iterator it=errorRates.begin();
			outString += "error rates " + toString(*it); ++it;
			for(; it!=errorRates.end(); ++it)
				outString += ", " + toString(*it);
			outString += " for depths ";
			for(int d=0; d<numErrorRates; ++d)
				outString += toString(depths[d]) + ", ";
			outString += "respectively.";
		}
		std::cerr << outString << std::endl;
	}

	//fill likelihood table for different depths
	TSquareMatrixStorage* likelihoods = new TSquareMatrixStorage[numErrorRates];
	TSquareMatrixStorage* likelihoodsCumul = new TSquareMatrixStorage[numErrorRates];
	TSquareMatrixStorage* likelihoodsPhred = new TSquareMatrixStorage[numErrorRates];
	for(int d=0; d<numErrorRates; ++d){
		fillEmissionLikelihoodMatrixSimulations(likelihoods[d], errorRates[d]);
		likelihoodsCumul[d].fillAsCumulative(likelihoods[d]);
		likelihoodsPhred[d].resize(3);
		for(int i=0; i<3; ++i){
			for(int j=0; j<3; ++j){
				likelihoodsPhred[d](i,j) = -10.0 * log10(likelihoods[d](i,j));
			}
		}
	}

	//open VCF file (using a simple stream
	bool isFile = false;
	std::ostream* vcfStream;
	std::ofstream vcfOutFileStream;
	if(myParameters->parameterExists("outname")){
		std::string filename=myParameters->getParameterString("outname");
		if(verbose) std::cerr << " - Writing resulting vcf to file '" << filename << "'." << std::endl;
		vcfOutFileStream.open(filename.c_str());
		if(!vcfOutFileStream) throw "Failed to open file '" + filename + "'!";
		vcfStream = &vcfOutFileStream;
		isFile = true;
	} else {
		if(verbose) std::cerr << "   - Writing resulting vcf to standard output." << std::endl;
		vcfStream = &std::cout;
	}

	//write header
	(*vcfStream) << "##fileformat=VCFv4.2\n";
	(*vcfStream) << "##source=SegDistort\n";
	(*vcfStream) << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
	(*vcfStream) << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
	(*vcfStream) << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
	(*vcfStream) << "##FORMAT=<ID=DP,Number=G,Type=Integer,Description=\"Depth at site\">\n";
	(*vcfStream) << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";
	int r,s;
	std::string sampleGroupString = "";
	for(s=0; s<numSamples; ++s){
		if(s>0) sampleGroupString += ',';
		sampleGroupString += "[";
		for(r=0; r<numReplicates; ++r){
			(*vcfStream) << "\tSample_" << s << "_" << r;
			if(r>0) sampleGroupString += ',';
			sampleGroupString += "Sample_" + toString(s) + "_" + toString(r);
		}
		sampleGroupString += ']';
	}

	(*vcfStream) << "\n";

	//report sample group string
	if(verbose) std::cerr << "Corresponding sample group string is '" << sampleGroupString << "'" << std::endl;

	//prepare storage
	int trueGenotype, observedGenotype, d;

	//loop over sites
	if(verbose) std::cerr << "   - Conducting simulations ...";
	for(long i=0; i<numSites; ++i){
		//write site info
		(*vcfStream) << "1\t" << i*100 + 1 << "\t.\tA\tC\t50\t.\t.\tGT:GQ:PL:DP";

		//loop over samples
		for(s=0; s<numSamples; ++s){

			//simulate genotype
			if(randomGenerator->getRand() < het)
				trueGenotype = 1;
			else trueGenotype = round(randomGenerator->getRand()) * 2;

			//now simulate data with different depths and print
			for(r=0; r<numReplicates; ++r){
				d = randomGenerator->pickOne(numErrorRates);
				observedGenotype = randomGenerator->pickOne(3, likelihoodsCumul[d].Row(trueGenotype));
				if(observedGenotype == 0) (*vcfStream) << "\t0/0";
				else if(observedGenotype == 1) (*vcfStream) << "\t0/1";
				else (*vcfStream) << "\t1/1";
				(*vcfStream) << ":30:" << likelihoodsPhred[d](0,observedGenotype) << "," << likelihoodsPhred[d](1,observedGenotype) << "," << likelihoodsPhred[d](2,observedGenotype);
				(*vcfStream) << ":" << depths[d];
			}
		}
		(*vcfStream) << "\n";
	}

	//clean up
	delete[] likelihoods;
	delete[] likelihoodsPhred;
	delete[] likelihoodsCumul;
	if(isFile)
		vcfOutFileStream.close();

	if(verbose) std::cerr << " done!";
}


////////////////////////////////////////
//INFERENCE
////////////////////////////////////////

void TGenoError::inferGenoError(TParameters* myParameters){
	//Reading basic parameters
	if(verbose) std::cerr << "   - Reading parameters:" << std::endl;
	double epsEm = myParameters->getParameterIntWithDefault("eps", 0.00001);
	if(verbose) std::cerr << "     - Will stop EM algorithm if change in parameter estimates < " << epsEm << "." << std::endl;
	int maxNumEmIterations = myParameters->getParameterIntWithDefault("numIter", 1000);
	if(verbose) std::cerr << "     - Will run EM for at max " << maxNumEmIterations << " iterations." << std::endl;
	int numEpsIter = myParameters->getParameterIntWithDefault("epsIter", 200);
	if(verbose) std::cerr << "     - Will optimize epsilon numerically for " << numEpsIter << " iterations." << std::endl;

	//read VCF
	readDataFromVCF(myParameters);

	//read outname, if given
	if(myParameters->parameterExists("out")){
		outname = myParameters->getParameterString("out");
	}
	if(verbose) std::cerr << "    - Will write output with prefix '" << outname << "'." << std::endl;

	//initialize storage & estimate initial parameters
	if(verbose) std::cerr << "    - Running EM algorithm:" << std::endl;
	initializeEMStorage();
	estimateInitialGenotypeFrequenciesAndErrorRates();
	double oldLL = calculateLL(freq, epsilon);
	if(verbose) std::cerr << "      - Initial LL = " << std::setprecision(12) << oldLL << std::endl;

	//temporary variables
	double LL;
	double delta;
	int d; int G; int s;
	std::vector<short*>::iterator itG;
	std::vector<int*>::iterator itD;
	long l;
	double sum;
	double** tmpFreq;
	double* tmpEpsilon;

	//---------------------------
	//run EM algo
	//---------------------------
	for(int i=0; i<maxNumEmIterations; ++i){
		//set storage for new estimates to zero
		for(G=0; G<groups.numGroups; ++G){
			newFreq[G][0] = 0.0;
			newFreq[G][1] = 0.0;
			newFreq[G][2] = 0.0;
		}
		for(d = 0; d<maxDepthPlusOne; ++d)
			newEpsilon[d] = 0.0;

		//fill emission probabilities
		for(d=0; d<numdepthsWithData; ++d)
			fillEmissionProbabilitiesGenoError(emissionProbs[d], epsilon[d]);

		//loop over sites
		itG = genotypes.begin();
		itD = depths.begin();
		l = 0;
		for(; itG != genotypes.end(); ++itG, ++itD, ++l){
			//first calculate all weights
			for(G=0; G<groups.numGroups; ++G){
				weights[l][G][0] = freq[G][0]; weights[l][G][1] = freq[G][1]; weights[l][G][2] = freq[G][2];
			}
			for(s=0; s<groups.numSamples; ++s){
				weights[l][groupOfSample[s]][0] *= emissionProbs[(*itD)[s]][0][(*itG)[s]];
				weights[l][groupOfSample[s]][1] *= emissionProbs[(*itD)[s]][1][(*itG)[s]];
				weights[l][groupOfSample[s]][2] *= emissionProbs[(*itD)[s]][2][(*itG)[s]];
			}

			//normalize and add to frequency estimates
			for(G=0; G<groups.numGroups; ++G){
				sum = weights[l][G][0] + weights[l][G][1] + weights[l][G][2];
				weights[l][G][0] /= sum;
				weights[l][G][1] /= sum;
				weights[l][G][2] /= sum;

				//add to frequency estimates
				newFreq[G][0] += weights[l][G][0];
				newFreq[G][1] += weights[l][G][1];
				newFreq[G][2] += weights[l][G][2];
			}
		}

		//estimate all f
		for(G=0; G<groups.numGroups; ++G){
			newFreq[G][0] /= (double) numSnps;
			newFreq[G][1] /= (double) numSnps;
			newFreq[G][2] /= (double) numSnps;
		}

		//estimate epsilon by numerically optimizing Q
		estimateEpsilonNumerically(numEpsIter);



		//calc LL
		LL = calculateLL(newFreq, newEpsilon);
		delta = LL - oldLL;
		if(verbose) std::cerr << "      - EM iteration " << i+1 << ": LL = " << std::setprecision(12) << LL << ", deltaLL = " << delta << std::endl;

		//save estimates
		tmpFreq = freq;
		freq = newFreq;
		newFreq = tmpFreq;
		tmpEpsilon = epsilon;
		epsilon = newEpsilon;
		newEpsilon = tmpEpsilon;
		oldLL = LL;

		//check if converged
		if(delta < epsEm){
			if(verbose) std::cerr << "        -> EM converged!" << std::endl;
			break;
		}
	}

	//report estimates
	std::string filename = outname + "_genotypeErrorRates.txt";
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";
	out << "depth\tsize\terrorRate\n";

	/*
	for(d=0; d<numdepthsWithData; ++d)
		out << depthsWithData[d] << "\t" << numDataPoints[d] << "\t" << epsilon[d] << "\n";
	*/

	for(d=0; d<maxDepthPlusOne; ++d){
		out << d << "\t";
		if(depthIndex[d] < 0) out << "0\t0.0\n";
		else out << numDataPoints[depthIndex[d]] << "\t" << epsilon[depthIndex[d]] << "\n";
	}

	out.close();
	if(verbose) std::cerr << "    - Genotype error rate estimates written to '" << filename << "'." << std::endl;

	filename = outname + "_genotypeFrequencies.txt";
	out.open(filename.c_str());
	if(!out)
		throw "Failed to open file '" + filename + "' for writing!";
	out << "sample\tfreq_0\tfreq_1\tfreq_2\n";
	for(G=0; G<groups.numGroups; ++G)
		out << groups.getGroupString(G) << "\t" << freq[G][0] << "\t" << freq[G][1] << "\t" << freq[G][2] << "\n";
	out.close();
	if(verbose) std::cerr << "    - Genotype frequency estimates written to '" << filename << "'." << std::endl;

}

void TGenoError::readDataFromVCF(TParameters* myParameters){
	TVcfFileSingleLine vcfFile(verbose);
	std::ifstream vcfFileStream;
	if(myParameters->parameterExists("file")){
		std::string filename=myParameters->getParameterString("file");
		if(verbose) std::cerr << "   - Reading vcf from file '" << filename << "':" << std::endl;
		vcfFileStream.open(filename.c_str());
		if(!vcfFileStream) throw "Failed to open file '" + filename + "'!";
		vcfFile.setStream(vcfFileStream);
		outname = extractBeforeLast(filename, '.');
	} else {
		if(verbose) std::cerr << "   - Reading vcf from standard input:" << std::endl;
		vcfFile.setStream(std::cin);
		outname = "genoError";
	}

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//read sample groups to identify replicates
	groups.initialize(&vcfFile, myParameters->getParameterString("groups"), verbose);
	if(groups.numGroups < 1) throw "No groups defined!";
	unsigned int* allSamples = NULL;
	groups.fillArrayOfAllSamples(allSamples);
	groupOfSample = new int[groups.numSamples];
	int G; int i; int s = 0;
	for(G=0; G<groups.numGroups; ++G){
		for(i=0; i<groups.numSamplesPerGroup[G]; ++i){
			groupOfSample[s] = G;
			++s;
		}
	}

	//prepare storage: genotypes and depth
	short* genoPointer;
	int* depthsPointer;
	maxDepthPlusOne = 0;

	//Loop through VCF file
	if(verbose) std::cerr << "    - Parsing VCF file:" << std::endl;
	numSnps = 0;
	long counter = 0;
	while(vcfFile.next()){
		++counter;
		//check if it is biallelic and ignore others
		if(vcfFile.getNumAlleles()==2){
			++numSnps;
			genoPointer = new short[groups.numSamples];
			genotypes.push_back(genoPointer);
			depthsPointer = new int[groups.numSamples];
			depths.push_back(depthsPointer);
			for(s=0; s<groups.numSamples; ++s){
				genoPointer[s] = vcfFile.sampleGenotype(allSamples[s]);
				depthsPointer[s] = vcfFile.sampleDepth(allSamples[s]);
				maxDepthPlusOne = std::max(maxDepthPlusOne, depthsPointer[s]);
			}
		}

		if(verbose && counter % 10000 == 0)	std::cerr << "    - read " << counter << " lines ..." << std::endl;
	}
	maxDepthPlusOne += 1;
	delete[] allSamples;
	if(verbose) std::cerr << "       -> " << numSnps << " bi-allelic markers read" << std::endl;

	//now create list of depth with data so that we will only need to consider those
	numdepthsWithData = 0;
	bool* depthsHasData = new bool[maxDepthPlusOne];
	int d;
	for(d = 0; d<maxDepthPlusOne; ++d){
		depthsHasData[d] = false;
	}
	for(std::vector<int*>::iterator itD = depths.begin(); itD != depths.end(); ++itD){
		for(s=0; s<groups.numSamples; ++s)
			depthsHasData[(*itD)[s]] = true;
	}

	for(d = 1; d<maxDepthPlusOne; ++d){
		if(depthsHasData[d]){
			++numdepthsWithData;
		}
	}
	++numdepthsWithData; //one one extra for depth = 0
	depthsWithData = new int[numdepthsWithData];
	depthIndex = new int[maxDepthPlusOne];
	//add extra for depth = 0
	depthsWithData[0] = 0;
	depthIndex[0] = 0;
	i = 1;
	for(d = 1; d<maxDepthPlusOne; ++d){
		if(depthsHasData[d]){
			depthsWithData[i] = d;
			depthIndex[d] = i;
			++i;
		} else
			depthIndex[d] = -1;
	}
	delete[] depthsHasData;

	//now store depth according to new index and count data points
	numDataPoints = new long[numdepthsWithData];
	for(int d = 0; d<numdepthsWithData; ++d)
		numDataPoints[d] = 0;
	for(std::vector<int*>::iterator itD = depths.begin(); itD != depths.end(); ++itD){
		for(s=0; s<groups.numSamples; ++s){
			(*itD)[s] = depthIndex[(*itD)[s]];
			++numDataPoints[(*itD)[s]];
		}
	}

	vcfRead = true;
}

void TGenoError::initializeEmissionProbabilityMatrix(){
	//error rates epsilon and emissions
	emissionProbs = new double**[numdepthsWithData];
	for(int d = 0; d<numdepthsWithData; ++d){
		emissionProbs[d] = new double*[3];
		emissionProbs[d][0] = new double[4]; //also missing!
		emissionProbs[d][1] = new double[4]; //also missing!
		emissionProbs[d][2] = new double[4]; //also missing!
	}
	emissionProbsInitialized = true;
}

void TGenoError::freeEmissionProbabilityMatrix(){
	if(emissionProbsInitialized){
		for(int d = 0; d<numdepthsWithData; ++d){
			delete[] emissionProbs[d][0];
			delete[] emissionProbs[d][1];
			delete[] emissionProbs[d][2];
			delete[] emissionProbs[d];
		}
		delete[] emissionProbs;
	}
	emissionProbsInitialized = false;
}

void TGenoError::initializeEMStorage(){
	//frequencies
	freq = new double*[groups.numGroups];
	newFreq = new double*[groups.numGroups];
	for(int G=0; G<groups.numGroups; ++G){
		freq[G] = new double[4]; //missing genotype = 3
		newFreq[G] = new double[3];
	}

	//error rates epsilon and emissions
	epsilon = new double[maxDepthPlusOne];
	newEpsilon = new double[maxDepthPlusOne];
	initializeEmissionProbabilityMatrix();

	//weights
	weights = new double**[numSnps];
	long l; int G;
	for(l=0; l<numSnps; ++l){
		weights[l] = new double*[groups.numGroups];
		for(G=0; G<groups.numGroups; ++G)
			weights[l][G] = new double[3];
	}

	EMstorageInitialized = true;
}

void TGenoError::freeEMStorage(){
	if(EMstorageInitialized){
		for(int G=0; G<groups.numGroups; ++G){
			delete[] freq[G];
			delete[] newFreq[G];
		}
		delete[] freq;
		delete[] newFreq;
		delete[] epsilon;
		delete[] newEpsilon;
		for(long l=0; l<numSnps; ++l){
			for(int G=0; G<groups.numGroups; ++G)
				delete[] weights[l][G];
			delete[] weights[l];
		}
		delete[] weights;
	}
	EMstorageInitialized = false;
}

void TGenoError::estimateInitialGenotypeFrequenciesAndErrorRates(){
	//estimate initial genotype frequencies and error rates (independent of depths)
	for(int G=0; G<groups.numGroups; ++G){
		freq[G][0] = 0.0;
		freq[G][1] = 0.0;
		freq[G][2] = 0.0;
		freq[G][3] = 0.0;
	}

	int tmp;
	double eps = 0.0;
	double numcomparisons = 0.0;
	int oldGroup; int s;
	for(std::vector<short*>::iterator itG = genotypes.begin(); itG != genotypes.end(); ++itG){
		oldGroup = -1;
		for(s=0; s<groups.numSamples; ++s){
			++freq[groupOfSample[s]][(*itG)[s]];

			//compare genotypes within groups
			if(groupOfSample[s] != oldGroup){
				tmp = (*itG)[s];
				oldGroup = groupOfSample[s];
			} else {
				eps += abs(tmp - (*itG)[s]);
				++numcomparisons;
			}
		}
	}
	eps = (eps + 1) / (double) (4.0 * numcomparisons + 1);
	if(verbose) std::cerr << "      - Initial estimate of epsilon = " << eps << std::endl;

	//normalize estimates
	double sum;
	for(int G=0; G<groups.numGroups; ++G){
		sum = freq[G][0] + freq[G][1] + freq[G][2] + freq[G][3];
		freq[G][0] /= sum;
		freq[G][1] /= sum;
		freq[G][2] /= sum;
		freq[G][3] = 1.0; //missing genotype has always probability 1.0
	}

	//set all epsilon
	for(int d = 0; d<numdepthsWithData; ++d){
		epsilon[d] = eps;
	}
}

void TGenoError::estimateEpsilonNumerically(int numIter){
	//estimate all epsilon using numerical optimization
	//do not estimate epsilon for depth=0!

	//prepare storage
	double* curQ = new double[numdepthsWithData];
	double* oldQ = new double[numdepthsWithData];
	double* step = new double[numdepthsWithData]; // on log scale!

	//set starting epsilon at old epsilon
	//init steps
	int d;
	for(d = 1; d<numdepthsWithData; ++d){
		newEpsilon[d] = epsilon[d];
		step[d] = 0.1;
	}

	//calculate Q or current epsilons
	calculateQ_epsilon(oldQ, newEpsilon);

	//now loop until convergence
	for(int i=0; i<numIter; ++i){
		//update all epsilon
		for(d = 1; d<numdepthsWithData; ++d){
			newEpsilon[d] = exp(log(newEpsilon[d]) + step[d]);
			//std::cout << std::setprecision(20) << d << ") new eps = " << newEpsilon[d] << std::endl;
			if(newEpsilon[d] > 0.9999999999)
				newEpsilon[d] = 0.9999999999;
			else if(newEpsilon[d] < 0.0000000001)
				newEpsilon[d] = 0.0000000001;
		}

		//calculate new Q
		calculateQ_epsilon(curQ, newEpsilon);

		//adjust step
		for(d = 1; d<numdepthsWithData; ++d){
			//std::cout << d << ") Q = " << curQ[d];

			if(curQ[d] < oldQ[d])
				step[d] = -step[d] / exp(1.0);
			oldQ[d] = curQ[d];
			//std::cout << ", step = " << step[d] << std::endl;
		}
	}

	//clean up
	delete[] curQ;
	delete[] oldQ;
	delete[] step;
}

void TGenoError::calculateQ_epsilon(double* Q, double* thisEpsilon){
	//initialize emission probabilities
	for(int d=0; d<numdepthsWithData; ++d)
		fillEmissionProbabilitiesGenoErrorLog(emissionProbs[d], thisEpsilon[d]);

	//variables
	std::vector<short*>::iterator itG = genotypes.begin();
	std::vector<int*>::iterator itD = depths.begin();
	long l = 0;
	int s;

	//set all Q = 0
	for(int d = 0; d<numdepthsWithData; ++d){
		Q[d] = 0.0;
	}

	//Now add to appropriate Q
	for(; itG != genotypes.end(); ++itG, ++itD, ++l){
		for(s=0; s<groups.numSamples; ++s){
			Q[(*itD)[s]] += weights[l][groupOfSample[s]][0] * emissionProbs[(*itD)[s]][0][(*itG)[s]]
					     +  weights[l][groupOfSample[s]][1] * emissionProbs[(*itD)[s]][1][(*itG)[s]]
						 +  weights[l][groupOfSample[s]][2] * emissionProbs[(*itD)[s]][2][(*itG)[s]];
		}
	}
}

double TGenoError::calculateLL(double** theseFrequencies, double* thisEpsilon){
	//initialize emission probabilities
	for(int d=0; d<numdepthsWithData; ++d)
		fillEmissionProbabilitiesGenoError(emissionProbs[d], thisEpsilon[d]);

	//variables
	std::vector<short*>::iterator itG = genotypes.begin();
	std::vector<int*>::iterator itD = depths.begin();
	int s; int G;
	double LL = 0.0;
	double** tmp = new double*[groups.numGroups];
	for(G=0; G<groups.numGroups; ++G){
		tmp[G] = new double[3];
	}

	//Now add to appropriate Q
	for(; itG != genotypes.end(); ++itG, ++itD){
		for(G=0; G<groups.numGroups; ++G){
			tmp[G][0] = 1.0; tmp[G][1] = 1.0; tmp[G][2] = 1.0;
		}
		for(s=0; s<groups.numSamples; ++s){
			tmp[groupOfSample[s]][0] *= emissionProbs[(*itD)[s]][0][(*itG)[s]];
			tmp[groupOfSample[s]][1] *= emissionProbs[(*itD)[s]][1][(*itG)[s]];
			tmp[groupOfSample[s]][2] *= emissionProbs[(*itD)[s]][2][(*itG)[s]];
		}
		for(G=0; G<groups.numGroups; ++G){
			LL += log(tmp[G][0] * theseFrequencies[G][0] + tmp[G][1] * theseFrequencies[G][1] + tmp[G][2] * theseFrequencies[G][2]);
		}
	}

	//clean up
	for(G=0; G<groups.numGroups; ++G)
		delete[] tmp[G];
	delete[] tmp;
	return LL;
}

void TGenoError::fillEmissionProbabilitiesGenoError(double** em, const double & epsilon){
	//em[i][j] = P(g=j|gamma=i)
	double oneMinusEps = 1.0 - epsilon;

	em[0][0] = oneMinusEps * oneMinusEps;
	em[0][1] = 2.0 * epsilon * oneMinusEps;
	em[0][2] = epsilon * epsilon;
	em[0][3] = 1.0; //missing

	em[1][0] = epsilon * oneMinusEps;
	em[1][1] = epsilon * epsilon + oneMinusEps * oneMinusEps;
	em[1][2] = epsilon * oneMinusEps;
	em[1][3] = 1.0; //missing

	em[2][0] = epsilon * epsilon;
	em[2][1] = 2.0 * epsilon * oneMinusEps;
	em[2][2] = oneMinusEps * oneMinusEps;
	em[2][3] = 1.0; //missing
}

void TGenoError::fillEmissionProbabilitiesGenoErrorLog(double** em, const double & epsilon){
	fillEmissionProbabilitiesGenoError(em, epsilon);
	for(int i=0; i<4; ++i){
		em[0][i] = log10(em[0][i]);
		em[1][i] = log10(em[1][i]);
		em[2][i] = log10(em[2][i]);
	}
}

////////////////////////////////////////
//RECALIBRATE
////////////////////////////////////////

void TGenoError::recalibrateVCFGenoError(TParameters* myParameters){
	//open vcf file
	//-------------
	if(verbose) std::cerr << " - Recalibrating VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);
	std::ifstream vcfFileStream;
	if(myParameters->parameterExists("file")){
		std::string filename = myParameters->getParameterString("file");
		if(verbose) std::cerr << "   - Reading vcf from file '" << filename << "':" << std::endl;
		vcfFileStream.open(filename.c_str());
		if(!vcfFileStream) throw "Failed to open file '" + filename + "'!";
		vcfFile.setStream(vcfFileStream);
		outname = extractBeforeLast(filename, '.');
	} else {
		if(verbose) std::cerr << "   - Reading vcf from standard input:" << std::endl;
		vcfFile.setStream(std::cin);
		outname = "genoError";
	}

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//read depth dependent genotyping errors (per allele)
	//---------------------------------------------------
	std::string inputFileName = myParameters->getParameterString("genoErrors");
	if(verbose) std::cerr << " - Reading genotyping error rates from '" << inputFileName << "." << std::endl;
	std::ifstream file(inputFileName.c_str());
	if(!file) throw "Failed to open file with genotyping errors' " + inputFileName + "'!";
	std::string paramLine;

	//parse input file
	std::map<int,double> genoErrors;
	std::vector<std::string> tmpVec;
	int depth; double error;
	std::getline(file, paramLine); //read header
	int lineNum = 0;
	while(file.good() && !file.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(file, tmpVec);
		if(!tmpVec.empty()){
			if(tmpVec.size() != 3) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + inputFileName + "'! There should be 3!";
			depth = stringToIntCheck(tmpVec[0]);
			error = stringToDoubleCheck(tmpVec[2]);
			if(genoErrors.find(depth) != genoErrors.end())
				throw "Multiple entries for depth " + toString(depth) + "!";
			genoErrors.insert(std::pair<int,double>(depth, error));
		}
	}

	if(verbose) std::cerr << "     -> found " << genoErrors.size() << " error rates." << std::endl;

	//construct emission probability matrices
	numdepthsWithData = genoErrors.size();
	initializeEmissionProbabilityMatrix();
	int maxDepth = genoErrors.rbegin()->first;
	int* depthIndex = new int[maxDepth+1];
	std::map<int,double>::iterator it = genoErrors.begin();
	int index = 0;
	for(int i=0; i<maxDepth+1; ++i){
		if(i == it->first){
			depthIndex[i] = index;
			fillEmissionProbabilitiesGenoErrorLog(emissionProbs[index], it->second);
			++it;
			++index;
		} else depthIndex[i] = -1;
	}

	//check if there are some groups
	//------------------------------
	bool hasGroups = false;
	TSampleGroups groups;
	int numSamplesNotInGrops;
	unsigned int* samplesNotInGroups;
	unsigned int** samplesInGroups;
	if(myParameters->parameterExists("groups")){
		hasGroups = true;
		groups.initialize(&vcfFile, myParameters->getParameterString("groups"), verbose);
		if(groups.numGroups < 1) throw "No groups defined!";

		groups.fill2DArrayOfSamples(samplesInGroups);

		numSamplesNotInGrops = vcfFile.numSamples() - groups.numSamples;
		samplesNotInGroups = new unsigned int[numSamplesNotInGrops];
		int j=0;
		for(int s=0; s<vcfFile.numSamples(); ++s){
			if(!groups.sampleInGroup(s)){
				samplesNotInGroups[j] = s;
				++j;
			}
		}
	} else {
		if(verbose) std::cerr << " - No groups defined." << std::endl;
		numSamplesNotInGrops = vcfFile.numSamples();
		samplesNotInGroups = new unsigned int[numSamplesNotInGrops];
		for(int s=0; s<vcfFile.numSamples(); ++s){
			samplesNotInGroups[s] = s;
		}
	}

	//prepare output vcf
	//------------------
	std::string outname = myParameters->getParameterString("outname");
	if(outname.empty()) outname = "reclibarted.vcf";
	std::ofstream outVcf(outname.c_str());
	if(!outVcf)
		throw "Failed to open file '" + outname + "' for writing!";

	//write header
	outVcf << "##fileformat=VCFv4.2\n";
	outVcf << "##source=SegDistort\n";
	outVcf << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
	outVcf << "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n";
	outVcf << "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Phred-scaled genotype likelihoods\">\n";
	outVcf << "##FORMAT=<ID=DP,Number=G,Type=Integer,Description=\"Depth at site\">\n";
	outVcf << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT";

	//first sample of each group
	if(hasGroups){
		groups.begin();
		do{
			outVcf << "\t" << groups.firstSampleInCurrentGroup();
		} while(groups.next());
	}

	//now all other samples
	for(int s=0; s<numSamplesNotInGrops; ++s)
		outVcf << "\t" << vcfFile.sampleName(samplesNotInGroups[s]);
	outVcf << "\n";

	//now parse VCF and translate errors
	//----------------------------------
	//some variables
	double* lik = new double[3];
	int G; int s;
	std::string depthTag = "DP";
	double ** thisEm;
	int g;
	std::string* genoString = new std::string[3];
	genoString[0] = "\t0/0:"; genoString[1] = "\t0/1:"; genoString[2] = "\t1/1:";
	int counter = 0;

	//loop through file
	while(vcfFile.next()){
		++counter;
		if(vcfFile.getNumAlleles()==2){
			//write variant and such
			outVcf << vcfFile.chr() << "\t" << vcfFile.position() << "\t.\t" << vcfFile.getRefAllele() << "\t" << vcfFile.getFirstAltAllele() << "\t50\t.\t.\tGT:GQ:PL:DP";

			//first groups
			if(hasGroups){
				for(G=0; G < groups.numGroups; ++G){
					//calculate likelihood across replicates
					lik[0] = 0.0; lik[1] = 0.0; lik[2] = 0.0;
					depth = 0;
					for(s=0; s < groups.numSamplesPerGroup[G]; ++s){
						if(!vcfFile.sampleIsMissing(samplesInGroups[G][s])){
							depth += (int) vcfFile.sampleDepth(samplesInGroups[G][s]);
							thisEm = emissionProbs[depthIndex[(int) vcfFile.sampleDepth(samplesInGroups[G][s])]];
							g = vcfFile.sampleGenotype(samplesInGroups[G][s]);
							lik[0] += thisEm[0][g]; lik[1] += thisEm[1][g]; lik[2] += thisEm[2][g];
						}
					}

					//write
					if(depth > 0)
						writeGenotypeToVcf(lik, depth, outVcf, genoString);
					else outVcf << "\t./.";
				}
			}

			//then all other samples
			for(s=0; s < numSamplesNotInGrops; ++s){
				if(vcfFile.sampleIsMissing(s)){
					outVcf << "\t./.";
				} else {
					//get likelihoods
					depth = (int) vcfFile.sampleDepth(samplesNotInGroups[s]);
					if(depth>maxDepth)
						throw "Depth " + toString(depth) + " larger than largest depths for which there are error rates provided!";
					thisEm = emissionProbs[depthIndex[depth]];
					//std::cout << "depth = " << depth << std::endl;
					g = vcfFile.sampleGenotype(samplesNotInGroups[s]);
					lik[0] = thisEm[0][g]; lik[1] = thisEm[1][g]; lik[2] = thisEm[2][g];

					//write
					writeGenotypeToVcf(lik, depth, outVcf, genoString);
				}
			}
			outVcf << "\n";
		}
		if(verbose && counter % 1000 == 0)	std::cerr << "    - read " << counter << " lines" << std::endl;
	}

	//clean up
	outVcf.close();
	delete[] depthIndex;
	delete[] samplesNotInGroups;
	if(hasGroups){
		for(G=0; G < groups.numGroups; ++G)
			delete[] samplesInGroups[G];
		delete[] samplesInGroups;
	}
	delete[] lik;
	delete[] genoString;
}

void TGenoError::writeGenotypeToVcf(double* lik, int depth, std::ofstream & outVcf, std::string* genoString){
	//variables
	static int mle = 0;
	static double qual;


	//write genotype
	mle = 0;
	if(lik[1] > lik[mle]) mle = 1;
	if(lik[2] > lik[mle]) mle = 2;
	outVcf << genoString[mle];

	//write quality
	if(mle == 0){
		if(lik[1] > lik[2]) qual = lik[0] - lik[1];
		else qual = lik[0] - lik[2];
	} else if(mle == 1){
		if(lik[0] > lik[2]) qual = lik[1] - lik[0];
		else qual = lik[1] - lik[2];
	} else {
		if(lik[0] > lik[1]) qual = lik[2] - lik[0];
		else qual = lik[2] - lik[1];
	}
	outVcf << abs(round(-10.0 * qual));

	//write likelihoods
	outVcf << ':' << abs(round(-10.0 * (lik[0]-lik[mle]))) << ',' << abs(round(-10.0 * (lik[1]-lik[mle]))) << ',' << abs(round(-10.0 * (lik[2]-lik[mle])));

	//write depth
	outVcf << ':' << depth;
}

