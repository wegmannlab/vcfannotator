/*
 * TVcfFile.h
 *
 *  Created on: Aug 8, 2011
 *      Author: wegmannd
 */

#ifndef TVCFFILE_H_
#define TVCFFILE_H_

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include "TVcfParser.h"
#include <algorithm>

typedef void (TVcfParser::*pt2Function)(TVcfLine &);
//---------------------------------------------------------------------------------------------------------
class TVcfFile_base{
public:
	std::istream* myStream;
	std::ostream* myOutStream;
	std::string fileFormat;
	TVcfColumnNumbers cols;
	TVcfParser parser;
	unsigned int numCols;
	long currentLine;
	std::vector< pt2Function > usedParsers;
	bool verbose;
	bool writeVcf;
	TVcfLine* tempLine;
	bool eof;
	double totalFileSize;

	//vector<TVcfFilter> filters;
	//bool applyFilters;

	std::vector<std::string> unknownHeader;

	TVcfFile_base(){currentLine=0; verbose=false; writeVcf=false;eof=false;numCols=-1;totalFileSize=-1;myOutStream=NULL; myStream=NULL; tempLine=NULL;};
	TVcfFile_base(std::istream & is, bool & Verbose);
	TVcfFile_base(bool & Verbose);
	virtual ~TVcfFile_base(){};
	void enableVerbose(){verbose=true;};
	void enableWriting(){writeVcf=true;};
	void setStream(std::istream & is);
	void setOutStream(std::ostream & is);

	//which parsers to use?
	void enablePositionParsing(){usedParsers.push_back(&TVcfParser::parsePosition);};
	void enableVariantParsing(){usedParsers.push_back(&TVcfParser::parseVariant);};
	void enableInfoParsing(){usedParsers.push_back(&TVcfParser::parseInfo);};
	void enableFormatParsing(){usedParsers.push_back(&TVcfParser::parseFormat);};
	void enableSampleParsing(){usedParsers.push_back(&TVcfParser::parseSamples);};

	//retrieve info
	GTLikelihoods genotypeLiklihoods(TVcfLine* line, unsigned int sample);
	GTLikelihoods genotypeLiklihoodsPhred(TVcfLine* line, unsigned int sample);
	int sampleNumber(std::string & Name);
	int numSamples();
	std::string sampleName(unsigned int num);
	bool sampleIsMissing(TVcfLine* line, unsigned int sample);
	bool sampleHasUnknownGenotype(TVcfLine* line, unsigned int sample);
	virtual std::string fieldContentAsString(std::string tag, TVcfLine* line, unsigned int sample);
	virtual int fieldContentAsInt(std::string tag, TVcfLine* line, unsigned int sample);
	virtual int depthAsIntNoCheckForMissingSample(std::string tag, TVcfLine* line, unsigned int sample);

	//modify
	void setSampleMissing(TVcfLine* line, unsigned int sample);
	void setSampleHasUndefinedGenotype(TVcfLine* line, unsigned int sample);

	//void addFilter(my_string filter);
	//void filterSamples();
	//void printFilters();

	void parseHeaderVCF_4_0();
	void writeHeaderVCF_4_0();
	void addNewHeaderLine(std::string headerLine);
	bool readLine();
	void addFormat(std::string Line){parser.addFormat(Line);};

	//modify header and columns
	void updateInfo(TVcfLine* line, std::string Id, std::string Data);
	void addToInfo(TVcfLine* line, std::string Id, std::string Data);
	void updatePL(TVcfLine* line, std::string Data, int S);
};

class TVcfFileSingleLine:public TVcfFile_base{
public:
	bool written;

	TVcfFileSingleLine(){currentLine=0; verbose=false;written=false;};
	TVcfFileSingleLine(std::istream & is, bool & Verbose);
	TVcfFileSingleLine(bool & Verbose);
	virtual ~TVcfFileSingleLine();
	void writeLine();
	bool next();
	TVcfLine* pointerToVcfLine(){return tempLine;};
	void updateInfo(std::string Id, std::string Data);
	void addToInfo(std::string Id, std::string Data);
	void updatePL(std::string & Data, int & S);
	virtual std::string fieldContentAsString(std::string tag, unsigned int sample);
	virtual int fieldContentAsInt(std::string tag, unsigned int sample);
	virtual int depthAsIntNoCheckForMissingSample(std::string tag, unsigned int sample);
	GTLikelihoods genotypeLikelihoods(unsigned int sample);
	GTLikelihoods genotypeLikelihoodsPhred(unsigned int sample);
	//variant info
	long position();
	std::string chr();
	int getNumAlleles();
	char getRefAllele();
	char getFirstAltAllele();
	char getAllele(int num);
	//sampel info
	void setSampleMissing(unsigned int sample);
	void setSampleHasUndefinedGenotype(unsigned int sample);
	bool sampleIsMissing(unsigned int sample);
	bool sampleHasUndefinedGenotype(unsigned int sample);
	bool sampleIsHomoRef(unsigned int sample);
	bool sampleIsHeteroRefNonref(unsigned int sample);
	char getFirstAlleleOfSample(unsigned int num);
	char getSecondAlleleOfSample(unsigned int num);
	short sampleGenotype(const unsigned int & num);
	float sampleGenotypeQuality(unsigned int sample);
	int sampleDepth(unsigned int sample);
	bool formatColExists(std::string tag){ return parser.formatColExists(tag, *tempLine); };
	std::string getSampleContentAt(std::string tag, unsigned int sample){
		return parser.sampleContentAt(*tempLine, tag, sample);
	}
};

class TVcfFileWindow:public TVcfFile_base{
public:
	TVcfLine** lines;
	int size, step;
	int numUsable;
	int start;
	std::string currentChr;
	long startPos, endPos;
	bool jumped, incomplete, isEmpty, willJump;
	bool tempLineFull;

	TVcfFileWindow(int Size, int Step, bool & Verbose);
	~TVcfFileWindow(){empty(); delete[] lines;}

	virtual bool advance();
	bool peekIfNextIsJump();
	void jump();
	bool initialFill();
	void empty();
	void empty(int n);
	TVcfLine* pointerToVcfLine(int posInWindow);
	TVcfLine* pointerToFirstVcfLine();
	TVcfLine* pointerToLastVcfLine();
	GTLikelihoods genotypeLikelihoodsAt(int posInWindow, int sample);
	long positionAt(int posInWindow);
	long positionOfFirst();
	long positionOfLast();
	std::string chr();
	int firstAfter(long pos);
	void addInfoToSampleAt(int posInWindow, unsigned int sample, std::string tag, std::string data);
	bool sampleIsMissingAt(int posInWindow, unsigned int sample);
	bool sampleIsHomoRefAt(int posInWindow, unsigned int sample);
	bool sampleIsHeteroRefNonrefAt(int posInWindow, unsigned int sample);
	float sampleGenotypeQualityAt(int posInWindow, unsigned int sample);
};

class TVcfFileWindowOddSizeStepOne:public TVcfFileWindow{
private:
	long oldLastToUpdatePos; //if update happend is not checked!
	int middle;
public:
	int firstToUpdate;
	int lastToUpdatePlusOne;
	bool doUpdate;

	TVcfFileWindowOddSizeStepOne(int Size, bool & Verbose);
	virtual bool advance();
};

#endif /* TVCFFILE_H_ */
