/*
 * TTrueFile.h
 *
 *  Created on: Oct 2, 2017
 *      Author: vivian
 */

#ifndef TTRUEFILE_H_
#define TTRUEFILE_H_

#include "stringFunctions.h"
#include "TParameters.h"
#include "gzstream.h"


class TTrueFile{
private:
	TParameters* myParameters;
	bool verbose;
	std::string trueFileName;
	int lineNum;
	std::istream* myStream;
	std::string trueLine;
	std::string trueGenotype;

	//stuff for trueFile = vcf
	std::vector<std::string> vcfGT;
	std::vector<std::string> allelesNum;
	std::vector<std::string> altAlleles;


	bool open();

public:
	std::vector<std::string> trueVec;
	std::vector<std::string> order;
	std::string trueChrom;
	int truePos;
	bool vcfVsFlatCalls, twoSingleVcf;


	void setOrder();
	void setOrder(std::string sampleName);
	void checkColumnNumbers();
	bool parseLine();
	std::string getTrueGenotype(int orderIndex);


	TTrueFile(TParameters & Params, bool Verbose);
	~TTrueFile(){
		delete myStream;
	}


};


#endif /* TTRUEFILE_H_ */
