/*
 * TTranscript.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#include "TTranscript.h"

TTranscript::TTranscript(long Start, long End, bool Strand){
	start=Start;
	end=End;
	strand=Strand;
	addExon(Start, End, Strand);
};

bool TTranscript::addExon(long Start, long End, bool Strand){
	if(Strand!=strand) return false;
	if(Start<start) start=Start;
	if(End>end) end=End;
	exons.push_back(TTranscriptExon(Start, End));
	return true;
};

long TTranscript::length(){
	long tot=0;
	for(std::vector<TTranscriptExon>::iterator it=exons.begin(); it!=exons.end(); ++it)
		tot+=(*it).length();
	return tot;
};

int TTranscript::codonPosition(long & pos){
	std::cerr << start << " " << end << std::endl;
	if(pos < start || pos > end) return -1;
	long p=0;
	if(strand){
		for(std::vector<TTranscriptExon>::iterator it=exons.begin(); it!=exons.end(); ++it){
			if(pos < it->start) return -1;
			else if(pos > it->end) p+=(it->end - it->start);
			else {
				p+=(pos - it->start);
				return p % 3;
			}
		}
	} else {
		for(std::vector<TTranscriptExon>::reverse_iterator it=exons.rbegin(); it!=exons.rend(); ++it){
			if(pos > it->end) return -1;
			else if(pos < it->start) p+=(it->end - it->start);
			else {
				p+=(it->end - pos);
				return p % 3;
			}
		}
	}
	return p % 3;
}
//--------------------------------------------------------------------
bool TTranscriptChromosome::addExon(std::string Name, long Start, long End, bool Strand){
	sorted=false;
	std::map<std::string, TTranscript>::iterator it;
	it=transcripts.find(Name);
	if(it==transcripts.end()){
		//add new transcript
		transcripts.insert(std::pair<std::string, TTranscript>(Name, TTranscript(Start, End, Strand)));
		return true;
	} else return it->second.addExon(Start, End, Strand);
}

void TTranscriptChromosome::checkTranscripts(){
	for(std::map<std::string, TTranscript>::iterator it=transcripts.begin(); it!=transcripts.end(); ++it){
		if(!it->second.length() % 3 != 0)
			throw "The length of transcript '" + it->first + "' is not a multipel of 3!";
	}
}
long TTranscriptChromosome::numTranscripts(){
	return transcripts.size();
}

bool transcriptSortingFunction(TTranscript* t1, TTranscript* t2){
	//function to sort transcripts
	//return true if t1 < t2
	if(t1->start < t2->start) return true;
	else if(t1->start > t2->start) return false;
	//equal start -> shorter one first
	else if(t1->end < t2->end) return true;
	else return false;
}
void TTranscriptChromosome::sortTranscripts(){
	if(!sorted){
		transcripts_sorted.clear();
		for(std::map<std::string, TTranscript>::iterator it=transcripts.begin(); it!=transcripts.end(); ++it)
			transcripts_sorted.push_back(&(it->second));
		std::sort(transcripts_sorted.begin(), transcripts_sorted.end(), transcriptSortingFunction);
		sorted=true;
	}
}
void TTranscriptChromosome::fillCodonPositions(long & pos, std::vector<int> & codonPos){
	int p;
	for(std::map<std::string, TTranscript>::iterator it=transcripts.begin(); it!=transcripts.end(); ++it){
		p=it->second.codonPosition(pos);
		if(p>=0){
			//already in list?
			bool exists=false;
			for(std::vector<int>::iterator i=codonPos.begin(); i!=codonPos.end(); ++i){
				if(*i == p){
					exists=true;
					break;
				}
			}
			if(!exists) codonPos.push_back(p);
			if(codonPos.size()==3) return;
		}
	}
}

//--------------------------------------------------------------------
TTranscripts::TTranscripts(std::string filename, bool & Verbose){
	verbose=Verbose;
	readTranscripts(filename);
};

void TTranscripts::readTranscripts(std::string filename){
	if(verbose) std::cerr << " - Reading transcripts from file '" << filename << "' ..." << std::flush;
	std::ifstream f;
	f.open(filename.c_str());
	if(!f) throw "File '" + filename + "' could not be opened!";
	std::string buf, chromosome, name;
	int pos, chr_read;
	bool strand;
	long line=0;
	std::vector<std::string> temp;
	std::map<std::string, TTranscriptChromosome>::iterator it;
	while(f.good() && !f.eof()){
		++line;
		getline(f, buf);
		if(!buf.empty()){
			trimString(buf);
			chromosome=extractBeforeWhiteSpace(buf);
			buf.erase(0,1);
			//remove "chr" if present
			pos=chromosome.find("chr");
			if(pos==0) chromosome.erase(0, 3);
			chr_read=stringToInt(chromosome);
			if(chr_read>0) chromosome=chr_read;
			//find appropriate chromosome
			it=chromosomes.find(chromosome);
			if(it==chromosomes.end()){
				//add new chromosome
				chromosomes.insert(std::pair<std::string, TTranscriptChromosome>(chromosome, TTranscriptChromosome()));
				it=chromosomes.find(chromosome);
			}
			//add exon
			fillVectorFromStringWhiteSpace(buf, temp);
			if(temp.size()<4) throw "Wrong number of columns on line " + toString(line) + " in file '"+ filename +"'!";
			if(temp[3]=="+") strand=true;
			else if(temp[3]=="-") strand=false;
			else throw "Unknown strand " + temp[3] + " on line " + toString(line) + " in file '"+ filename +"'!";
			trimString(temp[2]);
			if(!it->second.addExon(temp[2], stringToLong(temp[0]), stringToLong(temp[1]), strand)) throw "Exon of transcript '" + temp[2] + "' with wrong strand on line " + toString(line) + " in file '"+ filename +"'!";
		}
	}
	f.close();
	if(verbose) std::cerr << " done!" << std::endl;
	if(verbose) std::cerr << "   -> Read " << numTranscripts() << " transcripts on " << chromosomes.size() << " chromosomes." << std::endl;

	//check and sort
	checkTranscripts();
	sortTranscripts();
}

void TTranscripts::checkTranscripts(){
	if(verbose) std::cerr << " - Checking transcripts ..." << std::flush;
	for(std::map<std::string, TTranscriptChromosome>::iterator it=chromosomes.begin(); it!=chromosomes.end(); ++it){
		it->second.checkTranscripts();
	}
	if(verbose) std::cerr << " done!" << std::endl;
}

long TTranscripts::numTranscripts(){
	long tot=0;
	for(std::map<std::string, TTranscriptChromosome>::iterator it=chromosomes.begin(); it!=chromosomes.end(); ++it)
		tot+=it->second.numTranscripts();
	return tot;
}

void TTranscripts::sortTranscripts(){
	if(verbose) std::cerr << " - Sorting transcripts ..." << std::flush;
	for(std::map<std::string, TTranscriptChromosome>::iterator it=chromosomes.begin(); it!=chromosomes.end(); ++it)
		it->second.sortTranscripts();
	if(verbose) std::cerr << " done!" << std::endl;
}

void TTranscripts::fillCodonPositions(std::string & chr, long & pos, std::vector<int> & codonPos){
	codonPos.clear();
	//find chromosome
	std::map<std::string, TTranscriptChromosome>::iterator it=chromosomes.find(chr);
	if(it!=chromosomes.end()) it->second.fillCodonPositions(pos, codonPos);
}



















