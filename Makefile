#make file for vcfAnnotator

SRC = $(wildcard *.cpp) $(wildcard *.C) 
OBJ = $(SRC:%.cpp=%.o)

BIN = vcfAnnotator

all:	$(BIN)

$(BIN):	$(OBJ)
	$(CXX) -O3 -o $(BIN) $(OBJ) -lz 

%.o: %.cpp
	$(CXX) -O3 -c -std=c++1y $< -o $@

clean:
	rm -rf $(OBJ) $(BIN)
