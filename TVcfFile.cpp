/*
 * TVcfFile.cpp
 *
 *  Created on: Aug 8, 2011
 *      Author: wegmannd
 */

#include "TVcfFile.h"

//--------------------------------------------------------------------
TVcfFile_base::TVcfFile_base(std::istream & is, bool & Verbose){
	verbose=Verbose;
	eof=false;
	setStream(is);
}

TVcfFile_base::TVcfFile_base(bool & Verbose){
	verbose=Verbose;
	eof=false;
	currentLine=-1;
	myStream=NULL;
	myOutStream=NULL;
	numCols=-1;
	totalFileSize=-1;
	tempLine=NULL;
	writeVcf=false;
}

void TVcfFile_base::setStream(std::istream & is){
	myStream=&is;

	//first line contains fileformat version
	std::string temp;
	getline(*myStream, temp);
	currentLine=1;

	if(stringContains(temp, "##fileformat")){
		fileFormat=extractAfter(temp, '=');
		trimString(fileFormat);
		if(fileFormat!="VCFv4.0" && fileFormat!="VCFv4.1" && fileFormat!="VCFv4.2" && fileFormat!="VCFv4.3")
			std::cerr << "WARNING: VCF file is not of version 4.0, 4.1, 4.2 or 4.3!";
	} else throw "Missing VCF file format specification on first line!";

	//parse header
	parseHeaderVCF_4_0();
}

void TVcfFile_base::setOutStream(std::ostream & os){
	myOutStream=&os;
}

void TVcfFile_base::parseHeaderVCF_4_0(){
	//read the header of the vcf file and stop after that
	if(verbose) std::cerr << "   -> Reading header of a VCF file in '" << fileFormat << "' format ... " << std::flush;
	std::string temp, buf;
	parser.setColNumbers(&cols);
	bool headerRowRead=false;
	while(!myStream->eof() && myStream->peek()=='#'){
		++currentLine;
		getline(*myStream, temp);
		if(stringContains(temp, "#CHROM")){
			if(headerRowRead) throw "Found more than one header row!";
			//analyze header: save which column contains the chromosome, position, refbase, altbases, info, format and species
			trimString(temp);
			int i=0;
			while(!temp.empty()){
				buf=extractBeforeWhiteSpace(temp);
				trimString(buf);
				temp.erase(0,1);
				if(i<cols.FirstInd) cols.set(buf, i);
				else parser.addSample(buf);
				++i;
			}
			numCols=i;
			headerRowRead=true;
		} else {
			//parse all other header lines
			if(temp.find("##FORMAT")==0) parser.addFormat(temp);
			else if(temp.find("##INFO")==0) parser.addInfo(temp);
			else unknownHeader.push_back(temp);
		}
	}
	//check cols
	cols.check();
	if(verbose) std::cerr << " done!" << std::endl << "   -> File contains " << parser.samples.size() << " samples." << std::endl;
	if(parser.samples.size()<1) throw "VCF file contains no samples!";
}

void TVcfFile_base::addNewHeaderLine(std::string headerLine){
	unknownHeader.push_back(headerLine);
}

/*
void TVcfFile::addFilter(my_string filter){
	filters.push_back(TVcfFilter(filter, &currentLine));
	applyFilters=true;
}

void TVcfFile::filterSamples(){
	if(!samplesParsed) throw TException("Unable to filter samples: samples not parsed yet!");
	if(applyFilters){
		for(vector<TVcfFilter>::iterator itF=filters.begin(); itF!=filters.end(); ++itF){
			for(vector<TVcfSample>::iterator it=samples.begin(); it!=samples.end(); ++it)
				it->filter(&(*itF));
		}
	}
}

void TVcfFile::printFilters(){
	if(applyFilters){
		cout << " - Using the following filters:" << endl;
		for(vector<TVcfFilter>::iterator itF=filters.begin(); itF!=filters.end(); ++itF)
			itF->print();
	}
}
*/

GTLikelihoods TVcfFile_base::genotypeLiklihoods(TVcfLine* line, unsigned int s){
	return parser.genotypeLiklihoods(*line, s);
}

GTLikelihoods TVcfFile_base::genotypeLiklihoodsPhred(TVcfLine* line, unsigned int s){
	return parser.genotypeLiklihoodsPhred(*line, s);
}

int TVcfFile_base::sampleNumber(std::string & Name){
	return parser.getSampleNum(Name);
}

std::string TVcfFile_base::sampleName(unsigned int num){
	return parser.getSampleName(num);
}

int TVcfFile_base::numSamples(){
	return parser.getNumSamples();
}

bool TVcfFile_base::sampleIsMissing(TVcfLine* line, unsigned int sample){
	return parser.sampleIsMissing(*line, sample);
}

bool TVcfFile_base::sampleHasUnknownGenotype(TVcfLine* line, unsigned int sample){
	return parser.sampleHasUndefinedGenotype(*line, sample);
}

std::string TVcfFile_base::fieldContentAsString(std::string tag, TVcfLine* line, unsigned int sample){
	return parser.sampleContentAt(*line, tag, sample);
}

int TVcfFile_base::fieldContentAsInt(std::string tag, TVcfLine* line, unsigned int sample){
	return stringToInt(parser.sampleContentAt(*line, tag, sample));
}

int TVcfFile_base::depthAsIntNoCheckForMissingSample(std::string tag, TVcfLine* line, unsigned int sample){
	return stringToInt(parser.sampleContentAtNoCheckForMissingSample(*line, tag, sample));
}

void TVcfFile_base::setSampleMissing(TVcfLine* line, unsigned int sample){
	parser.setSampleMissing(*line, sample);
}
void TVcfFile_base::setSampleHasUndefinedGenotype(TVcfLine* line, unsigned int sample){
	parser.setSampleHasUndefinedGenotype(*line, sample);
}
void TVcfFile_base::writeHeaderVCF_4_0(){
	*myOutStream << "##fileformat=" << fileFormat << std::endl;
	parser.writeFormatHeader(*myOutStream);
	parser.writeInfoHeader(*myOutStream);
	//write unknown header columns
	for(std::vector<std::string>::iterator i=unknownHeader.begin(); i!=unknownHeader.end(); ++i){
		*myOutStream << *i << std::endl;
	}
	//add column description
	parser.writeColumnDescriptionHeader(*myOutStream);
}

bool TVcfFile_base::readLine(){
	std::string temp;
	do{
		if(myStream->eof()){
			eof=true;
			return false;
		}
		++currentLine;
		getline(*myStream, temp);
	} while(temp.empty());
	tempLine=new TVcfLine(temp, numCols, currentLine);

	//what to parse?
	for(std::vector<pt2Function>::iterator it=usedParsers.begin(); it!=usedParsers.end(); ++it){
		(parser.*(*it))(*tempLine);
	}
	return true;
}

void TVcfFile_base::updateInfo(TVcfLine* line, std::string id, std::string data){

}
void TVcfFile_base::addToInfo(TVcfLine* line, std::string Id, std::string Data){

}
 void TVcfFile_base::updatePL(TVcfLine* line, std::string Data, int S){

 }
//--------------------------------------------------------------------
TVcfFileSingleLine::TVcfFileSingleLine(std::istream & is, bool & Verbose){
	verbose=Verbose;
	setStream(is);
	written=true;
	eof=false;
}


TVcfFileSingleLine::TVcfFileSingleLine(bool & Verbose){
	verbose=Verbose;
	written=true;
	eof=false;
}

TVcfFileSingleLine::~TVcfFileSingleLine(){
	if(!written){
		if(writeVcf) parser.writeLine(*tempLine, *myOutStream);
		delete tempLine;
	}
}

void TVcfFileSingleLine::writeLine(){
	parser.writeLine(*tempLine, *myOutStream);
	written = true;
}

bool TVcfFileSingleLine::next(){
	//write old Line
	if(!written){
		if(writeVcf) parser.writeLine(*tempLine, *myOutStream);
		delete tempLine;
		written=true;
	}
	if(!readLine()) return false;
	else {
		written=false;
		return true;
	}
}

void TVcfFileSingleLine::updateInfo(std::string Id, std::string Data){
	if(written) throw "Can not update line, no line read!";
	parser.updateInfo(*tempLine, Id, Data);
}
void TVcfFileSingleLine::updatePL(std::string & Data, int & S){
	parser.updatePL(*tempLine, Data, S);
}
void TVcfFileSingleLine::addToInfo(std::string Id, std::string Data){
	if(written) throw "Can not update line, no line read!";
	parser.addToInfo(*tempLine, Id, Data);
}
std::string TVcfFileSingleLine::fieldContentAsString(std::string tag, unsigned int sample){
	return TVcfFile_base::fieldContentAsString(tag, tempLine, sample);
}
int TVcfFileSingleLine::fieldContentAsInt(std::string tag, unsigned int sample){
	return TVcfFile_base::fieldContentAsInt(tag, tempLine, sample);
}

int TVcfFileSingleLine::depthAsIntNoCheckForMissingSample(std::string tag, unsigned int sample){
	return TVcfFile_base::depthAsIntNoCheckForMissingSample(tag, tempLine, sample);
}

GTLikelihoods TVcfFileSingleLine::genotypeLikelihoods(unsigned int sample){
	return genotypeLiklihoods(tempLine ,sample);
}

GTLikelihoods TVcfFileSingleLine::genotypeLikelihoodsPhred(unsigned int sample){
	return genotypeLiklihoodsPhred(tempLine ,sample);
}

long TVcfFileSingleLine::position(){
	return parser.getPos(*tempLine);
}
std::string TVcfFileSingleLine::chr(){
	return parser.getChr(*tempLine);
}
int TVcfFileSingleLine::getNumAlleles(){
	return parser.getNumAlleles(*tempLine);
}
char TVcfFileSingleLine::getRefAllele(){
	return parser.getRefAllele(*tempLine);
}
char TVcfFileSingleLine::getFirstAltAllele(){
	return parser.getFirstAltAllele(*tempLine);
}
char TVcfFileSingleLine::getAllele(int num){
	return parser.getAllele(*tempLine, num);
}

void TVcfFileSingleLine::setSampleMissing(unsigned int sample){
	TVcfFile_base::setSampleMissing(tempLine, sample);
}
void TVcfFileSingleLine::setSampleHasUndefinedGenotype(unsigned int sample){
	TVcfFile_base::setSampleHasUndefinedGenotype(tempLine, sample);
}
bool TVcfFileSingleLine::sampleIsMissing(unsigned int sample){
	return TVcfFile_base::sampleIsMissing(tempLine, sample);
}
bool TVcfFileSingleLine::sampleHasUndefinedGenotype(unsigned int sample){
	return TVcfFile_base::sampleIsMissing(tempLine, sample);
}
bool TVcfFileSingleLine::sampleIsHomoRef(unsigned int sample){
	return parser.sampleIsHomoRef(*tempLine, sample);
}
bool TVcfFileSingleLine::sampleIsHeteroRefNonref(unsigned int sample){
	return parser.sampleIsHeteroRefNonref(*tempLine, sample);
}
float TVcfFileSingleLine::sampleGenotypeQuality(unsigned int sample){
	return parser.sampleGenotypeQuality(*tempLine, sample);
}
char TVcfFileSingleLine::getFirstAlleleOfSample(unsigned int num){
	return parser.getFirstAlleleOfSample(*tempLine, num);
}
char TVcfFileSingleLine::getSecondAlleleOfSample(unsigned int num){
	return parser.getSecondAlleleOfSample(*tempLine, num);
}
short TVcfFileSingleLine::sampleGenotype(const unsigned int & num){
	//NOTE: only makes sense for biallelic sites! Missing = 3
	return parser.sampleGenotype(*tempLine, num);
}

int TVcfFileSingleLine::sampleDepth(unsigned int sample){
	//return 0 if sample is missing
	if(parser.sampleIsMissing(*tempLine, sample))
		return 0;
	//chek if depth is given
	std::string DP = "DP";
	if(parser.formatColExists(DP, *tempLine))
		return stringToInt(parser.sampleContentAt(*tempLine, DP, sample));
	else return -1;
}

//--------------------------------------------------------------------
TVcfFileWindow::TVcfFileWindow(int Size, int Step, bool & Verbose){
	verbose=Verbose;
	size=Size;
	step=Step;
	start=0;
	jumped=false; incomplete=false; isEmpty=true; willJump=false;
	enablePositionParsing(); //we need positions to check things
	numUsable=0;
	eof=false;
	lines=new TVcfLine*[size];
	currentChr="";
	startPos=-99999;
	endPos=-99999;
	tempLineFull=false;
}

bool TVcfFileWindow::advance(){ //returns false except if status=ok
	willJump=false;
	jumped=false;
	incomplete=true;

	if(isEmpty){
		initialFill();
		return false;
	} else {
		startPos+=step;
		endPos+=step;
		//remove old lines
		int n=0;
		while(positionAt(n)<startPos && n<(numUsable-1)) ++n;
		if(n>0)	empty(n);
		//completely empty? refill!
		if(isEmpty){
			initialFill();
			return false;
		} else {
			//add new lines
			int newLines=0;
			while(newLines < step){
				if(!tempLineFull){
					if(!readLine()){
						break;
					}
					tempLineFull=true;
				}
				//check if we are on the same chromosome and the position fits
				if(currentChr != tempLine->chr){
					if(newLines==0){
						jump();
						return false;
					}
					willJump=true;
					return false;
				}
				if(tempLine->pos > endPos){
					if(tempLine->pos > lines[(start+numUsable-1) % size]->pos+size){
						if(newLines==0){
							 jump();
							 return false;
						}
						willJump=true;
						return false;
					}
					break;
				}
				//check if vcf is sorted
				if(tempLine->pos <= lines[(start+numUsable-1) % size]->pos) throw "Error when reading vcf file: positions are not sorted on line " + toString(currentLine) + "!";
				lines[(start+numUsable) % size]=tempLine;
				tempLineFull=false;
				++numUsable;
				++newLines;
			}
		}
	}
	willJump=peekIfNextIsJump();
	if(numUsable==size){
		incomplete=false;
		return !willJump;
	}
	return false;
}

bool TVcfFileWindow::peekIfNextIsJump(){
	if(!tempLineFull){
		if(!readLine()) return true;
		tempLineFull=true;
	}
	if(currentChr != tempLine->chr) return true;
	if(tempLine->pos > endPos+size) return true;
	return false;
}

void TVcfFileWindow::jump(){
	empty();
	initialFill();
}

bool TVcfFileWindow::initialFill(){
	empty();
	isEmpty=false;
	if(!tempLineFull){
		if(!readLine()){
			incomplete=true;
			isEmpty=true;
			return false;
		}
	}
	lines[0]=tempLine;
	//did we jump or was it a non-overlappign shift?
	if(currentChr!=lines[0]->chr || lines[0]->pos>endPos+size) jumped=true;
	else jumped=false;
	//set pointers
	start=0;
	numUsable=1;
	startPos=lines[0]->pos;
	currentChr=lines[0]->chr;
	endPos=startPos+size-1;
	tempLineFull=false;
	//fill rest
	while(numUsable < size){
		if(!readLine()){
			incomplete=true;
			return false;
		}
		//check if we are on the same chromosome and the position fits
		if(currentChr != tempLine->chr){
			willJump=true;
			incomplete=true;
			tempLineFull=true;
			return false;
		}
		if(tempLine->pos > endPos){
			if(tempLine->pos > lines[(start+numUsable-1) % size]->pos+size) willJump=true;
			else willJump=false;
			incomplete=true;
			tempLineFull=true;
			return false;
		}
		//check if vcf is sorted
		if(tempLine->pos <= lines[numUsable-1]->pos) throw "Error when reading vcf file: positions are not sorted on line " + toString(currentLine) + "!";
		lines[numUsable]=tempLine;
		++numUsable;
	}
	incomplete=false;

	//check if we will jump! read next line...
	willJump=peekIfNextIsJump();
	return !willJump;
}

void TVcfFileWindow::empty(){
	empty(numUsable);
}

void TVcfFileWindow::empty(int n){
	incomplete=true;
	n=std::min(n, numUsable);
	if(writeVcf){
		for(int i=start; i<(start+n); ++i){
			parser.writeLine(*(lines[i % size]), *myOutStream);
		}
	}
	for(int i=start; i<(start+n); ++i){
		delete lines[i % size];
	}
	numUsable=numUsable-n;
	if(numUsable==0){
		isEmpty=true;
	} else {
		//cerr << "EMPTYING " << n << ": old: " << startPos << "-" << endPos << "; start=" << start << "; new: ";
		start=(start+n) % size;
		//cerr << startPos << "-" << endPos << "; start=" << start << endl;
	}
}

TVcfLine* TVcfFileWindow::pointerToVcfLine(int posInWindow){
	return lines[(start+posInWindow) % size];
}
TVcfLine* TVcfFileWindow::pointerToFirstVcfLine(){
	return lines[start];
}
TVcfLine* TVcfFileWindow::pointerToLastVcfLine(){
	return lines[(start+numUsable-1) % size];
}
GTLikelihoods TVcfFileWindow::genotypeLikelihoodsAt(int posInWindow, int sample){
	return genotypeLiklihoods(pointerToVcfLine(posInWindow) ,sample);
}
long TVcfFileWindow::positionAt(int posInWindow){
	return parser.getPos(*pointerToVcfLine(posInWindow));
}
long TVcfFileWindow::positionOfFirst(){
	return parser.getPos(*pointerToFirstVcfLine());
}
long TVcfFileWindow::positionOfLast(){
	return parser.getPos(*pointerToLastVcfLine());
}
std::string TVcfFileWindow::chr(){
	return parser.getChr(*pointerToFirstVcfLine());
}

int TVcfFileWindow::firstAfter(long pos){
	for(int i=0; i<numUsable; ++i){
		if(positionAt(i)>pos) return i;
	}
	return -1;
}

void TVcfFileWindow::addInfoToSampleAt(int posInWindow, unsigned int sample, std::string tag, std::string data){
	parser.addInfoToSample(*pointerToVcfLine(posInWindow), sample, tag, data);
}

bool TVcfFileWindow::sampleIsMissingAt(int posInWindow, unsigned int sample){
	return sampleIsMissing(pointerToVcfLine(posInWindow) ,sample);
}

bool TVcfFileWindow::sampleIsHomoRefAt(int posInWindow, unsigned int sample){
	return parser.sampleIsHomoRef(*pointerToVcfLine(posInWindow), sample);
}
bool TVcfFileWindow::sampleIsHeteroRefNonrefAt(int posInWindow, unsigned int sample){
	return parser.sampleIsHeteroRefNonref(*pointerToVcfLine(posInWindow), sample);
}
float TVcfFileWindow::sampleGenotypeQualityAt(int posInWindow, unsigned int sample){
	return parser.sampleGenotypeQuality(*pointerToVcfLine(posInWindow), sample);
}
//--------------------------------------------------------------------
TVcfFileWindowOddSizeStepOne::TVcfFileWindowOddSizeStepOne(int Size, bool & Verbose):TVcfFileWindow(Size, 1, Verbose){
	if((size % 2) == 0) throw "TVcfFileWindowOddSizeStepOne initialized with an even size!";
	if(size <3) throw "TVcfFileWindowOddSizeStepOne initialized with a size < 3!";
	oldLastToUpdatePos=-99999;
	middle=floor(size/2.0);
	doUpdate=false;
	firstToUpdate=-1;
	lastToUpdatePlusOne=-1;
}

bool TVcfFileWindowOddSizeStepOne::advance(){
	bool temp=TVcfFileWindow::advance();

	//Figure out from where to where we have to update such that:
	// - each position is updated exactly once
	// - each Position is updated when in the middle, whenever possible
	// - helpful if one need to compute something for a positions that depends on a symmetric window around it
	// - the computed positions are internal positions, used with the *At() functions
	doUpdate=true;

	//figure out start
	if(jumped) firstToUpdate=0;
	else firstToUpdate=firstAfter(oldLastToUpdatePos);
	if(firstToUpdate < 0) doUpdate=false;

	//figure out end
	if(willJump || eof)	lastToUpdatePlusOne=numUsable;
	else {
		//until middle
		if(incomplete){
			lastToUpdatePlusOne=firstAfter(startPos + middle);
			if(lastToUpdatePlusOne < 0){
				lastToUpdatePlusOne=numUsable;
			}
		} else lastToUpdatePlusOne = middle + 1;
	}
	if(firstToUpdate>=lastToUpdatePlusOne) doUpdate=false;
	if(doUpdate) oldLastToUpdatePos=positionAt(lastToUpdatePlusOne-1);
	return temp;
}


