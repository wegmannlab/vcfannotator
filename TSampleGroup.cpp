/*
 * TSampleGroup.cpp
 *
 *  Created on: Aug 9, 2011
 *      Author: wegmannd
 */

#include "TSampleGroup.h"

//--------------------------------------------------
//TSampleGroup
//--------------------------------------------------

TSampleGroup::TSampleGroup(TVcfFileSingleLine* VcfFile, std::string & Samples){
	vcfFile=VcfFile;
	std::string temp;
	while(!Samples.empty()){
		temp=extractBefore(Samples, ',');
		trimString(temp);
		Samples.erase(0,1);
		if(!temp.empty()){
			trimString(temp);
			sampleNames.push_back(temp);
		}
	}

	//store numbers as array for faster parsing
	numSamples = sampleNames.size();
	samples = new unsigned int[numSamples];
	int i=0;
	for(std::vector<std::string>::iterator it = sampleNames.begin(); it != sampleNames.end(); ++it, ++i){
		samples[i] = vcfFile->sampleNumber(*it);
	}
}

TSampleGroup::~TSampleGroup(){
	delete[] samples;
}

std::string TSampleGroup::getListOfSampleNames(){
	std::string s;
	bool first=true;
	for(std::vector<std::string>::iterator it = sampleNames.begin(); it != sampleNames.end(); ++it){
		if(first) first=false;
		else s += ",";
		s += *it;
	}
	return s;
}

void TSampleGroup::fillAlleleCountsFromGT(int & numRef, int & numAlt){
	numRef = 0; numAlt = 0;
	for(int i=0; i<numSamples; ++i){
		if(!vcfFile->sampleIsMissing(samples[i])){
			if(vcfFile->sampleIsHomoRef(samples[i])) numRef +=2;
			else if(vcfFile->sampleIsHeteroRefNonref(samples[i])){
				++numRef; ++numAlt;
			} else numAlt += 2;
		}
	}
}

void TSampleGroup::fillAlleleCountsFromGP(int & numRef, int & numAlt, double & cutOff){
	numRef = 0; numAlt = 0;
	std::vector<double> gp;
	int g;
	for(int i=0; i<numSamples; ++i){
		fillVectorFromString(vcfFile->getSampleContentAt("GP", samples[i]), gp, ',');
		for(g=0; g<3; ++g){
			if(gp[g] > cutOff){
				numRef += 2 - g;
				numAlt += g;
			}
		}
	}
}

void TSampleGroup::addSamplesToVector(std::vector<int> & vec){
	for(int i=0; i<numSamples; ++i)
		vec.push_back(samples[i]);
}

void TSampleGroup::addSamplesToArray(unsigned int* array, int & index){
	for(int i=0; i<numSamples; ++i, ++index)
		array[index] = samples[i];
}

bool TSampleGroup::sampleInGroup(const int & sample){
	for(int i=0; i<numSamples; ++i){
		if(samples[i] == sample)
			return true;
	}
	return false;
}

//--------------------------------------------------
//TSampleGroups
//--------------------------------------------------
TSampleGroups::TSampleGroups(){
	vcfFile = NULL;
	numSamples = 0;
	numGroups = 0;
	numSamplesPerGroup = NULL;
	pointerToSampleVectors = NULL;
	initialized = false;
}

TSampleGroups::TSampleGroups(TVcfFileSingleLine* VcfFile, std::string Samples, bool verbose){
	initialize(VcfFile, Samples, verbose);
}

void TSampleGroups::initialize(TVcfFileSingleLine* VcfFile, std::string Samples, bool verbose){
	//groups should have format [Sample1,Sampl5],[Sample6,Sample7]
	vcfFile = VcfFile;
	if(verbose) std::cerr << "    - Parsing groups:" << std::endl;
	//parse samples enclosed in []
	trimString(Samples);
	numSamples = 0;
	while(!Samples.empty()){
		if(Samples[0]!='[') throw "Unable to understand sample groups: missing [!";
		//find closing ]
		std::size_t pos = Samples.find_first_of(']');
		if(pos == std::string::npos) throw "Unable to understand sample groups: missing ]!";
		std::string tmp = Samples.substr(1, pos-1);
		trimString(tmp);
		if(tmp.empty()) throw "Unable to understand sample groups: group is empty!";
		groups.push_back(new TSampleGroup(vcfFile, tmp));
		Samples.erase(0,pos+2);
		trimString(Samples);
		numSamples += (*groups.rbegin())->numSamples;
	}
	numGroups = groups.size();

	//fill map
	numSamplesPerGroup = new int[numGroups];
	pointerToSampleVectors = new unsigned int*[numGroups];
	initialized = true;
	int g=0;
	for(it=groups.begin(); it!=groups.end(); ++it, ++g){
		numSamplesPerGroup[g] = (*it)->numSamples;
		pointerToSampleVectors[g] = (*it)->samples;
	}

	//print groups
	if(verbose){
		int i=1;
		for(it=groups.begin(); it!=groups.end(); ++it, ++i){
			std::cerr << "       - Group " << i << ": " << (*it)->getListOfSampleNames() << std::endl;
		}
	}
}

std::string TSampleGroups::getGroupString(int group){
	return groups[group]->getListOfSampleNames();
}

void TSampleGroups::fillCurrentAlleleCountsGT(int & numRef, int & numAlt){
	(*it)->fillAlleleCountsFromGT(numRef, numAlt);
}

void TSampleGroups::fillCurrentAlleleCountsGP(int & numRef, int & numAlt, double & cutOff){
	(*it)->fillAlleleCountsFromGP(numRef, numAlt, cutOff);
}

void TSampleGroups::fillVectorOfAllSamples(std::vector<int> & vec){
	vec.clear();
	for(it=groups.begin(); it!=groups.end(); ++it){
		(*it)->addSamplesToVector(vec);
	}
}

void TSampleGroups::fillArrayOfAllSamples(unsigned int* & array){
	array = new unsigned int[numSamples];
	int index=0;
	for(it=groups.begin(); it!=groups.end(); ++it){
		(*it)->addSamplesToArray(array, index);
	}
}

void TSampleGroups::fill2DArrayOfSamples(unsigned int** & array){
	array = new unsigned int*[numGroups];
	int index=0; int g=0;
	for(it=groups.begin(); it!=groups.end(); ++it, ++g){
		index = 0;
		array[g] = new unsigned int[(*it)->numSamples];
		(*it)->addSamplesToArray(array[g], index);
	}
}

bool TSampleGroups::sampleInGroup(const int & sample){
	for(it=groups.begin(); it!=groups.end(); ++it){
		if((*it)->sampleInGroup(sample))
			return true;
	}
	return false;
}
