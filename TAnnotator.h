/*
 * TAnnotator.h
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#ifndef TANNOTATOR_H_
#define TANNOTATOR_H_

#include "stringFunctions.h"
#include <vector>
#include "TTranscript.h"
#include "TGeneticCode.h"
#include "TParameters.h"
#include <iostream>
#include <fstream>
#include "TVcfFile.h"
#include "TRandomGenerator.h"
#include "TSampleGroup.h"
#include "TGenoError.h"
#include <math.h>
#include <stdlib.h>


enum functionalClass {noncoding, coding, synonymous, nonsynonymous};

class TGenotype{
	public:
		char first;
		char second;
		TGenotype(char & f, char & s){
			first=f;
			second=s;
		};
		TGenotype(char f){
			first=f;
			second=f;
		};
};

class TFastaSequence{
public:
	std::stringstream stream;
	TFastaSequence(){};
	void addHomozygous(char & f){
		if(f!='A' && f!='C' && f!='T' && f!='G')
			throw "Wrong base to add to FASTA: unknown base '"+ toString(f) +"'!";
		stream << f;
	};
	void addHeterozygous(char & f, char & s){
		if(f==s) addHomozygous(f);
		else {
			if((f=='A' && s=='G') || (f=='G' && s=='A')) stream << 'R';
			else if((f=='C' && s=='T') || (f=='T' && s=='C')) stream << 'Y';
			else if((f=='C' && s=='G') || (f=='G' && s=='C')) stream << 'S';
			else if((f=='A' && s=='T') || (f=='T' && s=='A')) stream << 'W';
			else if((f=='G' && s=='T') || (f=='T' && s=='G')) stream << 'K';
			else if((f=='A' && s=='C') || (f=='C' && s=='A')) stream << 'M';
			else throw "Unknown genotype '" + toString(f) + "/" + toString(s) +"'!";
		}
	};
	void addMissing(){stream << 'N';};
	void write(std::ofstream & out){
		out << stream.rdbuf();
	};
};

class TFastaAlignment{
public:
	int numSamples;
	long numSnps;
	std::string* names;
	TFastaSequence* fasta;

	TFastaAlignment(TVcfFileSingleLine & vcfFile, bool & verbose);
	void writePhylip(std::string & outname, bool & verbose);
	void writeNexus(std::string & outname, bool & verbose);
};


class TAnnotator{
private:
	TParameters* myParameters;
	int chr;
	std::ifstream vcfFileStream;
	std::ofstream vcfOutFilestream;
	bool verbose;
	TRandomGenerator* randomGenerator;
	bool randomGeneratorInitialized;

	void prepareVcfInput(TVcfFile_base & vcfFile);
	void prepareVcfOutput(TVcfFile_base & vcfFile);
	void initializeRandomGenerator();
	void makeCounts(int countsAll[10][10], long countStats[12], bool & distinguishHomos, bool genotypeClass[3], int & ref, int & call);
	void openTrueFile(std::istream* myStream, std::string trueFileName, bool twoSingleVCF);
	void fillPositionsMap(std::map <std::string, int> & positionsToConsider);
	void defineExpectedFormats(bool & twoSingleVCF);
	void checkColumnNumbers(std::string & trueLine, std::vector<std::string> & trueVec, bool twoSingleVCF, int trueLineNum, int numSamples);
	void makeQualCounts(long qualAlleleCounts[4][101][2], bool & distinguishHomos, bool genotypeClass[3], int & ref, int & call, int & qual);
	std::pair<char, char> getGenotypeFromIndex(int index);
	void fillEmissionLikelihoodMatrix(TSquareMatrixStorage & likelihood, const double & error);

	void calculateQ_epsilon(double* Q, const int & maxDepth, const std::vector<short*> & genotypes, const std::vector<int*> & depths, double*** weights, double*** emissionProbsLog, int numSamples, int* groupOfSample);
	void fillEmissionProbabilitiesGenoError(double** em, const double & epsilon);
	void fillEmissionProbabilitiesGenoErrorLog(double** em, const double & epsilon);

public:

	TAnnotator(TParameters & Params, bool & Verbose);
	~TAnnotator(){if(randomGeneratorInitialized) delete randomGenerator;};
	void deriveSynonymouyNonsynonymousStatusFromTranscripts();
	bool isSynonymous(TGeneticCode & geneticCode, int & codonPos, char & ref, std::vector<char> & pos0, std::vector<char> & pos1, std::vector<char> & pos2);
	void updateFunctionalClass(TVcfFile_base & vcfFile, TVcfLine* line, functionalClass fc);
	void annotateDistanceToClosestVariant();
	void setDistanceToNextVariant(TVcfFileWindow & vcfFile, int lineNum, double & genotypeQualityThreshold);
	void checkDVField();
	void imposingHardFiltersOnSamples();
	void convert2Plink();
	void convert2Phylip();
	void convert2Nexus();
	void convert2TreeMix();
	void convert2FModelHMM();
	void compileSFS();
	int baseToNumber(char base, std::string & marker);
	void vcfToBeagle();
	int getGenotypeIndex(char first, char second);
	void validateGenotype(std::string & gtString);
//	void makeCountsPrintStuff(int countsAll[10][10], int countStats[12], bool & distinguishHomos, bool genotypeClass[3], int & ref, int & call);
	void compareCalls();
	void compareQualityToError();
	void filterForMinimumDepth();
	void compareCallsIndividuals();
	void adjustPL();
	void assessAllelicImbalance();
	void filterAllelicImbalance();
	void simulateGenoErrorData();
	void inferGenoError();
	void recalibrateVCFGenoError();
	void vcfToInvariantBed();
	void test();
};




#endif /* TANNOTATOR_H_ */

