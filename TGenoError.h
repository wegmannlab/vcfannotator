/*
 * TGenoError.h
 *
 *  Created on: May 11, 2017
 *      Author: wegmannd
 */

#ifndef TGENOERROR_H_
#define TGENOERROR_H_

#include "TMatrix.h"
#include "TParameters.h"
#include "TSampleGroup.h"

class TGenoError{
private:
	bool verbose;
	TSampleGroups groups;

	//storage for data
	bool vcfRead;
	int* groupOfSample;
	long numSnps;
	std::vector<short*> genotypes; //missing genotype = 3
	int maxDepthPlusOne;
	int numdepthsWithData;
	int* depthsWithData;
	int* depthIndex;
	long* numDataPoints;
	std::vector<int*> depths;
	std::string outname;

	//storage for EM
	bool EMstorageInitialized;
	double** freq;
	double** newFreq;
	double* epsilon;
	double* newEpsilon;
	double*** weights;
	bool emissionProbsInitialized;
	double*** emissionProbs;

	//functions for simulations
	void fillEmissionLikelihoodMatrixSimulations(TSquareMatrixStorage & likelihood, const double & error);

	//function for inference
	void readDataFromVCF(TParameters* myParameters);
	void initializeEmissionProbabilityMatrix();
	void freeEmissionProbabilityMatrix();
	void initializeEMStorage();
	void freeEMStorage();
	void estimateInitialGenotypeFrequenciesAndErrorRates();
	void estimateEpsilonNumerically(int numIter);
	void calculateQ_epsilon(double* Q, double* thisEpsilon);
	double calculateLL(double** theseFrequencies, double* thisEpsilon);
	void fillEmissionProbabilitiesGenoError(double** em, const double & epsilon);
	void fillEmissionProbabilitiesGenoErrorLog(double** em, const double & epsilon);
	void writeGenotypeToVcf(double* lik, int depth, std::ofstream & outVcf, std::string* genoString);

public:
	TGenoError(bool Verbose);
	~TGenoError(){
		if(vcfRead){
			delete[] groupOfSample;
			delete[] depthsWithData;
			delete[] depthIndex;
			delete[] numDataPoints;

			for(std::vector<int*>::iterator itD = depths.begin(); itD != depths.end(); ++itD)
				delete[] *itD;
			for(std::vector<short*>::iterator itG = genotypes.begin(); itG != genotypes.end(); ++itG)
				delete[] *itG;
		}

		freeEMStorage();
		freeEmissionProbabilityMatrix();
	};

	void simulateGenoErrorData(TParameters* myParameters, TRandomGenerator* randomGenerator);
	void inferGenoError(TParameters* myParameters);
	void recalibrateVCFGenoError(TParameters* myParameters);
};


#endif /* TGENOERROR_H_ */
