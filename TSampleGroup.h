/*
 * TSampleGroup.h
 *
 *  Created on: Aug 9, 2011
 *      Author: wegmannd
 */

#ifndef TSAMPLEGROUP_H_
#define TSAMPLEGROUP_H_

#include <vector>
#include "TVcfFile.h"
#include "TRandomGenerator.h"

class TSampleGroup{
public:
	int numSamples;
	unsigned int* samples;
	TVcfFileSingleLine* vcfFile;
	std::vector<std::string> sampleNames;

	TSampleGroup(){
		numSamples = 0;
		vcfFile = NULL;
		samples = NULL;
	};
	TSampleGroup(TVcfFileSingleLine* VcfFile, std::string & Samples);
	~TSampleGroup();
	std::string getListOfSampleNames();
	void fillAlleleCountsFromGT(int & numRef, int & numAlt);
	void fillAlleleCountsFromGP(int & numRef, int & numAlt, double & cutOff);
	void addSamplesToVector(std::vector<int> & vec);
	void addSamplesToArray(unsigned int* array, int & index);
	bool sampleInGroup(const int & sample);
};

class TSampleGroups{
private:
	bool initialized;
public:
	std::vector<TSampleGroup*> groups;
	std::vector<TSampleGroup*>::iterator it;
	TVcfFileSingleLine* vcfFile;
	int numGroups;
	int numSamples;
	int* numSamplesPerGroup;
	unsigned int** pointerToSampleVectors;


	TSampleGroups();
	TSampleGroups(TVcfFileSingleLine* VcfFile, std::string Samples, bool verbose);
	~TSampleGroups(){
		if(initialized){
			for(it = groups.begin(); it!=groups.end(); ++it) delete *it;
			delete[] numSamplesPerGroup;
			delete[] pointerToSampleVectors;
		}
	};

	void initialize(TVcfFileSingleLine* VcfFile, std::string Samples, bool verbose);
	void getNumSamples();
	std::string getGroupString(int group);
	void begin(){it = groups.begin();};
	bool next(){++it; if(it==groups.end()) return false; else return true;};
	std::string firstSampleInCurrentGroup(){ return (*it)->sampleNames[0]; };
	void fillCurrentAlleleCountsGT(int & numRef, int & numAlt);
	void fillCurrentAlleleCountsGP(int & numRef, int & numAlt, double & cutOff);
	void fillVectorOfAllSamples(std::vector<int> & vec);
	void fill2DArrayOfSamples(unsigned int** & array);
	void fillArrayOfAllSamples(unsigned int* & array);
	bool sampleInGroup(const int & sample);
};


#endif /* TSAMPLEGROUP_H_ */
