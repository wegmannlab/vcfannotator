/*
 * TTranscript.h
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#ifndef TTRANSCRIPT_H_
#define TTRANSCRIPT_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include "stringFunctions.h"


class TTranscriptExon{
public:
	long start;
	long end;

	TTranscriptExon(long Start, long End){
		start=Start;
		end=End;
	};
	long length(){ return end-start;};
};

class TTranscript{
public:
	long start;
	long end;
	bool strand;
	std::vector<TTranscriptExon> exons;

	TTranscript(long Start, long End, bool Strand);
	bool addExon(long Start, long End, bool Strand);
	long length();
	int codonPosition(long & pos);
};

class TTranscriptChromosome{
public:
	std::map<std::string, TTranscript> transcripts;
	std::vector <TTranscript*> transcripts_sorted;
	bool sorted;

	TTranscriptChromosome(){sorted=false;};
	~TTranscriptChromosome(){};
	bool addExon(std::string Name, long Start, long End, bool Strand);
	void checkTranscripts();
	long numTranscripts();
	void sortTranscripts();
	void fillCodonPositions(long & pos, std::vector<int> & codonPos);
};

class TTranscripts{
public:
	std::map<std::string, TTranscriptChromosome> chromosomes;
	bool verbose;

	TTranscripts(){verbose=false;};
	TTranscripts(std::string filename,bool & Verbose);
	void readTranscripts(std::string filename);
	void checkTranscripts();
	long numTranscripts();
	void sortTranscripts();
	void fillCodonPositions(std::string & chr, long & pos, std::vector<int> & codonPos);
};

#endif /* TTRANSCRIPT_H_ */
