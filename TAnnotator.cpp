/*
 * Tannotator.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */
#include "TAnnotator.h"
#include <algorithm>
#include <typeinfo>
#include <sstream>
#include "gzstream.h"
#include "TTrueFile.h"


//---------------------------------------------------
//TFastaAlignment
//---------------------------------------------------
TFastaAlignment::TFastaAlignment(TVcfFileSingleLine & vcfFile, bool & verbose){
	if(verbose) std::cerr << "    - Parsing VCF file ..." << std::flush;
	//enable parsers
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//prepare storage
	numSamples=vcfFile.numSamples();
	fasta = new TFastaSequence[numSamples];
	names=new std::string[numSamples];
	int s;
	for(s=0; s<numSamples; ++s) names[s]=vcfFile.sampleName(s);

	//Loop through file and compute distance to next variant position
	char ref, alt;
	numSnps=0;
	while(vcfFile.next()){
		//check if it is biallelic and ignore others
		if(vcfFile.getNumAlleles()==2){
			++numSnps;
			//store genotypes
			ref=vcfFile.getRefAllele();
			alt=vcfFile.getFirstAltAllele();
			for(s=0; s<numSamples; ++s){
				if(vcfFile.sampleIsMissing(s)) fasta[s].addMissing();
				else if(vcfFile.sampleIsHomoRef(s)) fasta[s].addHomozygous(ref);
				else if(vcfFile.sampleIsHeteroRefNonref(s)) fasta[s].addHeterozygous(ref, alt);
				else fasta[s].addHomozygous(alt);
			}
		}
	}
	if(verbose){
		std::cerr << " done!" << std::endl;
		std::cerr << "       -> " << numSnps << " bi-allelic markers found." << std::endl;
	}
}
void TFastaAlignment::writePhylip(std::string & outname, bool & verbose){
	//open output file
	if(verbose) std::cerr << "    - Writing Phylip file to '" << outname << ".phylip" << std::endl;
	std::ofstream out((outname + (std::string) ".phylip").c_str());
	out << numSamples << "\t" << numSnps << std::endl;
	for(int s=0; s<numSamples; ++s){
		out << names[s] << "\t";
		fasta[s].write(out);
		out << std::endl;
	}
	out.close();
	if(verbose) std::cerr << " done!" << std::endl;
}
void TFastaAlignment::writeNexus(std::string & outname, bool & verbose){
	//open output file
	if(verbose) std::cerr << "    - Writing Nexus file to '" << outname << ".nexus" << std::endl;
	std::ofstream out((outname + (std::string) ".nexus").c_str());
	out << "Begin data;" << std::endl;
	out << "Dimensions ntax=" << numSamples << " nchar=" << numSnps << ";" << std::endl;
	out << "Format datatype=dna symbols='ACGT' missing='N' gap='-';" << std::endl;
	out << "Matrix" << std::endl;
	for(int s=0; s<numSamples; ++s){
		out << names[s] << "\t";
		fasta[s].write(out);
		out << std::endl;
	}
	out << ";" << std::endl << "End;" << std::endl;
	out.close();
	if(verbose) std::cerr << " done!" << std::endl;
}

//---------------------------------------------------
//TAnnotator
//---------------------------------------------------
TAnnotator::TAnnotator(TParameters & Params, bool & Verbose){
	myParameters=&Params;
	verbose=Verbose;
	randomGeneratorInitialized=false;
	randomGenerator=NULL;
	chr=-1;
}

//open input stream
void TAnnotator::prepareVcfInput(TVcfFile_base & vcfFile){
	//open input stream
	if(myParameters->parameterExists("file")){
		std::string filename=myParameters->getParameterString("file");
		if(verbose) std::cerr << " - Reading vcf from file '" << filename << "':" << std::endl;
		vcfFileStream.open(filename.c_str());
		if(!vcfFileStream) throw "Failed to open file '" + filename + "'!";
		vcfFile.setStream(vcfFileStream);
	} else {
		if(verbose) std::cerr << " - Reading vcf from standard input:" << std::endl;
		vcfFile.setStream(std::cin);
	}
}

//open output stream
void TAnnotator::prepareVcfOutput(TVcfFile_base & vcfFile){
	if(!myParameters->parameterExists("novcf")){
		vcfFile.enableWriting();
		if(myParameters->parameterExists("outname")){
			std::string filename=myParameters->getParameterString("outname");
			if(verbose) std::cerr << " - Writing resulting vcf to file '" << filename << "'." << std::endl;
			vcfOutFilestream.open(filename.c_str());
			if(!vcfOutFilestream) throw "Failed to open file '" + filename + "'!";
			vcfFile.setOutStream(vcfOutFilestream);
		} else {
			if(verbose) std::cerr << " - Writing resulting vcf to standard output." << std::endl;
			vcfFile.setOutStream(std::cout);
		}
	}
}

void TAnnotator::initializeRandomGenerator(){
	if(!randomGeneratorInitialized){
		randomGenerator=new TRandomGenerator();
		if(verbose) std::cerr << " - Randomgenerator initialized with seed " << randomGenerator->usedSeed << std::endl;
		randomGeneratorInitialized=true;
	}
}

void TAnnotator::deriveSynonymouyNonsynonymousStatusFromTranscripts(){
	TTranscripts transcripts(myParameters->getParameterString("transcripts"), verbose);
	TGeneticCode geneticCode;

	//open vcf file
	TVcfFileWindow vcfFile(5, 1, verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableVariantParsing();
	vcfFile.enableInfoParsing();

	//prepare output vcf
	prepareVcfOutput(vcfFile);
	vcfFile.writeHeaderVCF_4_0();


	//save alleles at plus minus two positions and ALWAYS update all future positions to make sure gaps and chromosome ends are

	bool continueParsing=true;
	functionalClass fc;
	TVcfLine* thisLine;
	std::vector<int> codonPos;

	//start loop
	do {
		//std::cerr << "New Loop -> " << std::flush;
		if(!vcfFile.advance()){
			//std::cerr << "false -> " << std::flush;
			//check status
			if(vcfFile.eof) continueParsing=false;
			if(vcfFile.isEmpty)	continue;
			if(vcfFile.incomplete){
				//std::cerr << "incomplete -> " << std::flush;
				//difficult, check what can be done
				//since we move by one position only, the first time a window is incomplete means no new information
				// -> always only update last position
				//exception: after a jump
				//I'm lazy -> only mark coding vs non-cosing and skip the disection by position
				if(vcfFile.jumped){
					for(int i=0; i<vcfFile.numUsable; ++i){
						transcripts.fillCodonPositions(vcfFile.pointerToVcfLine(i)->chr, vcfFile.pointerToVcfLine(i)->pos, codonPos);
						if(codonPos.size()>0) updateFunctionalClass(vcfFile, vcfFile.pointerToVcfLine(i), coding);
						else updateFunctionalClass(vcfFile, vcfFile.pointerToVcfLine(i), noncoding);
					}
				} else {
					transcripts.fillCodonPositions(vcfFile.pointerToLastVcfLine()->chr, vcfFile.pointerToLastVcfLine()->pos, codonPos);
					if(codonPos.size()>0) updateFunctionalClass(vcfFile, vcfFile.pointerToLastVcfLine(), coding);
					else updateFunctionalClass(vcfFile, vcfFile.pointerToLastVcfLine(), noncoding);
				}
				continue;
			}
			if(vcfFile.jumped){
				//std::cerr << "jumped -> " << std::flush;
				//position 0 -> can only do complete analysis if codon position is 0
				thisLine=vcfFile.pointerToVcfLine(0);
				transcripts.fillCodonPositions(thisLine->chr, thisLine->pos, codonPos);
				fc=noncoding;
				if(codonPos.size()>0){
					fc=coding;
					for(std::vector<int>::iterator it=codonPos.begin(); it!=codonPos.end(); ++it){
						if(*it == 0){
							if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], geneticCode.possibleBases, vcfFile.pointerToVcfLine(1)->bases, vcfFile.pointerToVcfLine(2)->bases))
								fc=nonsynonymous;
							else fc=synonymous;
						}
					}
				}
				updateFunctionalClass(vcfFile, thisLine, fc);

				//1st -> can only do complete analysis if codon position is 0 or 1
				thisLine=vcfFile.pointerToVcfLine(1);
				transcripts.fillCodonPositions(thisLine->chr, thisLine->pos, codonPos);
				fc=noncoding;
				if(codonPos.size()>0){
					fc=coding;
					for(std::vector<int>::iterator it=codonPos.begin(); it!=codonPos.end(); ++it){
						switch(*it){
						case 0:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], geneticCode.possibleBases, vcfFile.pointerToVcfLine(1)->bases, vcfFile.pointerToVcfLine(2)->bases))
									 fc=nonsynonymous;
								 else if(fc<synonymous) fc=synonymous;
								 break;
						case 1:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], vcfFile.pointerToVcfLine(0)->bases, geneticCode.possibleBases, vcfFile.pointerToVcfLine(2)->bases))
									 fc=nonsynonymous;
								 else if(fc<synonymous) fc=synonymous;
								 break;
						}
						if(fc==nonsynonymous) break;
					}
				}
				updateFunctionalClass(vcfFile, thisLine, fc);
			}

		} //end status checking
		//everything is fine -> do 2nd one completely
		thisLine=vcfFile.pointerToVcfLine(2);
		transcripts.fillCodonPositions(thisLine->chr, thisLine->pos, codonPos);
		fc=noncoding;
		if(codonPos.size()>0){
			fc=synonymous;
			for(std::vector<int>::iterator it=codonPos.begin(); it!=codonPos.end(); ++it){
				switch(*it){
					case 0:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], geneticCode.possibleBases, vcfFile.pointerToVcfLine(3)->bases, vcfFile.pointerToVcfLine(4)->bases))
								 fc=nonsynonymous;
							 break;
					case 1:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], vcfFile.pointerToVcfLine(1)->bases, geneticCode.possibleBases, vcfFile.pointerToVcfLine(3)->bases))
								 fc=nonsynonymous;
							 break;
					case 2:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], vcfFile.pointerToVcfLine(0)->bases, vcfFile.pointerToVcfLine(1)->bases,  geneticCode.possibleBases))
								 fc=nonsynonymous;
							 break;
				}
				if(fc==nonsynonymous) break;
			}
		}
		updateFunctionalClass(vcfFile, thisLine, fc);

		//do 3rd -> cannot determien if codon possition is 0
		thisLine=vcfFile.pointerToVcfLine(3);
		transcripts.fillCodonPositions(thisLine->chr, thisLine->pos, codonPos);
		fc=noncoding;
		if(codonPos.size()>0){
			fc=coding;
			for(std::vector<int>::iterator it=codonPos.begin(); it!=codonPos.end(); ++it){
				switch(*it){
					case 1:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], vcfFile.pointerToVcfLine(2)->bases, geneticCode.possibleBases, vcfFile.pointerToVcfLine(4)->bases))
								 fc=nonsynonymous;
							 else if(fc<synonymous) fc=synonymous;
							 break;
					case 2:  if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], vcfFile.pointerToVcfLine(1)->bases, vcfFile.pointerToVcfLine(2)->bases,  geneticCode.possibleBases))
								 fc=nonsynonymous;
							 else if(fc<synonymous) fc=synonymous;
							 break;
				}
				if(fc==nonsynonymous) break;
			}
		}
		updateFunctionalClass(vcfFile, thisLine, fc);

		//do 4th -> can only do full evaluation if it is codon position 2
		thisLine=vcfFile.pointerToVcfLine(4);
		transcripts.fillCodonPositions(thisLine->chr, thisLine->pos, codonPos);
		fc=noncoding;
		if(codonPos.size()>0){
			fc=coding;
			for(std::vector<int>::iterator it=codonPos.begin(); it!=codonPos.end(); ++it){
				if(*it == 2){
					if(!isSynonymous(geneticCode, (*it), thisLine->bases[0], vcfFile.pointerToVcfLine(2)->bases, vcfFile.pointerToVcfLine(3)->bases,  geneticCode.possibleBases))
						fc=nonsynonymous;
					else fc=synonymous;
				}
			}
		}
		updateFunctionalClass(vcfFile, thisLine, fc);
		//std::cerr << vcfFile.pointerToFirstVcfLine()->chr << ": " << vcfFile.pointerToFirstVcfLine()->pos << " - " << vcfFile.pointerToLastVcfLine()->pos << std::endl;
	} while (continueParsing); //end loop
}

bool TAnnotator::isSynonymous(TGeneticCode & geneticCode, int & codonPos, char & ref, std::vector<char> & pos0, std::vector<char> & pos1, std::vector<char> & pos2){
	std::string codon1, codon2;
	for(std::vector<char>::iterator p0=pos0.begin(); p0!=pos0.end(); ++p0){
		for(std::vector<char>::iterator p1=pos1.begin(); p1!=pos1.end(); ++p1){
			for(std::vector<char>::iterator p2=pos2.begin(); p2!=pos2.end(); ++p2){
				codon2=(*p0); codon2+=(*p1); codon2+=(*p2);
				switch(codonPos){
					case 0: codon1=ref;   codon1+=(*p1); codon1+=(*p2);	break;
					case 1: codon1=(*p0); codon1+=ref;   codon1+=(*p2); break;
					case 2: codon1=(*p0); codon1+=(*p1); codon1+=ref;   break;
				}
				if(!geneticCode.isSynonymous(codon1, codon2))
					return false;
			}
		}
	}
	return true;
}

void TAnnotator::updateFunctionalClass(TVcfFile_base & vcfFile, TVcfLine* line, functionalClass fc){
	switch(fc){
		case noncoding: vcfFile.updateInfo(line, "FC", "NC"); break;
		case coding: vcfFile.updateInfo(line, "FC", "C"); break;
		case synonymous: vcfFile.updateInfo(line, "FC", "S"); break;
		case nonsynonymous: vcfFile.updateInfo(line, "FC", "NS"); break;
	}
}


void TAnnotator::annotateDistanceToClosestVariant(){
	//read parameters
	int maxSearchRange=myParameters->getParameterDouble("maxDist", true);
	if(maxSearchRange<1) throw "maxDist is smaller than one!";
	double genotypeQualityThreshold=myParameters->getParameterDouble("minGQ", true);
	if(verbose){
		std::cerr << "Annotating the distance to the closest variant:" << std::endl;
		std::cerr << " - maximal distance to look for variants is " << maxSearchRange << std::endl;
		std::cerr << " - minimal genotype quality (phred scale) for variants is " << genotypeQualityThreshold << std::endl;
	}

	//open vcf file
	TVcfFileWindowOddSizeStepOne vcfFile(2*maxSearchRange+1, verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enableVariantParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();

	//prepare output vcf
	prepareVcfOutput(vcfFile);
	vcfFile.addFormat("##FORMAT=<ID=DV,Number=4,Type=Integer,Description=\"Distance to closest upstream variant position, closest variant position is heterozygous (0) or homozygous (1) non-ref, same info downstream\">");
	vcfFile.writeHeaderVCF_4_0();

	//Loop through file and compute distance to next variant position
	do {
		vcfFile.advance();
		if(vcfFile.isEmpty)	continue;
		if(vcfFile.doUpdate){
			for(int i=vcfFile.firstToUpdate; i<vcfFile.lastToUpdatePlusOne; ++i){
				//std::cerr << vcfFile.positionAt(i) << std::endl;
				setDistanceToNextVariant(vcfFile, i, genotypeQualityThreshold);
			}
		}
	} while (!vcfFile.eof); //end loop
}

void TAnnotator::setDistanceToNextVariant(TVcfFileWindow & vcfFile, int lineNum, double & genotypeQualityThreshold){
	 int d1, d2;
	 bool het1, het2;
	 //do it for all samples
	 for(int s=0; s<vcfFile.numSamples(); ++s){
		 if(!vcfFile.sampleIsMissingAt(lineNum, s)){
			 //find closest variant backwards
			 d1=99999;
			 for(int l=lineNum-1; l>0; --l){
				 if(!vcfFile.sampleIsMissingAt(l, s) && !vcfFile.sampleIsHomoRefAt(l, s) && vcfFile.sampleGenotypeQualityAt(l, s)>=genotypeQualityThreshold){
					 d1=vcfFile.positionAt(lineNum) - vcfFile.positionAt(l);
					 het1=vcfFile.sampleIsHeteroRefNonrefAt(l, s);
					 break;
				 }
			 }
			 //find closest variant forward
			 d2=99999;
			 for(int l=lineNum+1; l<vcfFile.numUsable; ++l){
				 if(!vcfFile.sampleIsMissingAt(l, s) && !vcfFile.sampleIsHomoRefAt(l, s) && vcfFile.sampleGenotypeQualityAt(l, s)>=genotypeQualityThreshold){
					 d2=vcfFile.positionAt(l)-vcfFile.positionAt(lineNum);
					 het2=vcfFile.sampleIsHeteroRefNonrefAt(l, s);
					 break;
				 }
			 }
			 //save distance
			 std::string data;
			 if(d1==99999) data="NA,NA,";
			 else {
				 data = d1+",";
				 if(het1) data+="0,"; else data+="1,";
			 }
			 if(d2==99999) data+="NA,NA";
			 else {
				 data += d2+",";
				 if(het2) data+="0"; else data+="1";
			 }
			 vcfFile.addInfoToSampleAt(lineNum, s, "DV", data);
		 }
	 }
};


void TAnnotator::checkDVField(){
	if(verbose){
		std::cerr << " - Checking DV field ..." << std::endl;
	}

	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();

	//Loop through file and compute distance to next variant position
	while(vcfFile.next()){
		for(int s=0; s<vcfFile.numSamples(); ++s){
			if(!vcfFile.sampleIsMissing(s) && vcfFile.fieldContentAsString("DV", s)=="."){
				std::cerr << vcfFile.chr() << "\t" << vcfFile.position() << "\t" << vcfFile.sampleName(s) <<  std::endl;
			}
		}
	}
}

void TAnnotator::imposingHardFiltersOnSamples(){
	if(verbose) std::cerr << " - Imposing hard filters:" << std::endl;

	//assume that filter has format "FIELD<VALUE" or "FIELD>VALUE"
	std::string filter=myParameters->getParameterString("filter");
	if(verbose) std::cerr << "   - Read filter: '" << filter << "'" << std::endl;
	//get field
	std::string field;
	int value;
	bool smaller=true;
	char sign='<';
	if(filter.find('>')!=std::string::npos){
		smaller = false;
		sign = '>';
	}
	field=extractBefore(filter, sign);
	//get value and check it is a number
	filter.erase(0,1);
	value=stringToIntCheck(filter);
	if(verbose){
		 std::cerr << "   - Will set all genotypes as missing where field '" << field << "' " << sign << " " << value << std::endl;
	}

	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//prepare output vcf
	prepareVcfOutput(vcfFile);
	vcfFile.addNewHeaderLine("##FILTER=<ID=VcfAnnotator-hard,Description=\"VcfAnnotator: Hard filter (individuals set as missing) on "+ field + toString(sign) + toString(value) + ">");
	vcfFile.writeHeaderVCF_4_0();

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();

	//Loop through file and compute distance to next variant position
	long numGenotypesAffected=0;
	long numVariantsAffected=0;
	bool affected;
	if(smaller){ //if sign is <
		while(vcfFile.next()){
			affected=false;
			for(int s=0; s<vcfFile.numSamples(); ++s){
				if(!vcfFile.sampleIsMissing(s) && vcfFile.fieldContentAsInt(field, s) < value){
					vcfFile.setSampleMissing(s);
					++numGenotypesAffected;
					affected=true;
				}
			}
			if(affected) ++numVariantsAffected;
		}
	} else { //if sign is >
		while(vcfFile.next()){
			affected=false;
			for(int s=0; s<vcfFile.numSamples(); ++s){
				if(!vcfFile.sampleIsMissing(s) && vcfFile.fieldContentAsInt(field, s) > value){
					vcfFile.setSampleMissing(s);
					++numGenotypesAffected;
					affected=true;
				}
			}
			if(affected) ++numVariantsAffected;
		}
	}

	//write output
	if(verbose){
		std::cerr << " - Done! " << numGenotypesAffected << " genotypes set to missing in " << numVariantsAffected << " different variants." << std::endl;
	}
}

void TAnnotator::convert2Plink(){
	if(verbose) std::cerr << " - Converting to PLINK format (PED / MAP):" << std::endl;

	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//open output files: PED and MAP
	std::string outname= myParameters->getParameterString("outname");
	if(verbose) std::cerr << "    - Writing files to '" << outname << ".ped' and '" << outname << ".map'." << std::endl;
	std::ofstream map((outname + (std::string) ".map").c_str());

	//prepare storage
	int numSamples=vcfFile.numSamples();
	std::vector<TGenotype>* genotypes = new std::vector<TGenotype>[numSamples];

	//Loop through file and compute distance to next variant position
	if(verbose) std::cerr << "    - Parsing VCF file and writing MAP file ..." << std::flush;
	long snp=0;
	int s;
	char ref, alt;
	std::string chr;
	long addToPos=0;
	long oldPos=0;
	while(vcfFile.next()){
		//check if it is biallelic and ignore others
		if(vcfFile.getNumAlleles()==2){
			++snp;
			//check if it is a new chromosome
			if(chr!=vcfFile.chr()){
				if(!chr.empty()) addToPos = addToPos + oldPos + 100000;
				chr=vcfFile.chr();
			}
			//add SNP to MAP file
			//-> set chromosome to 1!!! Plink only accepts up to 22 chromosomes!
			oldPos=vcfFile.position();
			map << "1\tSNP_" << snp << "\t0\t" << oldPos+addToPos << std::endl;
			//store genotypes
			ref=vcfFile.getRefAllele();
			alt=vcfFile.getFirstAltAllele();
			for(s=0; s<numSamples; ++s){
				if(vcfFile.sampleIsMissing(s)) genotypes[s].push_back(TGenotype('0'));
				else if(vcfFile.sampleIsHomoRef(s)) genotypes[s].push_back(TGenotype(ref,ref));
				else if(vcfFile.sampleIsHeteroRefNonref(s)) genotypes[s].push_back(TGenotype(ref,alt));
				else genotypes[s].push_back(TGenotype(alt,alt));
			}
		}
	}
	map.close();
	if(verbose){
		std::cerr << " done!" << std::endl;
		std::cerr << "       -> " << snp << " bi-allelic markers found." << std::endl;
		std::cerr << "    - Writing PED file ..." << std::flush;
	}

	//write PED file
	std::ofstream ped((outname + (std::string) ".ped").c_str());
	std::vector<TGenotype>::iterator it;
	for(s=0; s<numSamples; ++s){
		//write first few lines
		ped << vcfFile.sampleName(s) << " " << vcfFile.sampleName(s) << " 0 0 0 0";
		for(it=genotypes[s].begin(); it!=genotypes[s].end(); ++it){
			ped << " " <<  it->first << " " << it->second;
		}
		ped << std::endl;
	}
	ped.close();
	//write output
	if(verbose) std::cerr << " done!" << std::endl;

}

void TAnnotator::convert2Phylip(){
	if(verbose) std::cerr << " - Converting to Phylip:" << std::endl;

	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	TFastaAlignment alignment(vcfFile, verbose);

	//open output file
	std::string outname= myParameters->getParameterString("outname");
	alignment.writePhylip(outname, verbose);
}

void TAnnotator::convert2Nexus(){
	if(verbose) std::cerr << " - Converting to Nexus:" << std::endl;

	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	TFastaAlignment alignment(vcfFile, verbose);

	//open output file
	std::string outname= myParameters->getParameterString("outname");
	alignment.writeNexus(outname, verbose);
}


void TAnnotator::convert2TreeMix(){
	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	if(verbose) std::cerr << " - Converting to TreeMix format:" << std::endl;

	//get sample groups
	TSampleGroups groups(&vcfFile, myParameters->getParameterString("groups"), verbose);
	if(groups.numGroups < 1) throw "No groups defined!";

	//open output
	std::string outname= myParameters->getParameterString("outname") + ".treemix";
	if(verbose) std::cerr << "    - Writing allele counts to '" << outname << "'" << std::endl;
	std::ofstream out(outname.c_str());
	out << "pop1";
	for(int i=2; i<=groups.numGroups; ++i) out << "\tpop" << i;
	out << std::endl;

	//get cutOff
	double cutOff = myParameters->getParameterDoubleWithDefault("cutOff", 0.95);
	if(verbose) std::cerr << "    - Will use a posterior probability cut off of " << cutOff << std::endl;

	//Loop through VCF file
	if(verbose) std::cerr << "    - Parsing VCF file and tree mix file ..." << std::flush;
	long snp = 0;
	int numRef, numAlt;
	bool first;
	bool useGT = true;
	bool usePosition = true;
	while(vcfFile.next()){
		//check if it is biallelic and ignore others
		if(vcfFile.getNumAlleles()==2){
			//check if GT or GP tag is present
			usePosition = true;
			if(vcfFile.formatColExists("GT")) useGT = true;
			else if(vcfFile.formatColExists("GP")) useGT = false;
			else usePosition = false;
			//assemble allele counts for each group
			if(usePosition){
				++snp;
				groups.begin();
				first = true;
				do{
					if(useGT) groups.fillCurrentAlleleCountsGT(numRef, numAlt);
					else groups.fillCurrentAlleleCountsGP(numRef, numAlt, cutOff);
					if(first) first = false;
					else out << "\t";
					out << numRef << ',' << numAlt;
				} while(groups.next());
				out << std::endl;
			}
		}
	}
	if(verbose){
		std::cerr << " done!" << std::endl;
		std::cerr << "       -> " << snp << " bi-allelic markers written" << std::endl;
	}
}


void TAnnotator::convert2FModelHMM(){
	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	if(verbose) std::cerr << " - Converting to FModelHMM format:" << std::endl;

	//get sample groups
	TSampleGroups groups(&vcfFile, myParameters->getParameterString("groups"), verbose);

	//write the groups in the output files
	std::string tmp = myParameters->getParameterString("pops");
	std::vector<int> numpopspergroup_vec;
	fillVectorFromString(tmp, numpopspergroup_vec, ',');
	unsigned int numgroups = numpopspergroup_vec.size();


	if(groups.numGroups < 1) throw "No groups defined!";

	//open output
	std::string outname= myParameters->getParameterString("outname") + ".fhmm";
	if(verbose) std::cerr << "    - Writing allele counts to '" << outname << "'" << std::endl;
	std::ofstream out(outname.c_str());

	out << "-\t-";
	std::string groupLetter;
	for(unsigned int g=0; g<numgroups; g++){
		groupLetter = static_cast<char>('A' + g);
		for(int j=0; j<numpopspergroup_vec[g]; j++){
			out << "\t" << "Group_" << groupLetter;
		}
	}
	out <<std::endl;
	out << "-\t-\t" << "pop1";
	for(int i=2; i<=groups.numGroups; ++i) out << "\tpop" << i;
	out << std::endl;

	//get cutOff
	double cutOff = myParameters->getParameterDoubleWithDefault("cutOff", 0.95);
	if(verbose) std::cerr << "    - Will use a posterior probability cut off of " << cutOff << std::endl;

	//Loop through VCF file
	if(verbose) std::cerr << "    - Parsing VCF file and tree mix file ..." << std::flush;
	long snp = 0;
	long multiAlleleic = 0;
	int numRef, numAlt;
	int totRef, totAll;
	bool useGT = true;
	bool usePosition = true;
	long oldPos = -1;
	std::string outputString;

	while(vcfFile.next()){
		//check if it is biallelic and ignore others
		if(vcfFile.getNumAlleles()==2 && vcfFile.position()!=oldPos){
			//check if GT or GP tag is present
			usePosition = true;
			if(vcfFile.formatColExists("GT")) useGT = true;
			else if(vcfFile.formatColExists("GP")) useGT = false;
			else usePosition = false;
			//assemble allele counts for each group
			totRef = 0; totAll = 0;
			if(usePosition){
				outputString = vcfFile.chr() + "\t" + toString(vcfFile.position());
				groups.begin();
				do{
					if(useGT) groups.fillCurrentAlleleCountsGT(numRef, numAlt);
					else groups.fillCurrentAlleleCountsGP(numRef, numAlt, cutOff);
					outputString += "\t" + toString(numRef) + "/" + toString(numRef+numAlt);
					totRef += numRef; totAll += numRef + numAlt;
				} while(groups.next());

				//only keep polymorphic sites
				if(totRef > 0 && totRef < totAll){
					out << outputString << "\n";
					//update counters
					++snp;
					oldPos = vcfFile.position();
				}
			}
		} else if(vcfFile.getNumAlleles() > 2)
			++multiAlleleic;
	}
	if(verbose){
		std::cerr << " done!" << std::endl;
		std::cerr << "       -> " << snp << " bi-allelic markers written" << std::endl;
		std::cerr << "       -> " << multiAlleleic << " SNPs with > 2 alleles ignored" << std::endl;
	}
}

void TAnnotator::compileSFS(){
	if(verbose) std::cerr << " - Compiling SFS from VCF:" << std::endl;
	//open vcf file
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	//vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//get samples to use: sampel groups

	//TSampleGroup group1(&vcfFile, randomGenerator, myParameters->getParameterString("group1"), windowSize);
	//TSampleGroup group2(&vcfFile, randomGenerator, myParameters->getParameterString("group2"), windowSize);
}

int TAnnotator::baseToNumber(char base, std::string & marker){
	if(base == 'A') return 0;
	else if(base == 'C') return 1;
	else if(base == 'G') return 2;
	else if (base == 'T') return 3;
	else throw "unknown base " + toString(base) + " at marker " + marker;
}

void TAnnotator::vcfToBeagle(){
	//open vcf file
	if(verbose) std::cerr << " - Reading from VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);

	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableVariantParsing(); //has to come before sample parsing!!
	vcfFile.enableSampleParsing();
	vcfFile.enableInfoParsing();

	//open output files:
	std::string outname= myParameters->getParameterString("out");
	if(verbose) std::cerr << " - Writing genotype likelihoods in beagle format to '" << outname << ".beagle.gz" << std::endl;
	gz::ogzstream beagleFile((outname + (std::string) ".beagle.gz").c_str());

	//other variables
	int vcfLines = 0;

	//header string
	std::string header = ""; header += "marker\tallele1\tallele2";
	for(int i=0; i<vcfFile.numSamples(); ++i){
		for(int r=0; r<3; ++r){
			header += "\t" + vcfFile.sampleName(i);
		}
	}
	header += "\n";

	beagleFile << header;

	while(vcfFile.next()){
		//report progress
		++vcfLines;
		std::cout << "vcfLines " << vcfLines << std::endl;
		if(verbose && fmod(vcfLines, 100000000) == 0)
			std::cerr << " - Progress: Lines read from vcfFile = " << vcfLines << std::endl;

		//write line
		std::string marker = ""; marker += vcfFile.chr();marker += "_"; marker += toString(vcfFile.position());
		beagleFile << marker;
		beagleFile << "\t" << baseToNumber(vcfFile.getRefAllele(), marker) << "\t" << baseToNumber(vcfFile.getFirstAltAllele(), marker);
		for(int i=0; i<vcfFile.numSamples(); ++i){
			if(vcfFile.sampleIsMissing(i)){
				beagleFile << "\t0.333\t0.333\t0.333";
			} else {
//				std::cout << "i " << i << " ll " << vcfFile.genotypeLikelihoods(i).AA << " phred " << vcfFile.genotypeLikelihoodsPhred(i).AA << std::endl;
				beagleFile << "\t" << vcfFile.genotypeLikelihoodsPhred(i).AA
					<< "\t" << vcfFile.genotypeLikelihoodsPhred(i).AB
					<< "\t" << vcfFile.genotypeLikelihoodsPhred(i).BB;
			}
		}
		beagleFile << "\n";
	}

	beagleFile.close();

}

int TAnnotator::getGenotypeIndex(char first, char second){
	if (first == 'A'){
		if (second == 'A') return 0;
		else if (second == 'C') return 1;
		else if (second == 'G') return 2;
		else if (second == 'T') return 3;
	}
	else if (first == 'C'){
		if (second == 'A') return 1;
		else if (second == 'C') return 4;
		else if (second == 'G')	return 5;
		else if (second == 'T')	return 6;
	}
	else if (first == 'G'){
		if (second == 'A') return 2;
		else if (second == 'C') return 5;
		else if (second == 'G')	return 7;
		else if (second == 'T')	return 8;
	}
	else if (first == 'T'){
		if (second == 'A') return 3;
		else if (second == 'C') return 6;
		else if (second == 'G')	return 8;
		else if (second == 'T')	return 9;
		}
	return -1;
}

void TAnnotator::validateGenotype(std::string & gtString){
	if(gtString.size() != 2) throw "genotype string " + gtString + " does not have length=2!";
	if(getGenotypeIndex(gtString[0], gtString[1]) < 0) throw "genotype string " + gtString + " contains unknown bases";
}


std::pair<char, char> TAnnotator::getGenotypeFromIndex(int index){
     std::pair<char, char> genotype;
     if (index==0){ genotype.first = 'A'; genotype.second = 'A'; return genotype;}
     if (index==1){ genotype.first = 'A'; genotype.second = 'C'; return genotype;}
     if (index==2){ genotype.first = 'A'; genotype.second = 'G'; return genotype;}
     if (index==3){ genotype.first = 'A'; genotype.second = 'T'; return genotype;}
     if (index==4){ genotype.first = 'C'; genotype.second = 'C'; return genotype;}
     if (index==5){ genotype.first = 'C'; genotype.second = 'G'; return genotype;}
     if (index==6){ genotype.first = 'C'; genotype.second = 'T'; return genotype;}
     if (index==7){ genotype.first = 'G'; genotype.second = 'G'; return genotype;}
     if (index==8){ genotype.first = 'G'; genotype.second = 'T'; return genotype;}
     if (index==9){ genotype.first = 'T'; genotype.second = 'T'; return genotype;}
     else { genotype.first = 'E'; genotype.second = 'E'; return genotype;}
}

enum countStats {
	allTotal=0,
	allHeteroTotal,
	allHomoRefTotal,
	allHomoAltTotal,

	allZeroDiff,
	allHeteroZeroDiff,
	allHomoRefZeroDiff,
	allHomoAltZeroDiff,

	allOneDiff,
	allHeteroOneDiff,
	allHomoRefOneDiff,
	allHomoAltOneDiff,

	allTwoDiff,
	allHeteroTwoDiff,
	allHomoRefTwoDiff,
	allHomoAltTwoDiff
};

enum genotypeClasses {
	hetero=0,
	homoRef,
	homoAlt,
	allTogether
};

std::string returnGTClassString(int genotypeClass){
	if(genotypeClass == hetero) return "hetero";
	else if(genotypeClass == homoRef) return "homoRef";
	else if(genotypeClass == homoAlt) return "homoAlt";
	else if(genotypeClass == allTogether) return "allTrueGenotypes";
	return "invalid";
}

void TAnnotator::makeQualCounts(long qualAlleleCounts[4][101][2], bool & distinguishHomos, bool genotypeClass[3], int & ref, int & call, int & qual) {
	std::pair<char, char> refG = getGenotypeFromIndex(ref);
    std::pair<char, char> callG = getGenotypeFromIndex(call);
    if(qual < 0) throw "quality = " + toString(qual) + "! It must be >= 0.";

	if (refG.first == callG.first){
		if (refG.second == callG.second){
			if(distinguishHomos){
				//zero diff
				if(genotypeClass[hetero]) qualAlleleCounts[hetero][qual][0] += 2;
				else if(genotypeClass[homoRef]) qualAlleleCounts[homoRef][qual][0] += 2;
				else ++qualAlleleCounts[homoAlt][qual][0] += 2;
			} qualAlleleCounts[allTogether][qual][0] += 2;
		}else{
			if(distinguishHomos){
				//one diff
				if(genotypeClass[hetero]){qualAlleleCounts[hetero][qual][0] += 2; ++qualAlleleCounts[hetero][qual][1];}
				else if(genotypeClass[homoRef]) {qualAlleleCounts[homoRef][qual][0] += 2; ++qualAlleleCounts[homoRef][qual][1];}
				else {qualAlleleCounts[homoAlt][qual][0] += 2; ++qualAlleleCounts[homoAlt][qual][1];}
			} qualAlleleCounts[allTogether][qual][0] += 2; ++qualAlleleCounts[allTogether][qual][1];
		}
	}else{
		if (refG.second == callG.second){
			if(distinguishHomos){
				//one diff
				if(genotypeClass[hetero]){qualAlleleCounts[hetero][qual][0] += 2; ++qualAlleleCounts[hetero][qual][1];}
				else if(genotypeClass[homoRef]) {qualAlleleCounts[homoRef][qual][0] += 2; ++qualAlleleCounts[homoRef][qual][1];}
				else {qualAlleleCounts[homoAlt][qual][0] += 2; ++qualAlleleCounts[homoAlt][qual][1];}
			} qualAlleleCounts[allTogether][qual][0] += 2; ++qualAlleleCounts[allTogether][qual][1];
		} else if(callG.first == refG.second && callG.second == refG.first){
			if(distinguishHomos){
				//zero diff
				if(genotypeClass[hetero]) qualAlleleCounts[hetero][qual][0] += 2;
				else if(genotypeClass[homoRef]) qualAlleleCounts[homoRef][qual][0] += 2;
				else qualAlleleCounts[homoAlt][qual][0] += 2;
			} qualAlleleCounts[allTogether][qual][0] += 2;
		} else if ((callG.first == refG.second && callG.second != refG.first) || (callG.first != refG.second && callG.second == refG.first)){
			if(distinguishHomos){
				//one diff
				if(genotypeClass[hetero]){qualAlleleCounts[hetero][qual][0] += 2; ++qualAlleleCounts[hetero][qual][1];}
				else if(genotypeClass[homoRef]) {qualAlleleCounts[homoRef][qual][0] += 2; ++qualAlleleCounts[homoRef][qual][1];}
				else {qualAlleleCounts[homoAlt][qual][0] += 2; ++qualAlleleCounts[homoAlt][qual][1];}
			}qualAlleleCounts[allTogether][qual][0] += 2; ++qualAlleleCounts[allTogether][qual][1];
		} else {
			if(distinguishHomos){
				//two diff
				if(genotypeClass[hetero]){qualAlleleCounts[hetero][qual][0] += 2; qualAlleleCounts[hetero][qual][1] += 2;}
				else if(genotypeClass[homoRef]) {qualAlleleCounts[homoRef][qual][0] += 2; qualAlleleCounts[homoRef][qual][1] += 2;}
				else {qualAlleleCounts[homoAlt][qual][0] += 2; qualAlleleCounts[homoAlt][qual][1] += 2;}
			} qualAlleleCounts[allTogether][qual][0] += 2; qualAlleleCounts[allTogether][qual][1] += 2;
		}
	}
}

void TAnnotator::makeCounts(int countsAll[10][10], long countStats[12], bool & distinguishHomos, bool genotypeClass[3], int & ref, int & call) {
	std::pair<char, char> refG = getGenotypeFromIndex(ref);
    std::pair<char, char> callG = getGenotypeFromIndex(call);

	if(distinguishHomos){
		if(genotypeClass[hetero]) ++countStats[allHeteroTotal];
		else if(genotypeClass[homoRef])	++countStats[allHomoRefTotal];
		else ++countStats[allHomoAltTotal];
	} else countStats[allTotal] += countsAll[ref][call];

	if (refG.first == callG.first){
		if (refG.second == callG.second){
			if(distinguishHomos){
				if(genotypeClass[hetero]) ++countStats[allHeteroZeroDiff];
				else if(genotypeClass[homoRef]) ++countStats[allHomoRefZeroDiff];
				else ++countStats[allHomoAltZeroDiff];
			} else countStats[allZeroDiff] += countsAll[ref][call];

		}else{
			if(distinguishHomos){
				if(genotypeClass[hetero]) ++countStats[allHeteroOneDiff];
				else if(genotypeClass[homoRef]) ++countStats[allHomoRefOneDiff];
				else ++countStats[allHomoAltOneDiff];
			} else countStats[allOneDiff] += countsAll[ref][call];
		}
	}else{
		if (refG.second == callG.second){
			if(distinguishHomos){
				if(genotypeClass[hetero]) ++countStats[allHeteroOneDiff];
				else if(genotypeClass[homoRef]) ++countStats[allHomoRefOneDiff];
				else ++countStats[allHomoAltOneDiff];
			} else countStats[allOneDiff] += countsAll[ref][call];
		} else if(callG.first == refG.second && callG.second == refG.first){
			if(distinguishHomos){
				if(genotypeClass[hetero]) ++countStats[allHeteroZeroDiff];
				else if(genotypeClass[homoRef]) ++countStats[allHomoRefZeroDiff];
				else ++countStats[allHomoAltZeroDiff];
			} else countStats[allZeroDiff] += countsAll[ref][call];
		} else if ((callG.first == refG.second && callG.second != refG.first) || (callG.first != refG.second && callG.second == refG.first)){
			if(distinguishHomos){
				if(genotypeClass[hetero]) ++countStats[allHeteroOneDiff];
				else if(genotypeClass[homoRef]) ++countStats[allHomoRefOneDiff];
				else ++countStats[allHomoAltOneDiff];
			} else countStats[allOneDiff] += countsAll[ref][call];
		} else {
			if(distinguishHomos){
				if(genotypeClass[hetero]) ++countStats[allHeteroTwoDiff];
				else if(genotypeClass[homoRef]) ++countStats[allHomoRefTwoDiff];
				else ++countStats[allHomoAltTwoDiff];
			} else countStats[allTwoDiff] += countsAll[ref][call];
		}
	}
}

void TAnnotator::fillPositionsMap(std::map <std::string, int> & positionsToConsider){
	std::string positionsFileName = myParameters->getParameterString("positionsFile");
	std::cerr << " - only considering positions in file " << positionsFileName << std::endl;
	std::ifstream positionsFile(positionsFileName.c_str());
	if(!positionsFile) throw "Failed to open file with positions " + positionsFileName + "'!";

	std::string posLine;
	while(positionsFile.good() && !positionsFile.eof()){
		std::getline(positionsFile, posLine);
		positionsToConsider.insert(make_pair(posLine, 1));
	}
}

void TAnnotator::compareCalls(){

	//open vcf file
	if(verbose) std::cerr << " - Reading from VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);

	//are we differentiating homo
	bool distinguishHomos = false;
	if(myParameters->parameterExists("distinguishHomos")){
		distinguishHomos = true;
		if(verbose) std::cerr << "- making counts for homo reference and homo alternative calls" << std::endl;
	}

	//what quality are we making quality files for
	int qual = myParameters->getParameterDoubleWithDefault("qualFilter", 10);
	if(verbose) std::cerr << " - quality filter for quality files set to " << qual << std::endl;

	// open true file
	TTrueFile trueFileTest(*myParameters, verbose);

	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableVariantParsing(); //has to come before sample parsing!!
	vcfFile.enableSampleParsing();

	if(vcfFile.numSamples() > 1){
		if(verbose) std::cerr << " - found more than 1 samples in vcf file -> header needs to be present in true file!" << std::endl;
		trueFileTest.setOrder();
	} else {
		if(verbose) std::cerr << " - only 1 individual found in vcf file -> considering header to be absent from true file!" << std::endl;
		trueFileTest.setOrder(vcfFile.sampleName(0));
	}


	// are we considering only certain positions?
	bool considerSites = false;
	std::map <std::string, int> positionsToConsider;
	if(myParameters->parameterExists("positionsFile")){
		fillPositionsMap(positionsToConsider);
		considerSites = true;
	}

	//are we considering only certain genotypes?
	bool restrictTrueGT = false;
	bool restrictCalledGT = false;
	std::vector<std::string> vec;
	std::map <std::string, int> trueGTToConsider;
	std::map <std::string, int> calledGTToConsider;
	if(myParameters->parameterExists("trueGT")){
		std::string trueString = myParameters->getParameterString("trueGT");
		fillVectorFromString(trueString, vec, ',');
		if(verbose) std::cerr << " - restricting to trueGenotypes:";
		for(std::vector<std::string>::iterator it=vec.begin(); it!=vec.end(); ++it){
			validateGenotype(*it);
			trueGTToConsider.insert(make_pair(*it,1));
			if(verbose) std::cerr << " " << *it;
		}
		if(verbose) std::cerr << "\n";
		restrictTrueGT = true;
	}
	if(myParameters->parameterExists("calledGT")){
			std::string trueString = myParameters->getParameterString("calledGT");
			fillVectorFromString(trueString, vec, ',');
			if(verbose) std::cerr << " - restricting to calledGenotypes:";
			for(std::vector<std::string>::iterator it=vec.begin(); it!=vec.end(); ++it){
				validateGenotype(*it);
				calledGTToConsider.insert(make_pair(*it,1));
				if(verbose) std::cerr << " " << *it;
			}
			if(verbose) std::cerr << "\n";
			restrictCalledGT = true;
		}

	//open output files:
	std::string outname= myParameters->getParameterString("out");
	if(verbose) std::cerr << " - Writing table files to '" << outname << "*.table.allQual' and '" << outname << "*.table.10Qual'." << std::endl;
	if(verbose) std::cerr << " - Writing stats files to '" << outname << "*.stats.allQual' and '" << outname << "*.stats.10Qual'." << std::endl;

	//table
	std::ofstream tableFile((outname + (std::string) ".table.allQual").c_str());
	std::ofstream statsFile((outname + (std::string) ".stats.allQual").c_str());

	std::ofstream statsFileHetero((outname + (std::string) "_hetero.stats.allQual").c_str());
	std::ofstream statsFileHomoRef((outname + (std::string) "_homoRef.stats.allQual").c_str());
	std::ofstream statsFileHomoAlt((outname + (std::string) "_homoAlt.stats.allQual").c_str());

	std::ofstream tableFileQual((outname + (std::string) ".table.qualFilter" + toString(qual)).c_str());
	std::ofstream statsFileQual((outname + (std::string) ".stats.qualFilter" + toString(qual)).c_str());


	//big table
	int countsAll[10][10] = {{0}};
	int countsQual[10][10] = {{0}};
	long countStats[16] = {0};
	long countStatsQual[16] = {0};
	bool genotypeClass[3];

	//get chr limit
	int chrLimit = myParameters->getParameterIntWithDefault("chrLimit", 50);
	if(verbose) std::cerr << " - stopping at chromosome " << chrLimit << std::endl;

	int considerSample = myParameters -> getParameterIntWithDefault("considerSample", -1);
	if(!considerSample) considerSample = -1;
	if(verbose && considerSample > -1) std::cerr << " - considering Sample " << considerSample << std::endl;

	//some variables
	long int vcfLines = 0;
	int sampleNum = -1;
	std::string trueGenotype;
	int trueIndex;
	int calledIndex;
	long noCoverage = 0, noCoverageHetero = 0, noCoverageHomoRef = 0, noCoverageHomoAlt = 0;
	long coverageButUncalled = 0, coverageButUncalledHetero = 0, coverageButUncalledHomoRef = 0, coverageButUncalledHomoAlt = 0;
	long qualityWasZero = 0, qualityWasZeroHetero = 0, qualityWasZeroHomoRef = 0, qualityWasZeroHomoAlt = 0;
	long totalHeteroSites = 0, totalHomoRefSites = 0, totalHomoAltSites = 0, noCallInTrueFile = 0;
	long heteroCalledHomoRef = 0, heteroCalledHomoAlt = 0;
	int indelInRef = 0;
	int indelAllGenotypes = 0, indelHetero = 0, indelHomoRef = 0, indelHomoAlt = 0;
	int indelAllGenotypesQual = 0;

	std::string chromosome;

	while(vcfFile.next()){
		//report progress
		if(verbose && fmod(vcfLines, 100000000) == 0) std::cerr << " - Progress: Lines read from vcfFile = " << vcfLines << std::endl;
		//should we stop?
		//remove "chr" if present
		chromosome=vcfFile.chr();
		if(chromosome.find("chr")==0) chromosome.erase(0, 3);
		if(stringToInt(chromosome) > chrLimit || stringToInt(chromosome) == 0) break;

		vcfLines ++;

		//if we consider only certain sites, is this one of them?
		if(considerSites && positionsToConsider.count(toString(vcfFile.position())) != 1) continue;

		//now parse
		if(trueFileTest.parseLine()){
			//positions the same?
			if(vcfFile.chr() != trueFileTest.trueChrom) throw "Chromosomes are not the same! True chromosome==" + trueFileTest.trueChrom + " at position " + toString(trueFileTest.truePos) + " and chr in calls file==" + vcfFile.chr() + " at position " + toString(vcfFile.position());
			if(vcfFile.position() != trueFileTest.truePos) throw "Positions are not the same! True position==" + toString(trueFileTest.truePos) + " on chr " + trueFileTest.trueChrom+ " and position in calls file==" + toString(vcfFile.position()) + " on " + vcfFile.chr();

			//loop over samples
			for(unsigned int i=0; i<trueFileTest.order.size(); ++i){
				bool genotypeClass[3] = {false};
				//the column of the true file should be assigned to the correct sample in vcf file
				//the vcfFile samples are numbered according to their order in the file, not their name
				//sampleNum is zero based
				if(trueFileTest.vcfVsFlatCalls) sampleNum = 0;
				else sampleNum = vcfFile.sampleNumber(trueFileTest.order[i]);
				if(sampleNum < 0) throw "Sample " + trueFileTest.order[i] + " is present in trueFile but not in vcf file";

				//get true genotype
				trueGenotype = trueFileTest.getTrueGenotype(i);

				//if there is a restriction: do we consider this true genotype?
				if(restrictTrueGT && trueGTToConsider.count(trueGenotype) != 1) continue;

				//if there is no restriction
				if(trueGenotype == "0" || trueGenotype == "-" || trueGenotype == "NN"){
					++noCallInTrueFile;
					continue;
				}
				//was there an indel?
				if(vcfFile.getRefAllele() == 'D') {
					indelInRef = indelAllGenotypes + vcfFile.numSamples();
					continue;
				}
				//is true genotype hetero, homoRef or homoAlt?
				else if(trueGenotype[0] != trueGenotype[1]){
					genotypeClass[hetero] = true;
					++totalHeteroSites;
				} else {
					if(vcfFile.getRefAllele() == trueGenotype[0]){
						genotypeClass[homoRef] = true;
						++totalHomoRefSites;
					} else {
						genotypeClass[homoAlt] = true;
						++totalHomoAltSites;
					}
				}

				//is there no coverage? -> sample automatically defined as "missing"
				if(vcfFile.depthAsIntNoCheckForMissingSample("DP", sampleNum) == 0){
					++noCoverage;
					if(genotypeClass[hetero]) ++noCoverageHetero;
					else if(genotypeClass[homoRef]) ++noCoverageHomoRef;
					else if(genotypeClass[homoAlt]) ++noCoverageHomoAlt;
				}

				//is there coverage but no call?
				else if (vcfFile.depthAsIntNoCheckForMissingSample("DP", sampleNum) > 0 && vcfFile.sampleIsMissing(sampleNum)){
					++coverageButUncalled; // genotype = ./.
					if(genotypeClass[hetero]) ++coverageButUncalledHetero;
					else if(genotypeClass[homoRef]) ++coverageButUncalledHomoRef;
					else if(genotypeClass[homoAlt]) ++coverageButUncalledHomoAlt;
				}

				//there is coverage and a call
				else {
					if(vcfFile.sampleGenotypeQuality(sampleNum) > 0){
						//get true genotype index
						trueIndex = getGenotypeIndex(trueGenotype[0], trueGenotype[1]);
						//get genotype of sample from vcf
//						if(haploid) calledIndex = getGenotypeIndex(vcfFile.getFirstAlleleOfSample(sampleNum), vcfFile.getFirstAlleleOfSample(sampleNum));
						calledIndex = getGenotypeIndex(vcfFile.getFirstAlleleOfSample(sampleNum), vcfFile.getSecondAlleleOfSample(sampleNum));
						if (calledIndex > -1 && trueIndex > -1){
							//if there is a restriction: do we consider this called genotype?
							if(restrictCalledGT){
								std::string cGT = ""; cGT += vcfFile.getFirstAlleleOfSample(sampleNum); cGT += vcfFile.getSecondAlleleOfSample(sampleNum);
								if(calledGTToConsider.count(trueGenotype) != 1) continue;
							}

							//add counts to tables
							++countsAll[trueIndex][calledIndex];
							if (vcfFile.sampleGenotypeQuality(sampleNum) >= qual) ++countsQual[trueIndex][calledIndex];
							if(distinguishHomos){
								makeCounts(countsAll, countStats, distinguishHomos, genotypeClass, trueIndex, calledIndex);
								//check for reference bias
								if(genotypeClass[hetero] && vcfFile.getFirstAlleleOfSample(sampleNum) == vcfFile.getSecondAlleleOfSample(sampleNum)){
									if(vcfFile.getRefAllele() == vcfFile.getFirstAlleleOfSample(sampleNum)) ++heteroCalledHomoRef;
									else ++heteroCalledHomoAlt;
								}
							}
						} else if(calledIndex== -1 && trueIndex > -1){
							++indelAllGenotypes;
							if (vcfFile.sampleGenotypeQuality(sampleNum) >= qual) ++indelAllGenotypesQual;
							if(genotypeClass[hetero]) ++indelHetero;
							else if(genotypeClass[homoRef]) ++indelHomoRef;
							else if(genotypeClass[homoAlt]) ++indelHomoAlt;

						} else 	std::cerr << "undefined call in sample " << sampleNum << " at position " << vcfFile.position() <<" : true index=" << trueIndex <<", calledIndex=" <<calledIndex << std::endl;
					} else {
						++qualityWasZero;
						if(genotypeClass[hetero]) ++qualityWasZeroHetero;
						else if(genotypeClass[homoRef]) ++qualityWasZeroHomoRef;
						else if(genotypeClass[homoAlt]) ++qualityWasZeroHomoAlt;
					}
				}
			}
		} else continue;
	}

	//calc stats
	distinguishHomos = false;
	//all calls
	for (int ref = 0; ref < 10; ref++){
		for (int call = 0; call < 10; call++){
			makeCounts(countsAll, countStats, distinguishHomos, genotypeClass, ref, call);
		}
	}
	//qual calls
	for (int ref = 0; ref < 10; ref++){
		for (int call = 0; call < 10; call++){
			makeCounts(countsQual, countStatsQual, distinguishHomos, genotypeClass, ref, call);
		}
	}

    //amount of 0,1,2 differences
 	//writing to file: all qualities
    //sum of all alleles = totalDiffs + 2*correct_calls+1*calls1Diff + 2*noCoverage+2*uncalledButData+2*qual0 = 2*positionsInReference*numSamples
    //table file
	std::string headerT = "ref/calls\tAA\tAC\tAG\tAT\tCC\tCG\tCT\tGG\tGT\tTT";
    tableFile << headerT << std::endl;
    tableFileQual << headerT << std::endl;

    for (int ref = 0; ref < 10; ref ++){
    	tableFile << getGenotypeFromIndex(ref).first << getGenotypeFromIndex(ref).second;
    	tableFileQual << getGenotypeFromIndex(ref).first << getGenotypeFromIndex(ref).second;
    	for (int call= 0; call < 10; call++){
    		tableFile << "\t" << countsAll[ref][call];
    		tableFileQual << "\t" << countsQual[ref][call];
        }
    	tableFile << "\n";
    	tableFileQual << "\n";
    }

    //stats files
    std::string headerS = "Positions_in_referrence\tnumSamples\tTotal_calls\tTotal_diff\tCalls_with_zero_diff\tCalls_with_one_diff\tCalls_with_two_diff\tnoCoverage\tcoverageButUncalled\tqualityWas0\tnoCallInTrueFile\tindelInSample\tindelInRef\n";
	statsFile << headerS;
	statsFile << vcfLines << "\t" <<vcfFile.numSamples() << "\t"<< countStats[allTotal] << "\t" << 2*countStats[allTwoDiff] + countStats[allOneDiff] << "\t" << countStats[allZeroDiff] << "\t" << countStats[allOneDiff] << "\t" << countStats[allTwoDiff] << "\t"<< noCoverage <<"\t"<< coverageButUncalled <<"\t"<< qualityWasZero << "\t" << noCallInTrueFile << "\t" << indelAllGenotypes << "\t" << indelInRef <<"\n";
	statsFileQual << headerS;
	statsFileQual << vcfLines << "\t" <<vcfFile.numSamples() << "\t"<< countStatsQual[allTotal] << "\t" << 2*countStatsQual[allTwoDiff] + countStatsQual[allOneDiff] << "\t" << countStatsQual[allZeroDiff] << "\t" << countStatsQual[allOneDiff] << "\t" << countStatsQual[allTwoDiff] << "\tNA\tNA\tNA\t" << noCallInTrueFile << "\t" << indelAllGenotypesQual << "\t" << indelInRef <<"\n";

	statsFileHetero << "Positions_in_referrence\tnumSamples\tTotal_calls\tTotal_diff\tCalls_with_zero_diff\tCalls_with_one_diff\tCalls_with_two_diff\tnoCoverage\tcoverageButUncalled\tqualityWas0\ttotalHeteroSites\tindelInSample\tindelInRef\theteroCalledHomoRef\theteroCalledHomoAlt\n";
	statsFileHetero << vcfLines<< "\t" << vcfFile.numSamples()<< "\t" << countStats[allHeteroTotal] << "\t" << 2*countStats[allHeteroTwoDiff] + countStats[allHeteroOneDiff] << "\t" << countStats[allHeteroZeroDiff] << "\t" << countStats[allHeteroOneDiff] << "\t" << countStats[allHeteroTwoDiff] << "\t"<< noCoverageHetero  <<"\t"<< coverageButUncalledHetero <<"\t"<< qualityWasZeroHetero << "\t" << totalHeteroSites << "\t" << indelHetero <<"\t" << indelInRef<<"\t" << heteroCalledHomoRef<<"\t" << heteroCalledHomoAlt <<"\n";

	statsFileHomoRef << "Positions_in_referrence\tnumSamples\tTotal_calls\tTotal_diff\tCalls_with_zero_diff\tCalls_with_one_diff\tCalls_with_two_diff\tnoCoverage\tcoverageButUncalled\tqualityWas0\ttotalHomoRefSites\tindelInSample\tindelInRef\n";
	statsFileHomoRef << vcfLines<<"\t" <<vcfFile.numSamples() << "\t" << countStats[allHomoRefTotal] << "\t" << 2*countStats[allHomoRefTwoDiff] + countStats[allHomoRefOneDiff] << "\t" << countStats[allHomoRefZeroDiff] << "\t" << countStats[allHomoRefOneDiff] << "\t" << countStats[allHomoRefTwoDiff] << "\t"<< noCoverageHomoRef  <<"\t"<< coverageButUncalledHomoRef <<"\t"<< qualityWasZeroHomoRef << "\t" << totalHomoRefSites << "\t" << indelHomoRef <<"\t" << indelInRef <<"\n";

	statsFileHomoAlt << "Positions_in_referrence\tnumSamples\tTotal_calls\tTotal_diff\tCalls_with_zero_diff\tCalls_with_one_diff\tCalls_with_two_diff\tnoCoverage\tcoverageButUncalled\tqualityWas0\ttotalHomoAltSites\tindelInSample\tindelInRef\n";
	statsFileHomoAlt << vcfLines <<"\t" <<vcfFile.numSamples()<< "\t" << countStats[allHomoAltTotal] << "\t" << 2*countStats[allHomoAltTwoDiff] + countStats[allHomoAltOneDiff] << "\t" << countStats[allHomoAltZeroDiff] << "\t" << countStats[allHomoAltOneDiff] << "\t" << countStats[allHomoAltTwoDiff] << "\t"<< noCoverageHomoAlt  <<"\t"<< coverageButUncalledHomoAlt <<"\t"<< qualityWasZeroHomoAlt << "\t" << totalHomoAltSites << "\t" << indelHomoAlt <<"\t" << indelInRef <<"\n";

    //close files
    tableFile.close();
	statsFile.close();
	statsFileHetero.close();
	statsFileHomoRef.close();
	statsFileHomoAlt.close();
    tableFileQual.close();
	statsFileQual.close();

//	delete myStream;

	if(verbose) std::cerr << " done!" << std::endl;
}

void TAnnotator::compareQualityToError(){

	//open vcf file
	if(verbose) std::cerr << " - Reading from VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);

	// open true file
	TTrueFile trueFileTest(*myParameters, verbose);

	//are we differentiating homo
	bool distinguishHomos = false;
	if(myParameters->parameterExists("distinguishHomos")){
		distinguishHomos = true;
		if(verbose) std::cerr << "- making counts for homo reference and homo alternative calls" << std::endl;
	}

	//open output files:
	std::string outname= myParameters->getParameterString("out");

	//vector of outputs
	//genotype quality
	std::vector<std::ostream*> myOutStreams_qual;
	std::vector<std::ostream*>::iterator myOutStreamsIt_qual;
	for(int i=0; i<4; ++i){
		std::string gtClass = returnGTClassString(i);
		std::string filename = outname +"_qual_"+gtClass+ ".txt";
		std::ofstream* f = new std::ofstream(filename.c_str()); // create in free store
		myOutStreams_qual.push_back(f);
		if(verbose) std::cerr << " - writing " << gtClass << " file to " << filename << std::endl;
		if(!f->good()) throw "file " + filename + " not good!";
	}

	//posterior genotype probability
	std::vector<std::ostream*> myOutStreams_prob;
	std::vector<std::ostream*>::iterator myOutStreamsIt_prob;
	for(int i=0; i<4; ++i){
		std::string gtClass = returnGTClassString(i);
		std::string filename = outname +"_prob_"+gtClass+ ".txt";
		std::ofstream* f = new std::ofstream(filename.c_str()); // create in free store
		myOutStreams_prob.push_back(f);
		if(verbose) std::cerr << " - writing " << gtClass << " file to " << filename << std::endl;
		if(!f->good()) throw "file " + filename + " not good!";
	}

	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableVariantParsing(); //has to come before sample parsing!!
	vcfFile.enableSampleParsing();

	if(vcfFile.numSamples() > 1){
		if(verbose) std::cerr << " - found more than 1 samples in vcf file -> header needs to be present in true file!" << std::endl;
		trueFileTest.setOrder();
	} else {
		if(verbose) std::cerr << " - only 1 individual found in vcf file -> considering header to be absent from true file!" << std::endl;
		trueFileTest.setOrder(vcfFile.sampleName(0));
	}

	//big table
	long qualAlleleCounts[4][101][2] = {{{0}}};
	long probAlleleCounts[4][101][2] = {{{0}}};


	//get chr limit
	int chrLimit = myParameters->getParameterIntWithDefault("chrLimit", 50);
	if(verbose) std::cerr << "    - stopping at chromosome " << chrLimit << std::endl;

	//some variables
	long int posInRef = 0;
	int sampleNum = -1;
	int trueIndex;
	int calledIndex;
	std::string trueGenotype;
	int totalHeteroSites = 0, totalHomoRefSites = 0, totalHomoAltSites = 0, noCallInTrueFile = 0;
	std::string chromosome;
	int qual;
	std::vector<int> PLs;

	//variables for priors
	double s;
	double GTProbDouble;
	int GTProb;
	bool uniformPrior = false;
	double piHet = -1.0, oneMinusPiHet = -1.0, refDiv = -1.0, theta = -1.0;
	double LLHomoRefAndPrior, LLHeteroAndPrior, LLHomoAltAndPrior;
	if(myParameters->parameterExists("uniformPrior")){
		uniformPrior=true;
		if(verbose) std::cerr << " - using uniform prior to calculate posterior genotype probability" << std::endl;
	} else {
		theta = myParameters->getParameterDoubleWithDefault("theta", 0.001);
		if(verbose) std::cerr << " - setting theta to " << theta << " to calculate genotype frequencies used as a prior for calculation of posterior genotype probability" << std::endl;
		refDiv = myParameters->getParameterDoubleWithDefault("refDiv", 0.01);
		if(verbose) std::cerr << " - setting refDiv to " << refDiv << " to calculate genotype frequencies used as a prior for calculation of posterior genotype probability" << std::endl;

	}

	while(vcfFile.next()){
		posInRef ++;
		//report progress
		if(verbose && fmod(posInRef, 10000000) == 0){
			std::cerr << posInRef << std::endl;
	//		break;
		}
		//should we stop?
		//remove "chr" if present
		chromosome=vcfFile.chr();
		if(chromosome.find("chr")==0) chromosome.erase(0, 3);
		if(stringToInt(chromosome) > chrLimit || stringToInt(chromosome) == 0) break;

		//now parse
		if(trueFileTest.parseLine()){

			//positions the same?
			if(vcfFile.chr() != trueFileTest.trueChrom) throw "Chromosomes are not the same! True chromosome==" + trueFileTest.trueChrom + " at position " + toString(trueFileTest.truePos) + " and chr in calls file==" + vcfFile.chr() + " at position " + toString(vcfFile.position());
			if(vcfFile.position() != trueFileTest.truePos) throw "Positions are not the same! True position==" + toString(trueFileTest.truePos) + " on chr " + trueFileTest.trueChrom+ " and position in calls file==" + toString(vcfFile.position()) + " on " + vcfFile.chr();

			//loop over samples
			for(unsigned int i=0; i<trueFileTest.order.size(); ++i){
				if(vcfFile.getNumAlleles() > 2) continue;
				bool genotypeClass[3] = {false};
				//the column of the true file should be assigned to the correct sample in vcf file
				//the vcfFile samples are numbered according to their order in the file, not their name
				//sampleNum is zero based
				if(trueFileTest.vcfVsFlatCalls) sampleNum = 0;
				else sampleNum = vcfFile.sampleNumber(trueFileTest.order[i]);
				if(sampleNum < 0) throw "Sample " + trueFileTest.order[i] + " is present in trueFile but not in vcf file";

				//get true genotype
				trueGenotype = trueFileTest.getTrueGenotype(i);
				if(trueGenotype == "0" || trueGenotype == "-" ){
					++noCallInTrueFile;
					continue;
				}
				//was there an indel?
				if(vcfFile.getRefAllele() == 'D') {
					continue;
				}
				//is true genotype hetero, homoRef or homoAlt?
				else if(trueGenotype[0] != trueGenotype[1]){
					genotypeClass[hetero] = true;
					++totalHeteroSites;
				} else {
					if(vcfFile.getRefAllele() == trueGenotype[0]){
						genotypeClass[homoRef] = true;
						++totalHomoRefSites;
					} else {
						genotypeClass[homoAlt] = true;
						++totalHomoAltSites;
					}
				}

				//there is coverage and a call
				if(!vcfFile.sampleIsMissing(sampleNum) && vcfFile.depthAsIntNoCheckForMissingSample("DP", sampleNum) > 0){
					qual = vcfFile.sampleGenotypeQuality(sampleNum);

					//posterior probability
					fillVectorFromString(vcfFile.fieldContentAsString("PL", i), PLs, ',');
					s = 0;

					if(uniformPrior){
						for(i=0; i<3; ++i){
							s += pow(10,PLs[i] / -10.0); //bayes' normalization constant with uniform prior
						}
						GTProb = round(1.0/s * 100.0); //phred(0) = 1, convert to percent
					} else {
						//prior = g.t. frequencies if base frequencies = 0.25
						piHet = 6.0 * 0.25 * 0.25 * (1.0 - exp(-theta));
						oneMinusPiHet = 1.0 - piHet;
						LLHomoRefAndPrior = pow(10, PLs[0] / -10.0) * oneMinusPiHet * (1.0 - refDiv);
						LLHeteroAndPrior = pow(10, PLs[1] / -10.0) * piHet;
						LLHomoAltAndPrior = pow(10, PLs[2] / -10.0) * oneMinusPiHet * refDiv;

						//bayes' normalization constant
						s = LLHomoRefAndPrior + LLHeteroAndPrior + LLHomoAltAndPrior;

						//calculate posterior GTProb
						if(genotypeClass[homoRef]){
							GTProbDouble = LLHomoRefAndPrior / s;
						}
						else if(genotypeClass[hetero]){
							GTProbDouble = LLHeteroAndPrior / s;
						}
						else {
							GTProbDouble = LLHomoAltAndPrior / s;
						}

						//convert to percent
						GTProb = round(GTProbDouble * 100.0);
//						if(GTProb == 93) std::cout << "93 " << GTProbDouble << " " << vcfFile.position() << " " << trueGenotype << " " << vcfFile.getFirstAlleleOfSample(sampleNum) << vcfFile.getSecondAlleleOfSample(sampleNum) << std::endl;
//						if(GTProb == 92) std::cout << "92 " << GTProbDouble << " " << vcfFile.position() << " " << trueGenotype << " " << vcfFile.getFirstAlleleOfSample(sampleNum) << vcfFile.getSecondAlleleOfSample(sampleNum) << std::endl;
//						if(genotypeClass[homoRef] && vcfFile.getFirstAlleleOfSample(sampleNum) == trueGenotype[1] && vcfFile.getSecondAlleleOfSample(sampleNum) == trueGenotype[1]){
//							std::cout << GTProb << " " << trueGenotype << " " << vcfFile.getFirstAlleleOfSample(sampleNum) << vcfFile.getSecondAlleleOfSample(sampleNum) << std::endl;
//						}

					}

					//get true genotype index
					trueIndex = getGenotypeIndex(trueGenotype[0], trueGenotype[1]);
					//get genotype of sample from vcf
					calledIndex = getGenotypeIndex(vcfFile.getFirstAlleleOfSample(sampleNum), vcfFile.getSecondAlleleOfSample(sampleNum));
					if (calledIndex > -1 && trueIndex > -1){
						//add counts to tables
						makeQualCounts(qualAlleleCounts, distinguishHomos, genotypeClass, trueIndex, calledIndex, qual);
						makeQualCounts(probAlleleCounts, distinguishHomos, genotypeClass, trueIndex, calledIndex, GTProb);
					}
					//if(GTProb != 99) std::cout << GTProb << std::endl;
					//if(GTProb == 93) std::cout << vcfFile.position() << " " << trueGenotype << " " << vcfFile.getFirstAlleleOfSample(sampleNum) << vcfFile.getSecondAlleleOfSample(sampleNum) << std::endl;
				}
			}
		}
	}

	for(int i=0; i<4; ++i){
		std::string header="qual\ttotalAlleles\twrongAlleles\n";
		*(myOutStreams_qual.at(i)) << header;
		for(int q=0; q<101; ++q){
			*(myOutStreams_qual.at(i)) << q << "\t" << qualAlleleCounts[i][q][0] << "\t" << qualAlleleCounts[i][q][1] << "\n";
		}
	}


	for(int i=0; i<4; ++i){
		std::string header="prob\ttotalAlleles\twrongAlleles\n";
		*(myOutStreams_prob.at(i)) << header;
		for(int q=0; q<101; ++q){
			*(myOutStreams_prob.at(i)) << q << "\t" << probAlleleCounts[i][q][0] << "\t" << probAlleleCounts[i][q][1] << "\n";
		}
	}

    for(myOutStreamsIt_qual = myOutStreams_qual.begin(); myOutStreamsIt_qual != myOutStreams_qual.end(); ++myOutStreamsIt_qual){
    	//(**myOutStreamsIt).close();
    	delete *myOutStreamsIt_qual;
    }

    for(myOutStreamsIt_prob = myOutStreams_prob.begin(); myOutStreamsIt_prob != myOutStreams_prob.end(); ++myOutStreamsIt_prob){
    	//(**myOutStreamsIt).close();
    	delete *myOutStreamsIt_prob;
    }

    if(verbose) std::cerr << " done!" << std::endl;
}

void makeSampleMapVectorForLoop(TVcfFileSingleLine vcfFile, std::map <std::string, int> & sampleNames, std::string & nameDiff, std::string & withoutDiff, std::vector<int> & samplesToConsiderInLoop, int & numSamplesIgnored, bool verbose){
	for(int i=0; i < vcfFile.numSamples(); ++i){
		if(vcfFile.sampleName(i).find(nameDiff) != std::string::npos) {
			withoutDiff = stringReplace(nameDiff, "", vcfFile.sampleName(i));
			if(sampleNames.count(withoutDiff) > 0) {
				//complete map (necessary for printing and filter function)
				sampleNames.at(vcfFile.sampleName(i)) = 1;
				sampleNames.at(withoutDiff) = 1;
				//necessary for compareCallsInd function
				samplesToConsiderInLoop.push_back(i);
			}
		}
	}

	//print out which samples will be ignored
	numSamplesIgnored = 0;
	if(verbose) std::cerr << "    - the genotypes for the following samples will be set to ./.: ";
	for(int i=0; i < vcfFile.numSamples(); ++i){
		if(sampleNames.at(vcfFile.sampleName(i)) == 0){
			++numSamplesIgnored;
			if(verbose) std::cerr << vcfFile.sampleName(i) << " ";
		}
	}
	if(verbose) std::cerr << "\n    -> A total of " << numSamplesIgnored << " samples will be ignored.\n";
}

void TAnnotator::filterForMinimumDepth(){
	//open vcf file
	if(verbose) std::cerr << " - Reading from VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//prepare output vcf
	prepareVcfOutput(vcfFile);
	vcfFile.writeHeaderVCF_4_0();

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//sample filter
//	std::vector<std::string> sampleFilters;
//	myParameters->fillParameterIntoVector("sampleFilters", sampleFilters, ',');
	bool sampleFilter = myParameters->parameterExists("sampleFilterNameDiff");
	std::string nameDiff = myParameters->getParameterStringWithDefault("sampleFilterNameDiff", "");
	if(sampleFilter == true && verbose) std::cerr << "Will filter out individuals that do not exist both the form +" << nameDiff << " and -" << nameDiff << std::endl;
	std::map <std::string, int> sampleNames;

	//parse input for depth filters
	std::vector<std::string> filterParams;
	std::vector<std::string>::iterator itFilterParams;
	std::vector<std::string>::iterator i;
	std::pair<std::string, int> filterPair;

	//classify individuals
	std::map <std::string, int> indWithFilters;
	std::vector<std::pair<std::string, int> > filterVector;
	std::vector<std::pair<std::string, int> >::iterator itFilterVector;

	//other variables
	std::string withoutMinus, withoutDiff;
	int counter = -1;


	//make depth filter vector of pairs
	std::string parameterString = myParameters->getParameterString("depthFilters");
	myParameters->fillParameterIntoVector("depthFilters", filterParams, ',');
	if(!filterParams.empty()){
		for(itFilterParams=filterParams.begin(); itFilterParams!=filterParams.end(); ++itFilterParams){
			fillDoubleFromString(*itFilterParams,filterPair,':');
			filterVector.push_back(make_pair(filterPair.first, filterPair.second));
		}
	}

	//put the individuals into the filter map with the correct value
	//at same time create map with sampleNames for sampleFilter
	for(int i=0; i < vcfFile.numSamples(); ++i){
		if(sampleFilter) sampleNames.insert(make_pair(vcfFile.sampleName(i), 0));

		for(itFilterVector = filterVector.begin(); itFilterVector!=filterVector.end(); ++itFilterVector){
			if(itFilterVector->first.at(0) == '-'){
				withoutMinus = itFilterVector->first.substr(1,itFilterVector->first.size());
				if(vcfFile.sampleName(i).find(withoutMinus) == std::string::npos){
					indWithFilters.insert(make_pair(vcfFile.sampleName(i), itFilterVector->second));
				}
			} else if(vcfFile.sampleName(i).find(itFilterVector->first)) indWithFilters.insert(make_pair(vcfFile.sampleName(i), itFilterVector->second));
		}
	}

	//which samples to keep?
	//go through the samples that contain nameDiff and if the name exists without the diff, set both to 1
	if(sampleFilter){
		int numSamplesIgnored;
		std::vector<int> samplesToConsiderInLoop; // this is not needed here
		makeSampleMapVectorForLoop(vcfFile, sampleNames, nameDiff, withoutDiff, samplesToConsiderInLoop, numSamplesIgnored, verbose);
		vcfFile.addNewHeaderLine("##FILTER=<ID=VcfAnnotator-hard,Description=\"VcfAnnotator: removed " + toString(numSamplesIgnored) + "samples because they did not exist in the form +" + nameDiff + " and -" + nameDiff);
	}

	//filter the vcfFile
	vcfFile.addNewHeaderLine("##FILTER=<ID=VcfAnnotator-hard,Description=\"VcfAnnotator: " + parameterString + ">");
	while(vcfFile.next()){
		++counter;
		for(int i=0; i<vcfFile.numSamples(); ++i){
			if(vcfFile.sampleIsMissing(i) == true) continue;
			else if(indWithFilters.count(vcfFile.sampleName(i)) > 0 || (sampleFilter && sampleNames.at(vcfFile.sampleName(i)) != 1)){
				if(vcfFile.sampleDepth(i) < indWithFilters[vcfFile.sampleName(i)]) vcfFile.setSampleMissing(i);
			} else std::cerr << "this individual was not found in filters and will be left as is: " << vcfFile.sampleName(i) << std::endl;
		} if(verbose && counter%1000==0) std::cerr << "read " << counter << " lines" << std::endl;
	}
}

void TAnnotator::compareCallsIndividuals(){
	//open vcf file
	if(verbose) std::cerr << " - Reading from VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//input parameters
	std::string nameDiff = myParameters->getParameterString("sampleFilterNameDiff");

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//output name
	std::string outname= myParameters->getParameterString("outname");
	if(verbose) std::cerr << "    - Writing files to '" << outname << ".table'" << std::endl;

	//other variables
	int counter = 0;
	int counts[4][4] = {{0}};
	int withoutDiffSampleNumber;
	std::string withoutDiff;
	std::map <std::string, int> sampleNames;
	std::string gt;

	//initialize individuals map
	for(int i=0; i < vcfFile.numSamples(); ++i){
		sampleNames.insert(make_pair(vcfFile.sampleName(i), 0));
	}

	//decide which individuals to consider: only loop over the ones without nameDiff
	int numSamplesIgnored;
	std::vector<int> samplesToConsiderInLoop;
	makeSampleMapVectorForLoop(vcfFile, sampleNames, nameDiff, withoutDiff, samplesToConsiderInLoop, numSamplesIgnored, verbose);
	int numSamplesKept = vcfFile.numSamples() - numSamplesIgnored;

	//create vector of count tables for each individual considered, set count tables to 0
	int*** individualTables = new int**[numSamplesKept];
	bool individualTablesInitiated = false;
	if(myParameters->parameterExists("individualTables")){
		for(int i=0; i < numSamplesKept; ++i){
			individualTables[i] = new int*[4];
			for(int j=0; j < 4; ++j){
				individualTables[i][j] = new int[4];
				for(int k=0; k < 4; ++k){
					individualTables[i][j][k] = 0;
				}
			}
		}
		individualTablesInitiated = true;
		std::cerr << "    - printing individual count tables" << std::endl;
	}

	std::string genotypes[4] = {"0/0", "0/1", "1/1", "./."};

	int totalAdded = 0;

	while(vcfFile.next()){
		++counter;
		int n = 0;
		for(std::vector<int>::iterator it=samplesToConsiderInLoop.begin(); it!=samplesToConsiderInLoop.end(); ++it, ++n){
			withoutDiff = stringReplace(nameDiff, "", vcfFile.sampleName(*it));
			withoutDiffSampleNumber = vcfFile.sampleNumber(withoutDiff);
			//if one or both genotypes are missing
			if(vcfFile.sampleIsMissing(*it) && vcfFile.sampleIsMissing(withoutDiffSampleNumber)){
				++counts[3][3];
				if(individualTablesInitiated) ++individualTables[n][3][3];
				++totalAdded;
			} else if(vcfFile.sampleIsMissing(*it) && !vcfFile.sampleIsMissing(withoutDiffSampleNumber)){
				for(int j=0; j < 3; ++j){
					if(vcfFile.fieldContentAsString("GT", withoutDiffSampleNumber) == genotypes[j]){
						++counts[3][j];
						if(individualTablesInitiated) ++individualTables[n][3][j];
						++totalAdded;
					}
				}
			} else if(!vcfFile.sampleIsMissing(*it) && vcfFile.sampleIsMissing(withoutDiffSampleNumber)){
				for(int i = 0; i < 3; ++i){
					if(vcfFile.fieldContentAsString("GT", *it) == genotypes[i]){
						++counts[i][3];
						if(individualTablesInitiated) ++individualTables[n][i][3];
						++totalAdded;
					}
				}
			}
			//if both genotypes are available
			else if(!vcfFile.sampleIsMissing(*it) && !vcfFile.sampleIsMissing(withoutDiffSampleNumber)){
				for(int i=0; i<3; ++i){
					for(int j=0; j<3; ++j){
						if(vcfFile.fieldContentAsString("GT", *it) == genotypes[i] && vcfFile.fieldContentAsString("GT", withoutDiffSampleNumber) == genotypes[j]){
	//						std::cout << "i is " << i << " and j is " << j << " *it is " << vcfFile.fieldContentAsString("GT", *it) << "withoutDiff is " << vcfFile.fieldContentAsString("GT", withoutDiffSampleNumber)<< std::endl;
							++counts[i][j];
							if(individualTablesInitiated) ++individualTables[n][i][j];
							++totalAdded;
						}
					}
				}
			}
		}
		if(unsigned(totalAdded) != (counter * samplesToConsiderInLoop.size())) std::cerr << "at position " << vcfFile.position() << " the counts were " << totalAdded << " instead of " << counter*samplesToConsiderInLoop.size() << std::endl;
		if(verbose && counter % 1000 == 0)	std::cerr << "    - read " << counter << " lines" << std::endl;
	}

	//output
	//total file
	std::string filename = outname+"_allIndividuals.table";
	std::ofstream tableFileAllInd(filename.c_str());
    tableFileAllInd << "withStringDiff/withoutStringDiff\t0/0\t0/1\t1/1\t./." << std::endl;
    for (int i = 0; i < 4; i ++){
    	tableFileAllInd << genotypes[i];
    	for (int j= 0; j < 4; j++){
    		tableFileAllInd << "\t" << counts[i][j];
        }
    	tableFileAllInd << "\n";
    }
    //all separate files
    if(individualTablesInitiated){
    	int n = 0;
    	for(std::vector<int>::iterator it=samplesToConsiderInLoop.begin(); it!=samplesToConsiderInLoop.end(); ++it, ++n){
    		withoutDiff = stringReplace(nameDiff, "", vcfFile.sampleName(*it));
    		filename = outname + "_" + vcfFile.sampleName(*it) + "_vs_" + withoutDiff + ".table";
    		std::ofstream tableFileOneInd(filename.c_str());
    		tableFileOneInd << "withStringDiff/withoutStringDiff\t0/0\t0/1\t1/1\t./." << std::endl;
    		for (int i = 0; i < 4; i ++){
    			tableFileOneInd << genotypes[i];
    			for (int j= 0; j < 4; j++) tableFileOneInd << "\t" << individualTables[n][i][j];
    			tableFileOneInd << "\n";
    		}
    	}
    }

    if(verbose) std::cerr << "    - Parsed a total of " << counter << " positions. Added " << totalAdded << " counts to table of all individuals." << std::endl;

    //delete individual tables
    if(individualTablesInitiated){
		for(int i=0; i < numSamplesKept; ++i) for(int j=0; j < 4; ++j) delete[] individualTables[i][j];
		delete[] individualTables;
    }
}

void TAnnotator::adjustPL(){
	//open vcf file
	if(verbose) std::cerr << " - Reading from VCF:" << std::endl;
	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();
	vcfFile.enableWriting();

	//prepare output vcf
	std::string outname= myParameters->getParameterString("outname");
	prepareVcfOutput(vcfFile);
	vcfFile.writeHeaderVCF_4_0();


	//what type of input file?
	bool considerDepth = false;

//	read input file
	std::string inputFileName = myParameters->getParameterString("newPLs");
	std::ifstream file(inputFileName.c_str());
	if(!file) throw "Failed to open file with new PL's' " + inputFileName + "'!";
	std::string paramLine;

	//parse input file
	std::vector<std::string> paramVector;
	std::vector<int> depthInterval;

	std::getline(file, paramLine); //header
	fillVectorFromString(paramLine, paramVector, "\t");
	if(paramVector.size() == 3) considerDepth = true;
	else if(paramVector.size() < 2 && paramVector.size() > 3) throw "Wrong number of columns in " + inputFileName + ": " + toString(paramVector.size()) + " instead of 2 or 3 columns found!";

	std::map<std::pair<std::string, int>, std::string> newPLs; //key is pair of g.t. and depth
	int lineNum = 0;

	while(file.good() && !file.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(file, paramVector);
		if(!paramVector.empty()){
			if(considerDepth){
				if(paramVector.size() != 3) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + inputFileName + "'! There should be 3!";
				//make entry in newPLs-map for each depth in interval
				fillVectorFromStringAny(paramVector[1], depthInterval, "-");
				for(int i = (int) depthInterval.at(0); i < ((int) depthInterval[1] + 1); ++i){
					newPLs.insert(make_pair(make_pair(paramVector[0], i), paramVector[2]));
				}
			} else {
				if(paramVector.size() != 2) throw "Wrong number of entries on line " + toString(lineNum) + " in file '" + inputFileName + "'! There should be 2!";
				newPLs.insert(make_pair(make_pair(paramVector[0], -1), paramVector[1]));
			}
		}
	}

	if(verbose) std::cerr << "     - finished parsing " << inputFileName << std::endl;


	int counter = 0;
	std::pair<std::string, int> gtDP;
	while(vcfFile.next()){
		++counter;
		for(int i=0; i < vcfFile.numSamples(); ++i){
			if(!vcfFile.sampleIsMissing(i)){
				if(considerDepth) gtDP = make_pair(vcfFile.fieldContentAsString("GT", i), vcfFile.fieldContentAsInt("DP", i));
				else gtDP = make_pair(vcfFile.fieldContentAsString("GT", i), -1);
				if(newPLs.count(gtDP) > 0){
					vcfFile.updatePL(newPLs[gtDP], i);
				}
			}
		}
		if(verbose && counter % 1000 == 0)	std::cerr << "    - read " << counter << " lines" << std::endl;
	}
}


void TAnnotator::assessAllelicImbalance(){
	//open vcf file
	if(verbose) std::cerr << " - assessing allelic imbalance:" << std::endl;

	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//prepare output
	std::string outname = myParameters->getParameterString("outname") + "_allelicDepth.txt";
	if(verbose) std::cerr << "    - Writing files to '" << outname  << std::endl;
	int maxDP = myParameters->getParameterIntWithDefault("maxDepth", 1000);

	//limit input?
	long inputLines = myParameters->getParameterLongWithDefault("inputLines", -1);
	if(inputLines <= 0){
		if(verbose)
			std::cerr << "    - Reading whole vcf." << std::endl;
	} else
		std::cerr << "    - Limiting input to " << inputLines << " lines." << std::endl;

	//initialize table
	long** table = new long*[maxDP];
	for(int i=0; i<maxDP; ++i){
		table[i] = new long[maxDP];
	}

	for(int i=0; i<maxDP; ++i){
		for(int j=0; j<maxDP; ++j)
			table[i][j] = 0;
	}

	//temp variables
	int counter = 0;
	int numHet = 0;

	//open log file for sites
	std::ofstream outTable(outname.c_str());
	if(!outTable)
		throw "Failed to open file '" + outname + " for writing!";

	while(vcfFile.next()){
		++counter;

		//count allelic depth across all samples
		for(int i=0; i < vcfFile.numSamples(); ++i){
			if(!vcfFile.sampleIsMissing(i) && vcfFile.sampleIsHeteroRefNonref(i)){
				++numHet;
				std::vector<std::string> tmp;
				std::string tag="AD";
				fillVectorFromString(vcfFile.getSampleContentAt(tag, i), tmp, ',');
				int numRef = stringToInt(tmp[0]);
				int numAlt = stringToInt(tmp[1]);
				if(vcfFile.depthAsIntNoCheckForMissingSample("DP", i) > maxDP){
					std::cerr << "WARNING: DP is " + toString(vcfFile.depthAsIntNoCheckForMissingSample("DP", i)) + " at pos " + toString(vcfFile.position()) + ". This site will be ignored.";
					continue;
				}
				++table[numRef][numAlt];
			}
		}
		if(verbose && counter % 1000000 == 0)
			std::cerr << "    - read " << counter << " lines, " << numHet << " heterozygous sites were found." << std::endl;
		if(inputLines != -1 && counter > inputLines){
			std::cerr << "    - reached input limit!" << std::endl;
			break;
		}
	}

	std::cerr << "    - Done reading vcf!" << std::endl;;

	//write table
	//header
	outTable << "ref_depth/alt_depth";
	for(int i=0; i<maxDP; ++i){
		outTable << "\talt_" << i;
	}
	outTable << "\n";

	for(int i=0; i<maxDP; ++i){
		outTable << "ref_" << i;
		for(int j=0; j<maxDP; ++j)
			outTable << "\t" << table[i][j];
		outTable << "\n";
	}

	//clean up
	outTable.close();

	for(int i = 0; i < maxDP; ++i)
	    delete[] table[i];
	delete[] table;
}

void TAnnotator::filterAllelicImbalance(){
	//open vcf file
	if(verbose) std::cerr << " - Filtering sites with allelic imbalance:" << std::endl;

	double pValFilter = myParameters->getParameterDoubleWithDefault("pValFilter", 0.001);

	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();
	vcfFile.enableWriting();

	//prepare output vcf
	prepareVcfOutput(vcfFile);
	vcfFile.writeHeaderVCF_4_0();

	//how to filter?
	int counter = 0;
	std::string tag="AD";
	int numRef; int numAlt; int tot;
	int numHet;
	std::vector<int> tmp;
	double logHalf = log(0.5);
	double cumul;
	int i;
	long numFiltered = 0;
	long numPassedFilter = 0;

	//open log file for sites
	std::string filename = myParameters->getParameterStringWithDefault("infoOut", "ImbalanceFilter_siteInfo.txt");
	std::ofstream sitesInfoOut(filename.c_str());
	if(!sitesInfoOut)
		throw "Failed to open file '" + filename + " for writing!";
	sitesInfoOut << "chr\tpos\tnumHet\tnumRef\tnumAlt\taltFrac\tpVal\tstatus\n";

	while(vcfFile.next()){
		++counter;

		//count allelic depth across all samples
		numRef = 0; numAlt = 0;
		numHet = 0;
		for(int i=0; i < vcfFile.numSamples(); ++i){
			if(!vcfFile.sampleIsMissing(i) && vcfFile.sampleIsHeteroRefNonref(i)){
				fillVectorFromString(vcfFile.getSampleContentAt(tag, i), tmp, ',');
				numRef += tmp[0]; numAlt += tmp[1];
				++numHet;
			}
		}

		//write info
		sitesInfoOut << vcfFile.chr() << "\t" << vcfFile.position() << "\t" << numHet;

		//calculate p-value for sampling
		if(numHet > 0){
			sitesInfoOut << "\t" << numRef << "\t" << numAlt << "\t" << (double) numAlt / (double)(numAlt+numRef);
			cumul = 0.0;
			tot = numAlt + numRef;
			if(numAlt < numRef){
				for(i = 0; i <= numAlt; ++i)
					cumul += exp(randomGenerator->binomCoeffLn(tot, i) + logHalf*tot);
			} else {
				for(i = numAlt; i <= tot; ++i)
					cumul += exp(randomGenerator->binomCoeffLn(tot, i) + logHalf*tot);
			}

			//now filter
			if(cumul < pValFilter){
				++numFiltered;
				vcfFile.written = true;
				sitesInfoOut << "\t" << cumul << "\tfiltered\n";
			} else {
				vcfFile.writeLine();
				++numPassedFilter;
				sitesInfoOut << "\t" << cumul << "\tpassed\n";
			}
		} else {
			++numPassedFilter;
			sitesInfoOut << "\t-\t-\t-\t-\tpassed\n";
		}

		if(verbose && counter % 1000 == 0)	std::cerr << "    - read " << counter << " lines, " << numPassedFilter << " passed, " << numFiltered << " were filtered out." << std::endl;
	}

	//clean up
	sitesInfoOut.close();
}

//------------------------------------------------------------------------
//SIMULATE AND ESTIMATE GENOTYPING ERROR RATES
//------------------------------------------------------------------------
void TAnnotator::simulateGenoErrorData(){
	//initialize random generator
	initializeRandomGenerator();

	if(verbose) std::cerr << " - Simulating genotypes with errors:" << std::endl;;

	//create object and run!
	TGenoError genoError(verbose);
	genoError.simulateGenoErrorData(myParameters, randomGenerator);
}

void TAnnotator::inferGenoError(){
	if(verbose) std::cerr << " - Inferring per allele genotyping errors from replicates:" << std::endl;

	//create object and run!
	TGenoError genoError(verbose);
	genoError.inferGenoError(myParameters);
}

void TAnnotator::recalibrateVCFGenoError(){
	if(verbose) std::cerr << " - Recalibrating a VCF file from estimates of genotyping errors:" << std::endl;

	//create object and run!
	TGenoError genoError(verbose);
	genoError.recalibrateVCFGenoError(myParameters);
}


void TAnnotator::vcfToInvariantBed(){
	//open vcf file
	if(verbose) std::cerr << " - Writing sites that are invariant across individuals to bed:" << std::endl;

	TVcfFileSingleLine vcfFile(verbose);
	prepareVcfInput(vcfFile);

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableFormatParsing();
	vcfFile.enableSampleParsing();
	vcfFile.enableVariantParsing();

	//open output
	std::string outname= myParameters->getParameterString("outname");
	if(verbose) std::cerr << "    - Writing files to '" << outname << ".bed.gz'" << std::endl;
	gz::ogzstream bedFile((outname + (std::string) ".bed.gz").c_str());

	//parse vcf file
	std::map<char, int> bases = {{'A', 1}, {'C', 1}, {'G', 1}, {'T', 1}};
	int counter = 0;
	while(vcfFile.next()){
		++counter;
		if(bases.find(vcfFile.getRefAllele()) == bases.end()) {//!= 'A' && vcfFile.getRefAllele() != 'C' && vcfFile.getRefAllele() != 'G' && vcfFile.getRefAllele() != 'T'
			continue; //ignore indels
		}

		int indCounter = 0;
		char allele = vcfFile.getFirstAlleleOfSample(0);
		for(int ind=0; ind<vcfFile.numSamples(); ++ind){
			++indCounter;
			if(vcfFile.getFirstAlleleOfSample(ind) != allele || vcfFile.getSecondAlleleOfSample(ind) != allele){
				break;
			}
		}
		if(indCounter == vcfFile.numSamples()){
			//bed is 0-based
			bedFile << vcfFile.chr() << "\t" << vcfFile.position() - 1 << "\t" << vcfFile.position() << std::endl;
		}

	}

	bedFile.close();
}
//--------------------------------------------------------
//TEST
//--------------------------------------------------------

void TAnnotator::test(){
	/*
	//read parameters
	long windowSize=myParameters->getParameterDouble("windowsize");
	long step=myParameters->getParameterDouble("step");
	long reps=myParameters->getParameterDouble("replicates");

	//open vcf file
	TVcfFileWindow vcfFile(10, 5, verbose);
	prepareVcfInput(vcfFile);
	initializeRandomGenerator();

	//enable parsers
	vcfFile.enablePositionParsing();
	vcfFile.enableVariantParsing();
	vcfFile.enableSampleParsing();

	//get samples to use
	TSampleGroup group1(&vcfFile, randomGenerator, myParameters->getParameterString("group1"), windowSize);
	TSampleGroup group2(&vcfFile, randomGenerator, myParameters->getParameterString("group2"), windowSize);


	//output
	ostream* out;
	if(myParameters->parameterExists("outname")){
		out=new std::ofstream(myParameters->getParameterString("outname").c_str());
	} else out=&std::cout;

	//start loop
	bool continueParsing=true;
	long numSegSites1, numSegSites2;
	double* logRatio=new double[reps];

	do {
		if(!vcfFile.advance()){
			if(vcfFile.eof)	continueParsing=false;
			if(vcfFile.isEmpty) continue;
		}

		group1.fill();
		group2.fill();

		for(long i=0; i<reps; ++i){
			logRatio[i]=log10(group1.sampleNumSegSites()) - log10(group2.sampleNumSegSites());
		}

		//write something better
		//out << vcfFile.pointerToFirstVcfLine()->chr << "\t" << vcfFile.pointerToFirstVcfLine()->pos << "\t" << vcfFile.pointerToLastVcfLine()->pos;
		//out << "\t" << x.AA << "\t" << x.AB << "\t" << x.BB << "\t" << randomGenerator->getRand() << std::endl;

	} while(continueParsing);

	//delete
	delete[] logRatio;

*/
}




