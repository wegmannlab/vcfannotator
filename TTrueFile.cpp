/*
 * TTrueFile.cpp
 *
 *  Created on: Oct 2, 2017
 *      Author: vivian
 */

#include "TTrueFile.h"

//------------------
// TTrueFile
//------------------

TTrueFile::TTrueFile(TParameters & Params, bool Verbose){
	myParameters = &Params;
	verbose = Verbose;
	vcfVsFlatCalls = myParameters->parameterExists("vcfVsFlatCalls");
	twoSingleVcf = myParameters->parameterExists("twoSingleVcf");
	myStream = NULL;
	trueChrom = "none";
	truePos = 0;
	lineNum = 0;
	trueFileName = myParameters->getParameterString("trueFile");
	if(!open()) throw "unable to initialize true file class!";
}

bool TTrueFile::open(){
	if(stringContains(trueFileName, ".gz")) myStream = new gz::igzstream(trueFileName.c_str());
	else myStream = new std::ifstream(trueFileName.c_str());
	if(!(*myStream)) throw "Failed to open true genotype file '" + trueFileName + "'!";

	//a) UstIshim experiment: compare two call files: vcf and trueFile=flat MLE calls
	if(vcfVsFlatCalls){
		if(verbose) std::cerr << " - comparing vcf with single individual to flat file calls" << std::endl;
	//b) compare two vcf files
	} else if(twoSingleVcf){
		if(verbose) std::cerr << " - comparing two vcf's with single individuals" << std::endl;
		if(verbose) std::cerr << " - WARNING: trueFile calls with DP=0 but GT=0/0 will be counted as homozygous reference!" << std::endl;

	} else std::cerr << " - assuming trueFile to have format 'chromosome \\tab position \\tab genotype' and to contain one or more individuals" << std::endl;

	if(!myStream->good() || myStream->eof()) return false;
	else return true;
}

void TTrueFile::setOrder(){
	if(!myStream->good() || myStream->eof()) throw "Error! True file is not good!";
	std::getline(*myStream, trueLine);
	fillVectorFromString(trueLine, order, "\t"); //split header into the names
	if(order.size() < 3) throw "trueFile must contain at least 3 columns!";
	order.erase(order.begin()); //first two elements are pos and reference
	order.erase(order.begin()); //first two elements are pos and reference
	if(vcfVsFlatCalls) fillVectorFromString(trueLine, order, '-'); //so that there is no split
}

void TTrueFile::setOrder(std::string sampleName){
	order.push_back(sampleName);
}

void TTrueFile::checkColumnNumbers(){
	//correct column numbers?
	if(vcfVsFlatCalls && trueVec.size() != 15 && trueVec.size() != 16) {
		for(unsigned int i=0; i < trueVec.size(); i++){
			std::cerr << trueVec[i] << std::endl;
		}
		throw "Found " + toString(trueVec.size()) + " instead of 15 or 16 columns in '" + trueFileName + "' on line " + toString(lineNum) + "!";
	} else if(twoSingleVcf && trueVec.size() != 10) throw "true vcf file contains " + toString(trueVec.size()) + " instead of 10!";
	else if (!vcfVsFlatCalls && !twoSingleVcf && trueVec.size() != 2 + unsigned(order.size()))	throw "Found " + toString(trueVec.size()) + "column(s) instead of " + toString(2 + order.size()) + " in '" + trueFileName + "' on line " + toString(lineNum) + "!";
}


bool TTrueFile::parseLine(){
	if(!myStream->good() || myStream->eof()) throw "Error! True file is not good!";
	std::getline(*myStream, trueLine);

	if(!trueLine.empty()){
		if(twoSingleVcf){
			//remove header
			while(trueLine[0] == '#') std::getline(*myStream, trueLine);
		}
		trimString(trueLine);
		fillVectorFromStringWhiteSpaceSkipEmpty(trueLine, trueVec);
		checkColumnNumbers();
		trueChrom = trueVec[0];
		truePos = stringToInt(trueVec[1]);
		lineNum ++;

		return true;
	} else return false;
}

std::string TTrueFile::getTrueGenotype(int orderIndex){
	//get true genotype and index
	if(vcfVsFlatCalls) trueGenotype = trueVec[14];
	else if(twoSingleVcf){
		//get true alleles in numbers
		std::string lastCol = trueVec[9];
		fillVectorFromString(lastCol,vcfGT,':');
		fillVectorFromString(vcfGT[0], allelesNum, '/');

		if(vcfGT[0] == "./." || stringContains(lastCol, ":0:")){ //only genotypes with DP=0 or GQ=0 would have this
			trueGenotype = "-";
			return trueGenotype;
		}

		//translate them to base characters
		std::string allelesBaseChar = "";
		allelesBaseChar += trueVec[3];
		fillVectorFromString(trueVec[4], altAlleles, ',');
		for(std::vector<std::string>::iterator it = altAlleles.begin(); it != altAlleles.end(); ++it){
			allelesBaseChar += *it;
		}
		trueGenotype = "";
		for(unsigned int i=0; i<allelesNum.size(); ++i){
			int index = stringToInt(allelesNum.at(i));
			trueGenotype += allelesBaseChar.at(index);
		}
	}
	else {trueGenotype = ""; trueGenotype += trueVec[orderIndex+2][0]; trueGenotype += trueVec[orderIndex+2][2];}
	return trueGenotype;
}




