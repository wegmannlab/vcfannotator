/*
 * vcfAnnotator.cpp
 *
 *  Created on: Jun 15, 2011
 *      Author: wegmannd
 */

#include "TAnnotator.h"
#include <time.h>
#include "TParameters.h"
#include <sys/time.h>
#include "TLog.h"


/*TODO:
 * Treat X and Y chromosomes as Int by defining a constant and parsing appropriately
 * Add a flag to skip testing things when parsing VCF to speed up
 * Read INFO column as well
 * Add possibility to filter on INFO column
 */


void showExplanations(){
	std::cerr << "\n************************************************************************************"
		 << "\n* vcfAnnotator by Daniel Wegmann                                                   *"
		 << "\n************************************************************************************"
	     << "\n* This program annotates positions as non-coding, synonymous or non-synonymous     *"
	     << "\n* by comparing the sequences of different individuals and the coordinates of known *"
	     << "\n* transcripts. It analysis a single chromosome at the time. The sequence data is   *"
	     << "\n* expected to be read from stdin and to be in vcf format.                          *"
	     << "\n*                                                                                  *"
	     << "\n* It needs the following arguments (in arg=value format):                           *"
	     << "\n*    task        : one of the following tags:                                      *"
	     << "\n*                  - synNonsyn : deriving the mutational class of all variants from*"
	     << "\n*                                transcript definitions. Requires 'transcripts'.   *"
	     << "\n*                  - varDist   : adding the distance to the next variant position  *"
	     << "\n*                                (up to 100) to each individual through the tag DV.*"
	     << "\n*                                Requires 'maxDist' and 'minGQ'.                   *"
	     << "\n*                  - checkDV   : Print lines and samples for which the DV entry is *"
	     << "\n*                                wrong.                                            *"
	     << "\n*                  - hardFilter: Impose a hard filter, i.e. setting all genotypes  *"
	     << "\n*                                missing where filter is not passed. Requires      *"
	     << "\n*                                a filter specified using argument 'filter'.       *"
	     << "\n*    file        : the name of the vcf file to parse. If absent, the program reads *"
	     << "\n*                  from standard input instead. To parse zipped vcf files use      *"
	     << "\n*                  'gunzip <vcf-file> | vcfAnnotator <options>'. The vcf file/info *"
	     << "\n*                  is assumed to be ordered by position within chromosomes.        *"
	     << "\n*    maxDist     : the largest distance up stream and downstream to consider.      *"
	     << "\n*    minGQ       : the smallest GQ value allowed for calling a position a variant. *"
	     << "\n*    filter      : a filter to be imposed in the format FIELD<VALUE or FIELD>VALUE.*"
	     << "\n*                  Example: \"filter=GQ<20\" will set all genotypes with GQ<20 as    *"
	     << "\n*                           missing.                                               *"
	     << "\n*    transcripts : the name of a file with all exons to consider. The exons should *"
	     << "\n*                  only include the coding parts. The format is as follows:        *"
	     << "\n*                    - chromosome (treated as text, leading 'chr' is ignored)      *"
	     << "\n*                    - start position                                              *"
	     << "\n*                    - end position                                                *"
	     << "\n*                    - name of transcript                                          *"
	     << "\n*                    - strand as + or -                                            *"
	     << "\n*                    - additional columns will be ignored                          *"
		 << "\n*    outname    : name of a file to which the resulting annotation is written. If  *"
		 << "\n*                 absent, results are written to standard output. Use 'novcf' to   *"
		 << "\n*                 prevent writing a vcf output (see below).                        *"
		 << "\n*    novcf      : if present, no vcf file is written.                              *"
		 << "\n*    verbose    : if given, performed computations and progress updates are printed*"
		 << "\n*                 to standard error.                                               *"
		 << "\n************************************************************************************\n\n";
}


int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	try{
		if (argc<2){
			showExplanations();
		} else {
			//read parameters from the command line
			TLog logfile;
			TParameters myParameters(argc, argv, &logfile);

			//verbose?
			bool verbose=myParameters.parameterExists("verbose");
			if(verbose) std::cerr << "vcfAnnotator" << std::endl << "---------" << std::endl;

			//what to do?
			TAnnotator annotator(myParameters, verbose);
			std::string task = myParameters.getParameterString("task");

			if(task=="synNonsyn") annotator.deriveSynonymouyNonsynonymousStatusFromTranscripts();
			else if(task=="varDist") annotator.annotateDistanceToClosestVariant();
			else if(task=="test") annotator.test();
			else if(task=="checkDV") annotator.checkDVField();
			else if(task=="hardFilter") annotator.imposingHardFiltersOnSamples();
			else if(task=="convert2Plink") annotator.convert2Plink();
			else if(task=="convert2Phylip") annotator.convert2Phylip();
			else if(task=="convert2Nexus") annotator.convert2Nexus();
			else if(task=="convert2TreeMix") annotator.convert2TreeMix();
			else if(task=="convert2FModelHMM") annotator.convert2FModelHMM();
			else if(task=="vcfToBeagle") annotator.vcfToBeagle();
			else if(task=="compareCalls") annotator.compareCalls();
			else if(task=="compareQualityToError") annotator.compareQualityToError();
			else if(task=="filter") annotator.filterForMinimumDepth();
			else if(task=="compareCallsIndividuals") annotator.compareCallsIndividuals();
			else if(task=="adjustPL") annotator.adjustPL();
			else if(task=="assessAllelicImbalance") annotator.assessAllelicImbalance();
			else if(task=="filterAllelicImbalance") annotator.filterAllelicImbalance();
			else if(task=="simulateGenoErrors") annotator.simulateGenoErrorData();
			else if(task=="inferGenoErrors") annotator.inferGenoError();
			else if(task=="recalVcf") annotator.recalibrateVCFGenoError();
			else if(task=="vcfToInvariantBed") annotator.vcfToInvariantBed();
			else throw "Unknown task '" + task + "'!";

			//end of program
			gettimeofday(&end, NULL);
			float runtime=(end.tv_sec  - start.tv_sec)/60.0;
			std::string unusedParams=myParameters.getListOfUnusedParameters();
			if(!unusedParams.empty()) std::cerr << "Arguments not used: " << unusedParams << std::endl;

			if(verbose) std::cerr << std::endl << "Program finished successfully in " << runtime << "s!" << std::endl << std::endl;
		}

	 }
	 catch (std::string & error){
		 std::cerr << std::endl << std::endl << "ERROR: " << error << std::endl << std::endl;
		 return 0;
	 }
	 catch (const char* error){
		 std::cerr << std::endl << std::endl << "ERROR: " << error << std::endl << std::endl;
		 return 0;
	 }
	 catch (...){
		 std::cerr << std::endl << std::endl << "ERROR: unhandeled error!" << std::endl << std::endl;
		 return 0;
	 }
	 return 1;
}
